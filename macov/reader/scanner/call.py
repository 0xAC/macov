from macov.core import atom
from macov.core.util import atom_set, dict_by_type, Range
from macov.reader.scanner.group import Group
from macov.reader.scanner.util import CALLABLE
from macov.util import trampoline
from pyrsistent import l
from typing import List, Tuple

_call_terminators = dict_by_type(
    atom.PostIf,
    atom.PostUnless,
    atom.Terminator,
)

_strip_exist_call_before = _call_terminators.keys() | atom_set(
    atom.LeftBracket,
    atom.ParenCallStart,
)

@trampoline.wrap
def add_missing_calls(atoms, result=l(), stack=l()):
    if not atoms:
        if not stack:
            return trampoline.result(result)

        position = result.first.range.end
        call_end = atom.WhitespaceCallEnd(Range(position, position))

        return trampoline.call(add_missing_calls)(atoms, result.cons(call_end), stack.rest)

    head, tail = atoms.first, atoms.rest

    if head.type in _call_terminators and stack:
        return trampoline.call(add_missing_calls)(atoms, result.cons(atom.WhitespaceCallEnd(head.range)), stack.rest)

    if head.type == atom.WhitespaceCallStart.type:
        return trampoline.call(add_missing_calls)(tail, result.cons(head), stack.cons(head))

    return trampoline.call(add_missing_calls)(tail, result.cons(head), stack)

@trampoline.wrap
def promote_parens_to_calls(atoms: List[atom.Atom], result: List[atom.Atom]=l()) -> List[atom.Atom]:
    if not atoms:
        return trampoline.result(result)

    head, tail = atoms.first, atoms.rest

    if head.type in CALLABLE:
        updated_tail, updated_result = (tail.rest, result.cons(head).cons(tail.first)) if tail and _is_implicit_call_start(tail.first) else (tail, result.cons(head))

        new_atoms, new_result = _upgrade_parens(updated_tail, updated_result)

        return trampoline.call(promote_parens_to_calls)(new_atoms, new_result)

    return trampoline.call(promote_parens_to_calls)(tail, result.cons(head))

def _upgrade_parens(atoms: List[atom.Atom], result: List[atom.Atom]) -> Tuple[List[atom.Atom], List[atom.Atom]]:
    if not atoms:
        return atoms, result

    head, tail = atoms.first, atoms.rest

    if head.type != atom.LeftParen.type:
        return atoms, result

    left_paren = head

    assert tail

    inside = tail.first

    assert inside.type == Group.type
    assert tail.rest

    right_paren = tail.rest.first

    assert right_paren.type == atom.RightParen.type

    call_end = atom.ParenCallEnd(right_paren.range)
    call_start = atom.ParenCallStart(left_paren.range)

    predecessor = result.first
    updated_result = result.rest if _is_implicit_call_start(predecessor) else result

    # pass call_end back to the list of tokens to detect it is callable
    final_atoms = tail.rest.rest.cons(call_end)
    new_result = updated_result.cons(call_start).cons(inside)

    return final_atoms, new_result

def _is_implicit_call_start(head) -> bool:
    return head.type == atom.WhitespaceCallStart.type and head.kind == atom.ImplicitCallStart.kind
