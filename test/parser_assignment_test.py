from macov.core import atom, node
from macov.reader import parse
import unittest

class ParserAssignmentTest(unittest.TestCase):
    def test_assignment_to_id(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Id("foo"),
                    node.Id("bar"),
                ),
            ]),
        )

    def test_mod_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ModAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_rem_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.RemAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_bit_and_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.BitAndAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_exp_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ExpAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_mul_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.MulAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_add_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.AddAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_sub_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.SubAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_integer_div_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IntegerDivAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_float_div_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.FloatDivAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_left_shift_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.LeftShiftAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_sign_right_shift_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.SignRightShiftAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_zero_fill_right_shift_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ZeroFillRightShiftAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_exist_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ExistAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_bit_xor_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.BitXorAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitXorAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_bit_or_assign(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.BitOrAssign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitOrAssignExpr(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_assignment_chain(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Assign(),
            atom.Id("bar"),
            atom.Assign(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Id("foo"),
                    node.AssignExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                ),
            ]),
        )

    def test_assignment_to_array(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.RightBracket(),
            atom.Assign(),
            atom.Id("bar"),
            atom.Assign(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Array([
                        node.Id("foo"),
                    ]),
                    node.AssignExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                ),
            ]),
        )

    def test_assignment_to_object(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.RightBrace(),
            atom.Assign(),
            atom.Id("bar"),
            atom.Assign(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Object([
                        node.Prop(
                            node.String("foo"),
                            node.Id("foo"),
                        ),
                    ]),
                    node.AssignExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                ),
            ]),
        )

    def test_assignment_to_accessor(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Dot(),
            atom.Property("bar"),
            atom.Assign(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Accessor(
                        node.Id("foo"),
                        node.String("bar"),
                    ),
                    node.Id("baz"),
                ),
            ]),
        )

    def test_assignment_function(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Assign(),
            atom.ParamStart(),
            atom.Id("bar"),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Id("foo"),
                    node.Function(
                        [
                            node.Param(node.Id("bar")),
                        ],
                        node.Block([]),
                    ),
                ),
            ]),
        )

    def test_assignment_function_with_block_body(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Assign(),
            atom.ParamStart(),
            atom.Id("bar"),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Indent(),
            atom.Id("baz"),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Id("foo"),
                    node.Function(
                        [
                            node.Param(node.Id("bar")),
                        ],
                        node.Block([
                            node.Id("baz"),
                        ]),
                    ),
                ),
            ]),
        )

    def test_destructive_assign_to_object(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.RightBrace(),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Object([
                        node.Prop(
                            node.String("foo"),
                            node.Id("foo"),
                        ),
                    ]),
                    node.Id("bar")
                )
            ])
        )

    def test_destructive_assign_to_object_with_at_property(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.At(),
            atom.Property("foo"),
            atom.RightBrace(),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Object([
                        node.Prop(
                            node.String("foo"),
                            node.Accessor(
                                node.This(),
                                node.String("foo"),
                            )
                        ),
                    ]),
                    node.Id("bar")
                )
            ])
        )

    def test_destructive_assign_to_object_with_property_initializer(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.Assign(),
            atom.Id("bar"),
            atom.RightBrace(),
            atom.Assign(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Object([
                        node.Prop(
                            node.String("foo"),
                            node.AssignExpr(
                                node.Id("foo"),
                                node.Id("bar"),
                            )
                        ),
                    ]),
                    node.Id("baz")
                )
            ])
        )

    def test_destructive_assign_to_array(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.RightBracket(),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Array([
                        node.Id("foo")
                    ]),
                    node.Id("bar")
                )
            ])
        )

    def test_destructive_assign_to_array_with_at_property(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.At(),
            atom.Property("foo"),
            atom.RightBracket(),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Array([
                        node.Accessor(
                            node.This(),
                            node.String("foo")
                        )
                    ]),
                    node.Id("bar")
                )
            ])
        )

    def test_destructive_assign_to_array_with_item_initializer(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.Assign(),
            atom.Id("bar"),
            atom.RightBracket(),
            atom.Assign(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Array([
                        node.AssignExpr(
                            node.Id("foo"),
                            node.Id("bar")
                        )
                    ]),
                    node.Id("baz")
                )
            ])
        )

    def test_destructive_assign_splat(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.Splat(),
            atom.RightBracket(),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Array([
                        node.SplatExpr(
                            node.Id("foo")
                        ),
                    ]),
                    node.Id("bar")
                )
            ])
        )

    def test_destructive_assign_splat_pre(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Splat(),
            atom.Comma(),
            atom.Id("foo"),
            atom.RightBracket(),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Array([
                        node.Rest(),
                        node.Id("foo"),
                    ]),
                    node.Id("bar")
                )
            ])
        )

    def test_destructive_assign_splat_post(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Splat(),
            atom.RightBracket(),
            atom.Assign(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AssignExpr(
                    node.Array([
                        node.Id("foo"),
                        node.Rest(),
                    ]),
                    node.Id("bar")
                )
            ])
        )

if __name__ == "__main__":
    unittest.main()
