from macov.core import node
from macov.core.util import dict_by_type, Range, SyntaxException, WithRange
import ply.yacc as yacc
from typing import NamedTuple, List, Tuple

tokens = [
    "add_assign",
    "and",
    "bin_exist",
    "bit_and_assign",
    "bit_or_assign",
    "bit_xor_assign",
    "class",
    "concat",
    "dec",
    "delete",
    "div",
    "dot_exist",
    "else",
    "exist_assign",
    "exp",
    "exp_assign",
    "extends",
    "false",
    "float_div_assign",
    "greater_than_or_equal_to",
    "id",
    "if",
    "in",
    "inc",
    "indent",
    "index_end",
    "index_start",
    "infinity",
    "instanceof",
    "integer_div_assign",
    "is",
    "isnt",
    "left_shift",
    "left_shift_assign",
    "less_than_or_equal_to",
    "mod",
    "mod_assign",
    "mul_assign",
    "nan",
    "new",
    "not",
    "null",
    "number",
    "of",
    "or",
    "outdent",
    "param_end",
    "param_start",
    "paren_call_end",
    "paren_call_start",
    "post_if",
    "post_unless",
    "property",
    "prototype",
    "regex",
    "regex_end",
    "regex_start",
    "rem_assign",
    "return",
    "sign_right_shift",
    "sign_right_shift_assign",
    "span",
    "splat",
    "string",
    "string_end",
    "string_start",
    "sub_assign",
    "super",
    "terminator",
    "thick_arrow",
    "thin_arrow",
    "this",
    "throw",
    "true",
    "typeof",
    "unless",
    "void",
    "whitespace_call_end",
    "whitespace_call_start",
    "zero_fill_right_shift",
    "zero_fill_right_shift_assign",
]

_assign_map = dict_by_type(
    node.AddAssignExpr,
    node.AssignExpr,
    node.BitAndAssignExpr,
    node.BitOrAssignExpr,
    node.BitXorAssignExpr,
    node.ExistAssignExpr,
    node.ExpAssignExpr,
    node.FloatDivAssignExpr,
    node.IntegerDivAssignExpr,
    node.LeftShiftAssignExpr,
    node.ModAssignExpr,
    node.MulAssignExpr,
    node.RemAssignExpr,
    node.SignRightShiftAssignExpr,
    node.SubAssignExpr,
    node.ZeroFillRightShiftAssignExpr,
)

precedence = (
    ("left", "post_if", "post_unless", "class"),
    ("nonassoc", "string_start", "string_end"),
    ("left", "concat"),
    ("right", ":", "return", *_assign_map.keys()),
    ("nonassoc", "indent", "outdent"),
    ("left", "bin_exist"),
    ("left", "or"),
    ("left", "and"),
    ("left", "|"),
    ("left", "^"),
    ("left", "&"),
    ("left", "is", "isnt", "<", ">", "less_than_or_equal_to", "greater_than_or_equal_to"),
    ("left", "in", "of", "instanceof"),
    ("left", "left_shift", "sign_right_shift", "zero_fill_right_shift"),
    ("left", "+", "-"),
    ("left", "*", "/", "div", "%", "mod"),
    ("left", "UADD"),
    ("right", "exp"),
    ("right", "not", "~"),
    ("right", "new", "typeof", "delete"),
    ("left", "?"),
    ("nonassoc", "inc", "dec"),
    ("left", "index_start", "index_end", "param_start", "param_end", "paren_call_start", "paren_call_end", "whitespace_call_start", "whitespace_call_end"),
    ("left", ".", "dot_exist"),
)

start = "Body"

class _Prototype(node.Literal):
    type = "prototype"

    def to_string(self) -> node.String:
        return node.String("prototype", self.range)

def _range_between(left: WithRange, right: WithRange) -> Range:
    return Range(left.range.begin, right.range.end)

def p_empty(p) -> None:
    """
    Empty :
    """

    pass

def p_param_var(p) -> None:
    """
    ParamVar : Id
             | Object
             | AtAccessor
             | Array
             | Assign
    """

    p[0] = p[1]

def p_param(p) -> None:
    """
    Param : ParamVar
          | SplatExpr
          | Rest
    """

    p[0] = node.Param(p[1], p[1].range)

def p_opt_comma(p) -> None:
    """
    OptComma : Empty
             | ','
    """

def p_param_list(p) -> None:
    """
    ParamList : Empty
              | Param
    """

    p[0] = [p[1]] if p[1] else []

def p_param_list_2(p) -> None:
    """
    ParamList : ParamList ',' Param
    """

    p[0] = p[1] + [p[3]]

def p_param_list_3(p) -> None:
    """
    ParamList : indent ParamList OptComma outdent
    """

    p[0] = p[2]

def p_param_list_4(p) -> None:
    """
    ParamList : ParamList OptComma terminator Param
    """

    p[0] = p[1] + [p[4]]

Arrow = NamedTuple("Arrow", [("is_bound", bool), ("range", Range)])

def p_arrow_thick(p) -> None:
    """
    Arrow : thick_arrow
    """

    p[0] = Arrow(is_bound=True, range=p[1].range)

def p_arrow_thin(p) -> None:
    """
    Arrow : thin_arrow
    """

    p[0] = Arrow(is_bound=False, range=p[1].range)

def p_function_without_parens(p) -> None:
    """
    Function : Arrow Block
    """

    p[0] = node.Function([], p[2], is_bound=p[1].is_bound, range=_range_between(p[1], p[2]))

def p_function(p) -> None:
    """
    Function : param_start ParamList param_end Arrow Block
    """

    p[0] = node.Function(p[2], p[5], is_bound=p[4].is_bound, range=_range_between(p[1], p[5]))

def p_obj_key_id(p) -> None:
    """
    ObjKey : id
    """

    p[0] = node.String(p[1].value, p[1].range)

def p_obj_key_string(p) -> None:
    """
    ObjKey : String
    """

    p[0] = p[1]

def p_assign_obj_id(p) -> None:
    """
    AssignObj : Id
    """

    p[0] = node.Prop(node.String(p[1].name, p[1].range), p[1])

def p_assign_obj_key_with_initializer(p) -> None:
    """
    AssignObj : Id '=' Expr
    """

    p[0] = node.Prop(node.String(p[1].name, p[1].range), node.AssignExpr(p[1], p[3]))

def p_assign_obj_key_at_accessor(p) -> None:
    """
    AssignObj : AtAccessor
    """

    p[0] = node.Prop(node.String(p[1].key.value, p[1].range), p[1])

def p_indent_object(p) -> None:
    """
    IndentObject : indent AssignList OptComma outdent
    """

    p[0] = node.Object(p[2])

def p_assign_obj_key_colon_value(p) -> None:
    """
    AssignObj : ObjKey ':' Expr
    """

    p[0] = node.Prop(p[1], p[3])

def p_assign_obj_key_colon_object(p) -> None:
    """
    AssignObj : ObjKey ':' IndentObject
    """

    p[0] = node.Prop(p[1], p[3])

def p_assign_list_empty(p) -> None:
    """
    AssignList : Empty
    """

    p[0] = []

def p_assign_list_assign_comma_assign_obj(p) -> None:
    """
    AssignList : AssignList ',' AssignObj
    """

    p[0] = p[1] + [p[3]]

def p_assign_list_assign_terminator_assign_obj(p) -> None:
    """
    AssignList : AssignList terminator AssignObj
    """

    p[0] = p[1] + [p[3]]

def p_assign_list_assign_obj(p) -> None:
    """
    AssignList : AssignObj
    """

    p[0] = [p[1]]

def p_assign_list_indent_outdent(p) -> None:
    """
    AssignList : indent AssignList OptComma outdent
    """

    p[0] = p[2]

def p_object(p) -> None:
    """
    Object : '{' AssignList OptComma '}'
    """

    p[0] = node.Object(p[2])

def p_arg(p) -> None:
    """
    Arg : Expr
        | SplatExpr
        | Rest
    """

    p[0] = p[1]

def p_arg_splat(p) -> None:
    """
    SplatExpr : Expr splat
    """

    p[0] = node.SplatExpr(p[1], _range_between(p[1], p[2]))

def p_rest(p) -> None:
    """
    Rest : splat
    """

    p[0] = node.Rest(p[1].range)

def p_arg_list_arg_empty(p) -> None:
    """
    ArgList : Empty
    """

    p[0] = []

def p_arg_list_arg_arg(p) -> None:
    """
    ArgList : Arg
    """

    p[0] = [p[1]]

def p_arg_list_arg_comma(p) -> None:
    """
    ArgList : ArgList ',' Arg
    """

    p[0] = p[1] + [p[3]]

def p_arg_list_arg_terminator(p) -> None:
    """
    ArgList : ArgList terminator Arg
    """

    p[0] = p[1] + [p[3]]

def p_arg_list_indent(p) -> None:
    """
    ArgList : indent ArgList OptComma outdent
    """

    p[0] = p[2]

def p_incl_span_array(p) -> None:
    """
    Array : '[' Expr span Expr ']'
    """

    p[0] = node.InclSpanArray(p[2], p[4], _range_between(p[1], p[5]))

def p_excl_span_array(p) -> None:
    """
    Array : '[' SplatExpr Expr ']'
    """

    p[0] = node.ExclSpanArray(p[2].expr, p[3], _range_between(p[1], p[4]))

def p_array(p) -> None:
    """
    Array : '[' ArgList OptComma ']'
    """

    p[0] = node.Array(p[2])

def p_id(p) -> None:
    """
    Id : id
    """

    p[0] = node.Id(p[1].value, p[1].range)

def p_property(p) -> None:
    """
    Property : property
    """

    p[0] = _Prototype(p[1].range) if p[1].value == "prototype" else node.String(p[1].value, p[1].range)

def p_assignment(p) -> None:
    """
    Assign : Assignable '=' Expr
           | Assignable add_assign Expr
           | Assignable bit_and_assign Expr
           | Assignable bit_or_assign Expr
           | Assignable bit_xor_assign Expr
           | Assignable exist_assign Expr
           | Assignable exp_assign Expr
           | Assignable float_div_assign Expr
           | Assignable integer_div_assign Expr
           | Assignable left_shift_assign Expr
           | Assignable mod_assign Expr
           | Assignable mul_assign Expr
           | Assignable rem_assign Expr
           | Assignable sign_right_shift_assign Expr
           | Assignable sub_assign Expr
           | Assignable zero_fill_right_shift_assign Expr
    """

    p[0] = _assign_map[p[2].type](p[1], p[3])

def p_assignable(p) -> None:
    """
    Assignable : Value
               | Assign
    """

    p[0] = p[1]

def p_string(p) -> None:
    """
    String : string
    """

    p[0] = node.String(p[1].content, p[1].range)

def p_regex(p) -> None:
    """
    Regex : regex
    """

    p[0] = node.Regex(p[1].value, p[1].delimiter, p[1].range)

def p_number(p) -> None:
    """
    Number : number
    """

    p[0] = node.Number(p[1].value, p[1].range)

def p_nan(p) -> None:
    """
    Number : nan
    """

    p[0] = node.Nan(p[1].range)

def p_infinity(p) -> None:
    """
    Number : infinity
    """

    p[0] = node.Infinity(p[1].range)

def p_null(p) -> None:
    """
    Null : null
    """

    p[0] = node.Null(p[1].range)

def p_void(p) -> None:
    """
    Void : void
    """

    p[0] = node.Void(p[1].range)

def p_boolean_true(p) -> None:
    """
    Boolean : true
    """

    p[0] = node.True_(p[1].range)

def p_boolean_false(p) -> None:
    """
    Boolean : false
    """

    p[0] = node.False_(p[1].range)

def p_literal(p) -> None:
    """
    Literal : String
            | Regex
            | Number
            | Boolean
            | Null
            | Void
            | This
            | At
    """

    p[0] = p[1]

def p_call_start(p) -> None:
    """
    CallStart : whitespace_call_start
              | paren_call_start
    """

    p[0] = p[1]

def p_call_end(p) -> None:
    """
    CallEnd : whitespace_call_end
            | paren_call_end
    """

    p[0] = p[1]

def p_call(p) -> None:
    """
    Call : Expr CallStart ArgList OptComma CallEnd
    """

    p[0] = node.Call(p[1], p[3], _range_between(p[1], p[5]))

def p_super_call(p) -> None:
    """
    Call : super CallStart ArgList OptComma CallEnd
    """

    p[0] = node.SuperCall(p[3], _range_between(p[1], p[5]))

def p_callable_new_value(p) -> None:
    """
    New : new Expr
    """

    call = p[2] if isinstance(p[2], node.Call) else node.Call(p[2], ())

    p[0] = node.New(call.expr, call.args)

def p_accessor(p) -> None:
    """
    Accessor : Expr '.' Property
    """

    p[0] = node.Accessor(expr=p[1], key=p[3].to_string(), range=_range_between(p[1], p[3]))

def p_index_accessor(p) -> None:
    """
    Accessor : Expr index_start Expr index_end
    """

    p[0] = node.Accessor(expr=p[1], key=p[3], range=_range_between(p[1], p[4]))

def p_index_excl_slice_copy(p) -> None:
    """
    Slice : Expr index_start splat index_end
    """

    p[0] = node.ExclSliceExpr(p[1], node.Void(), node.Void(), _range_between(p[1], p[4]))

def p_index_incl_slice_copy(p) -> None:
    """
    Slice : Expr index_start span index_end
    """

    p[0] = node.InclSliceExpr(p[1], node.Void(), node.Void(), _range_between(p[1], p[4]))

def p_index_excl_slice_from(p) -> None:
    """
    Slice : Expr index_start Expr splat index_end
    """

    p[0] = node.ExclSliceExpr(p[1], p[3], node.Void(), _range_between(p[1], p[5]))

def p_index_incl_slice_from(p) -> None:
    """
    Slice : Expr index_start Expr span index_end
    """

    p[0] = node.InclSliceExpr(p[1], p[3], node.Void(), _range_between(p[1], p[5]))

def p_index_excl_slice_to(p) -> None:
    """
    Slice : Expr index_start splat Expr index_end
    """

    p[0] = node.ExclSliceExpr(p[1], node.Void(), p[4], _range_between(p[1], p[5]))

def p_index_incl_slice_to(p) -> None:
    """
    Slice : Expr index_start span Expr index_end
    """

    p[0] = node.InclSliceExpr(p[1], node.Void(), p[4], _range_between(p[1], p[5]))

def p_index_excl_slice_between(p) -> None:
    """
    Slice : Expr index_start Expr splat Expr index_end
    """

    p[0] = node.ExclSliceExpr(p[1], p[3], p[5], _range_between(p[1], p[6]))

def p_index_incl_slice_between(p) -> None:
    """
    Slice : Expr index_start Expr span Expr index_end
    """

    p[0] = node.InclSliceExpr(p[1], p[3], p[5], _range_between(p[1], p[6]))

def p_exist_accessor(p) -> None:
    """
    Accessor : Expr dot_exist Property
    """

    p[0] = node.ExistAccessor(expr=p[1], key=p[3], range=_range_between(p[1], p[3]))

def p_prototype_accessor(p) -> None:
    """
    Accessor : '@' Prototype Empty
    """

    message = (
        '"@::" cannot be used to access the "prototype" property, use "@prototype" instead'
    )

    raise SyntaxException(message, p[1])

def p_prototype_property_accessor(p) -> None:
    """
    Accessor : '@' Prototype Property
    """

    expr = node.Accessor(node.This(p[1].range), p[2].to_string(), _range_between(p[1], p[2]))

    p[0] = node.Accessor(expr=expr, key=p[3].to_string(), range=_range_between(p[1], p[3]))

def p_at_prototype_accessor(p) -> None:
    """
    Accessor : Expr Prototype Empty
    """

    message = (
        '"::" cannot be used to access the "prototype" property, use ".prototype" instead'
    )

    raise SyntaxException(message, p[2])

def p_at_property(p) -> None:
    """
    AtAccessor : '@' Property
    """

    p[0] = node.Accessor(node.This(p[1].range), p[2].to_string())

def p_at_property_index(p) -> None:
    """
    AtAccessor : '@' index_start Expr index_end
    """

    p[0] = node.Accessor(node.This(p[1].range), p[3], _range_between(p[1], p[4]))

def p_accessor_at_property(p) -> None:
    """
    Accessor : AtAccessor
    """

    p[0] = p[1]

def p_this(p) -> None:
    """
    This : this
    """

    p[0] = node.This(p[1].range)

def p_prototype(p) -> None:
    """
    Prototype : prototype
    """

    p[0] = _Prototype(p[1].range)

def p_at_error(p) -> None:
    """
    At : '@'
    """

    message = '"@" must be followed by a property name, use "this" instead'

    raise SyntaxException(message, p[1])


this_as_property_accessor_error_message = (
    '"this" cannot be used to access a property, use "@" followed by a property name instead'
)

this_as_prototype_accessor_error_message = (
    '"this.prototype" cannot be used to access the "prototype" object, use "@prototype" instead'
)

def p_accessor_this_error(p) -> None:
    """
    Accessor : This '.' Property
    """

    if p[3].type == _Prototype.type:
        raise SyntaxException(this_as_prototype_accessor_error_message, p[1])

    raise SyntaxException(this_as_property_accessor_error_message, p[1])

def p_accessor_this_index_error(p) -> None:
    """
    Accessor : This index_start Expr index_end
    """

    raise SyntaxException('"this" cannot be used to access a property, use "@" instead', p[1])

def p_accessor_this_prototype_error(p) -> None:
    """
    Accessor : This Prototype Empty
    """

    message = (
        '"this::" cannot be used to access the "prototype" object, use "@prototype" instead'
    )

    raise SyntaxException(message, p[1])

def p_accessor_at_error(p) -> None:
    """
    Accessor : '@' '.' Property
    """

    if p[3].type == _Prototype.type:
        message = (
            '"@.prototype" cannot be used to access the "prototype" object, '
            'use "@prototype" instead'
        )

        raise SyntaxException(message, p[2])

    raise SyntaxException('"@" must be followed by a property name, remove "."', p[1])

def p_accessor_super(p) -> None:
    """
    Accessor : super '.' Property
    """

    p[0] = node.Accessor(node.Super(p[1].range), p[3].to_string())

def p_accessor_super_property_of_prototype(p) -> None:
    """
    Accessor : super Prototype Property
    """

    expr = node.Accessor(node.Super(p[1].range), p[2].to_string(), _range_between(p[1], p[2]))

    p[0] = node.Accessor(expr=expr, key=p[3].to_string(), range=_range_between(p[1], p[3]))

def p_accessor_property_of_prototype(p) -> None:
    """
    Accessor : Expr Prototype Property
    """

    expr = node.Accessor(p[1], p[2].to_string(), _range_between(p[1], p[2]))

    p[0] = node.Accessor(expr=expr, key=p[3].to_string(), range=_range_between(p[1], p[3]))

def p_accessor_super_prototype_error(p) -> None:
    """
    Accessor : super Prototype Empty
    """

    message = (
        '"super::" cannot be used to access the "prototype" object, use "super.prototype" instead'
    )

    raise SyntaxException(message, p[2])

def p_value(p) -> None:
    """
    Value : Literal
          | Object
          | Array
          | Accessor
          | Id
    """

    p[0] = p[1]

_COMPARATORS = {it.op: it for it in node.Cmp.__subclasses__()}

def p_cmp_single(p) -> None:
    """
    Cmp : is Expr
        | isnt Expr
        | '<' Expr
        | '>' Expr
        | greater_than_or_equal_to Expr
        | less_than_or_equal_to Expr
    """

    p[0] = (_COMPARATORS[p[1].value](p[2]),)

def p_cmp_expr_binary(p) -> None:
    """
    CmpExpr : Expr Cmp
    """

    p[0] = node.CompExpr(*(p[1].left, p[1].cmps + p[2]) if isinstance(p[1], node.CompExpr) else (p[1], p[2]))

def p_exist_expr(p) -> None:
    """
    ExistExpr : Expr '?'
    """

    p[0] = node.ExistExpr(p[1], _range_between(p[1], p[2]))

def p_delete_expr(p) -> None:
    """
    DeleteExpr : delete Expr
    """

    if not isinstance(p[2], node.Accessor):
        raise SyntaxException(f'"{p[2].type}" cannot be deleted', p[2])

    p[0] = node.DeleteExpr(key=p[2].key, expr=p[2].expr, range=_range_between(p[1], p[2]))

_UN_OPERATORS = {it.type: it for it in node.UnExpr.__subclasses__()}

def p_splat_expr_this_property(p) -> None:
    """
    SplatExpr : Id splat
    """

    p[0] = node.SplatExpr(p[1], _range_between(p[1], p[2]))

def p_splat_expr_accessor(p) -> None:
    """
    SplatExpr : Accessor splat
    """

    p[0] = node.SplatExpr(p[1], _range_between(p[1], p[2]))

def p_prefix_inc(p) -> None:
    """
    Expr : inc Expr
    """

    p[0] = node.PrefixIncExpr(p[2], _range_between(p[1], p[2]))

def p_prefix_dec(p) -> None:
    """
    Expr : dec Expr
    """

    p[0] = node.PrefixDecExpr(p[2], _range_between(p[1], p[2]))

def p_postfix_inc(p) -> None:
    """
    Expr : Expr inc
    """

    p[0] = node.PostfixIncExpr(p[1], _range_between(p[1], p[2]))

def p_postfix_dec(p) -> None:
    """
    Expr : Expr dec
    """

    p[0] = node.PostfixDecExpr(p[1], _range_between(p[1], p[2]))

def p_un_expr(p) -> None:
    """
    UnExpr : not Expr
           | '~' Expr
           | typeof Expr
           | '-' Expr %prec UADD
           | '+' Expr %prec UADD
    """

    p[0] = _UN_OPERATORS[p[1].value](p[2])

_disallowed_on_left_of_exp = dict_by_type(
    node.BitNotExpr,
    node.NotExpr,
    node.TypeOf,
)

def p_exp_expr(p) -> None:
    """
    ExpExpr : Expr exp Expr
    """

    if p[1].type in _disallowed_on_left_of_exp:
        raise SyntaxException("unparenthesized unary expression can't appear on the left-hand side of '**'", p[1])

    p[0] = node.ExpExpr(p[1], p[3], _range_between(p[1], p[3]))

_BIN_OPERATORS = {it.type: it for it in node.BinExpr.__subclasses__()}

def p_bin_expr(p) -> None:
    """
    BinExpr : Expr '+' Expr
            | Expr '-' Expr
            | Expr '*' Expr
            | Expr '/' Expr
            | Expr '%' Expr
            | Expr '&' Expr
            | Expr '|' Expr
            | Expr '^' Expr
            | Expr and Expr
            | Expr bin_exist Expr
            | Expr div Expr
            | Expr in Expr
            | Expr instanceof Expr
            | Expr left_shift Expr
            | Expr mod Expr
            | Expr of Expr
            | Expr or Expr
            | Expr sign_right_shift Expr
            | Expr zero_fill_right_shift Expr
    """

    p[0] = _BIN_OPERATORS[p[2].type](p[1], p[3], _range_between(p[1], p[3]))

def p_not_relation(p) -> None:
    """
    BinExpr : Expr not instanceof Expr
            | Expr not in Expr
            | Expr not of Expr
    """

    p[0] = node.NotExpr(_BIN_OPERATORS[p[3].type](p[1], p[4], _range_between(p[1], p[4])), _range_between(p[1], p[4]))

def p_paren_expr(p) -> None:
    """
    ParenExpr : '(' Expr ')'
    """

    p[0] = p[2]

def p_paren_indent(p) -> None:
    """
    ParenExpr : '(' indent Expr outdent ')'
    """

    p[0] = p[3]

def p_interpolation_expr_single(p) -> None:
    """
    InterpolationChain : Expr
    """

    p[0] = (p[1],)

def p_interpolation_expr_chain(p) -> None:
    """
    InterpolationChain : InterpolationChain concat Expr
    """

    p[0] = p[1] + (p[3],)

def p_string_interpolation_empty(p) -> None:
    """
    Interpolation : string_start string_end
    """

    p[0] = node.StringInterpolation(p[1].value, (), _range_between(p[1], p[2]))

def p_string_interpolation(p) -> None:
    """
    String : string_start InterpolationChain string_end
    """

    p[0] = node.StringInterpolation(p[1].value, p[2], _range_between(p[1], p[3]))

def p_regex_interpolataion(p) -> None:
    """
    Interpolation : regex_start InterpolationChain regex_end
    """

    p[0] = node.RegexInterpolation(p[2], _range_between(p[1], p[3]))

def p_regex_interpolataion_empty(p) -> None:
    """
    Interpolation : regex_start regex_end
    """

    p[0] = node.RegexInterpolation((), _range_between(p[1], p[2]))

def p_expression(p) -> None:
    """
    Expr : Assignable
         | BinExpr
         | Call
         | CmpExpr
         | DeleteExpr
         | ExistExpr
         | ExpExpr
         | Function
         | IfExpr
         | Interpolation
         | New
         | ParenExpr
         | PostIf
         | UnExpr
         | Class
         | Slice
    """

    p[0] = p[1]

def p_return(p) -> None:
    """
    Return : return Expr
    """

    p[0] = node.Return(p[2], _range_between(p[1], p[2]))

def p_return_void(p) -> None:
    """
    Return : return Empty
    """

    p[0] = node.Return(node.Void(Range(p[1].range.end, p[1].range.end)), p[1].range)

def p_throw(p) -> None:
    """
    Throw : throw Expr
    """

    p[0] = node.Throw(p[2], _range_between(p[1], p[2]))

def p_post_unless(p) -> None:
    """
    PostIf : Stmt post_unless Expr
           | Expr post_unless Expr
    """

    block = node.Block([p[1]], p[1].range)
    p[0] = node.Unless(p[3], block, node.Block([]), _range_between(block, p[3]))


def p_post_if(p) -> None:
    """
    PostIf : Stmt post_if Expr
           | Expr post_if Expr
    """

    block = node.Block([p[1]], p[1].range)
    p[0] = node.If(p[3], block, node.Block([]), _range_between(block, p[3]))

def p_if(p) -> None:
    """
    IfExpr : if Expr Block else Block
    """

    p[0] = node.If(p[2], p[3], p[5], _range_between(p[1], p[5]))

def p_unless(p) -> None:
    """
    IfExpr : unless Expr Block else Block
    """

    p[0] = node.Unless(p[2], p[3], p[5], _range_between(p[1], p[5]))

def p_statement(p) -> None:
    """
    Stmt : Return
         | Throw
    """

    p[0] = p[1]

def p_body_empty(p) -> None:
    """
    Body : Empty
    """

    p[0] = node.Block([])

def p_body_single(p) -> None:
    """
    Body : Expr
         | Stmt
    """

    p[0] = node.Block([p[1]])

def p_body_multi(p) -> None:
    """
    Body : Body terminator Expr
         | Body terminator Stmt
    """

    p[0] = node.Block(p[1].statements + [p[3]])

def p_block_indent(p) -> None:
    """
    Block : indent Body outdent
    """

    p[0] = p[2]

def p_block_line(p) -> None:
    """
    Block : Expr
          | Stmt
    """

    p[0] = node.Block([p[1]])

def p_block_empty(p) -> None:
    """
    Block : Empty
    """

    p[0] = node.Block([])

_ClassHeader = NamedTuple("ClassHeader", [("name", node.Id), ("parent", node.Id), ("range", Range)])

def p_class_header_anonymous(p) -> None:
    """
    ClassHeader : class Empty
    """

    p[0] = _ClassHeader(node.Void(), node.Void(), _range_between(p[1], p[1]))

def p_class_header_anonymous_extends(p) -> None:
    """
    ClassHeader : class extends Expr
    """

    p[0] = _ClassHeader(node.Void(), p[3], _range_between(p[1], p[3]))

def p_class_header(p) -> None:
    """
    ClassHeader : class Id Empty
    """

    p[0] = _ClassHeader(p[2], node.Void(), _range_between(p[1], p[2]))

def p_class_header_extends(p) -> None:
    """
    ClassHeader : class Id extends Expr
    """

    p[0] = _ClassHeader(p[2], p[4], _range_between(p[1], p[4]))

def p_class(p) -> None:
    """
    Class : ClassHeader indent ClassBody outdent
    """

    p[0] = node.Class(p[1].name, p[1].parent, p[3].props, node.Block(p[3].stmts))

def p_class_empty(p) -> None:
    """
    Class : ClassHeader Empty
    """

    p[0] = node.Class(p[1].name, p[1].parent, (), node.Block([]), _range_between(p[1], p[1]))

_ClassBody = NamedTuple("ClassBody", [("block", node.Block), ("props", List[node.Prop]), ("range", Range)])
_ClassProps = NamedTuple("ClassProps", [("props", Tuple[node.Prop]), ("range", Range)])

def p_class_prop(p) -> None:
    """
    ClassProp : ObjKey ':' Expr
    """

    p[0] = node.Prop(p[1], p[3])

_ClassMember = NamedTuple("ClassHeader", [("props", Tuple[node.Prop]), ("stmts", Tuple[node.Stmt])])

def p_class_body_member_prop(p) -> None:
    """
    ClassBodyMember : ClassProp
    """

    p[0] = _ClassMember(props=(p[1],), stmts=())

def p_class_body_member_stmt(p) -> None:
    """
    ClassBodyMember : Stmt
                    | Expr
    """

    p[0] = _ClassMember(props=(), stmts=(p[1],))

def p_class_body_single(p) -> None:
    """
    ClassBody : ClassBodyMember
    """

    p[0] = p[1]

def p_class_body_chain(p) -> None:
    """
    ClassBody : ClassBody terminator ClassBodyMember
    """

    p[0] = _ClassMember(stmts=p[1].stmts + p[3].stmts, props=p[1].props + p[3].props)

def p_error(p) -> None:
    # TODO: Handle end of input
    raise SyntaxException(f'unexpected token "{p.type}"', p.value)

class _Atom:
    def __init__(self, atom):
        self._atom = atom

    @property
    def type(self):
        return self._atom.type

    @property
    def value(self):
        return self._atom

    def __str__(self):
        return str(self._atom)

class _Scanner:
    def __init__(self, atoms):
        self._index = -1
        self._atoms = atoms
        self._count = len(atoms)

    def token(self):
        self._index += 1

        if self._index < self._count:
            return _Atom(self._atoms[self._index])

parser = yacc.yacc()

def parse(atoms):
    return parser.parse(lexer=_Scanner(atoms))
