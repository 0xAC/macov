import re
from macov.core.util import LexicalException
from macov.core.atom import Number
from macov.core.util import Range
from macov.util import trampoline

_REGEXPES = {
    "b": re.compile("[0-1]"),
    "o": re.compile("[0-7]"),
    "x": re.compile(r"[\da-fA-F]"),
}

_DECIMAL_REGEXP = re.compile(r"\d")

def scan_number_start(state, atoms):
    found, new_state, value = _scan_number(state, "")

    new_atoms, final_state = ([Number(value, Range(state.position, new_state.position))], new_state) if found else ([], state)

    return found, final_state, atoms.extend(new_atoms)

@trampoline.wrap
def _scan_digits(state, value, regexp):
    if regexp.match(state.char):
        return trampoline.call(_scan_digits)(state.next, value + state.char, regexp)

    return trampoline.result((state, value))

def _scan_number_non_decimal(state, value):
    if state.char in _REGEXPES:
        return _scan_digits(state.next, value + state.char, _REGEXPES[state.char])

    return state, value

def _scan_decimal_integer_start(state):
    if _DECIMAL_REGEXP.match(state.char):
        new_state, value = _scan_digits(state.next, state.char, _DECIMAL_REGEXP)

        return new_state, value

    return state, ""

def _scan_decimal_fraction(state):
    if state.char != ".":
        return state, ""

    new_state, number_value = _scan_decimal_integer_start(state.next)

    if not number_value:
        return state, ""

    return new_state, "." + number_value

def _scan_decimal_e(state):
    if state.char not in ["e", "E"]:
        return state, ""

    sign_state = state.next

    number_state, sign = (sign_state.next, sign_state.char) if sign_state.char in ["+", "-"] else (sign_state, "")

    final_state, number = _scan_decimal_integer_start(number_state)

    if not number:
        raise LexicalException("expected a digit but found '{}'".format(final_state.char), sign_state.position)

    return final_state, state.char + sign + number

def _scan_number(state, value):
    if state.char == "0" and state.next.char != ".":
        new_state, new_value = _scan_number_non_decimal(state.next, value + state.char)

        return True, new_state, new_value

    decimal_state, decimal_value = _scan_decimal_integer_start(state)
    fraction_state, fraction_value = _scan_decimal_fraction(decimal_state)
    e_state, e_value = _scan_decimal_e(fraction_state) if fraction_value or decimal_value else (fraction_state, "")

    final_value = decimal_value + fraction_value + e_value

    return final_value != "", e_state, value + final_value
