from macov.core import atom, node
from macov.core.util import Position, Range, SyntaxException
from macov.reader import parse
from typing import List
import unittest

class ParserRootTest(unittest.TestCase):
    def test_unexpected_token(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Id("bar", range=Range(Position(row=1, column=3), Position(row=1, column=4))),
        ]

        expected_message = '^unexpected token "id" at 1,3'

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_empty(self) -> None:
        atoms: List[atom.Atom] = []

        self.assertEqual(
            parse(atoms),
            node.Block([])
        )

    def test_multiple_statements(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Terminator(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Id("foo"),
                node.Id("bar"),
            ]),
        )

    def test_return_statement(self) -> None:
        atoms = [
            atom.Return(),
            atom.Id("foo"),
            atom.Add(),
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Return(
                    node.AddExpr(
                        node.Id("foo"),
                        node.MulExpr(
                            node.Id("bar"),
                            node.Id("baz"),
                        )
                    )
                )
            ])
        )

    def test_return_void(self) -> None:
        atoms = [
            atom.Return(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Return(
                    node.Void()
                )
            ])
        )

    def test_return_index_accessor(self) -> None:
        atoms = [
            atom.Return(),
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Id("bar"),
            atom.IndexEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Return(
                    node.Accessor(
                        node.Id("foo"),
                        node.Id("bar")
                    )
                )
            ])
        )

    def test_delete(self) -> None:
        atoms = [
            atom.Delete(),
            atom.Id("foo"),
            atom.Dot(),
            atom.Property("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.DeleteExpr(
                    node.Id("foo"),
                    node.String("bar")
                ),
            ])
        )

    def test_delete_with_non_accessor(self) -> None:
        atoms = [
            atom.Delete(),
            atom.Id("foo", range=Range(Position(1, 3), Position(1, 4))),
        ]

        expected_message = '^"id" cannot be deleted at 1,3'

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_throw(self) -> None:
        atoms = [
            atom.Throw(),
            atom.New(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Throw(
                    node.New(
                        node.Id("foo"),
                        [
                        ]
                    )
                ),
            ])
        )

    def test_type_of(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.TypeOf(),
            atom.Id("bar"),
            atom.WhitespaceCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.TypeOf(
                            node.Id("bar")
                        ),
                    ]
                ),
            ])
        )

    def test_brackets_accessor(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Id("bar"),
            atom.IndexEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_at_accessor(self) -> None:
        atoms = [
            atom.At(),
            atom.Property("foo"),
            atom.WhitespaceCallStart(),
            atom.At(),
            atom.Property("bar"),
            atom.WhitespaceCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Accessor(
                        node.This(),
                        node.String("foo")
                    ),
                    [
                        node.Accessor(
                            node.This(),
                            node.String("bar")
                        ),
                    ]
                ),
            ])
        )

    def test_at_index_accessor(self) -> None:
        atoms = [
            atom.At(),
            atom.IndexStart(),
            atom.Id("foo"),
            atom.IndexEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.This(),
                    node.Id("foo")
                ),
            ])
        )

    def test_at_prototype_with_property_accessor(self) -> None:
        atoms = [
            atom.At(),
            atom.Prototype(),
            atom.Property("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Accessor(
                        node.This(),
                        node.String("prototype"),
                    ),
                    node.String("foo")
                ),
            ])
        )

    def test_id_prototype_with_property_accessor(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Prototype(),
            atom.Property("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Accessor(
                        node.Id("foo"),
                        node.String("prototype")
                    ),
                    node.String("bar")
                ),
            ])
        )

    def test_at_prototype_property_accessor(self) -> None:
        atoms = [
            atom.At(Range(Position(2, 3), Position(2, 4))),
            atom.Property("prototype"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.This(),
                    node.String("prototype"),
                ),
            ])
        )

    def test_at_prototype_accessor(self) -> None:
        atoms = [
            atom.At(Range(Position(2, 3), Position(2, 4))),
            atom.Prototype(),
        ]

        expected_message = (
            r'^"@::" cannot be used to access the "prototype" property, use "@prototype" '
            r'instead at 2,3'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_id_prototype_accessor(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Prototype(Range(Position(2, 3), Position(2, 4))),
        ]

        expected_message = (
            r'^"::" cannot be used to access the "prototype" property, use ".prototype" '
            r'instead at 2,3'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_at_dot_prototype_accessor(self) -> None:
        atoms = [
            atom.At(),
            atom.Dot(Range(Position(2, 3), Position(2, 5))),
            atom.Property("prototype"),
        ]

        expected_message = (
            r'^"@\.prototype" cannot be used to access the "prototype" object, '
            'use "@prototype" instead at 2,3'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_id_dot_prototype_accessor(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Dot(Range(Position(2, 3), Position(2, 5))),
            atom.Property("prototype"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Id("foo"),
                    node.String("prototype")
                )
            ])
        )

    def test_this_prototype_accessor(self) -> None:
        atoms = [
            atom.This(Range(Position(2, 3), Position(2, 5))),
            atom.Prototype(),
        ]

        expected_message = (
            r'^"this::" cannot be used to access the "prototype" object, '
            r'use "@prototype" instead at 2,3'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_this_dot_prototype_accessor(self) -> None:
        atoms = [
            atom.This(Range(Position(2, 3), Position(2, 5))),
            atom.Dot(),
            atom.Property("prototype"),
        ]

        expected_message = (
            r'^"this\.prototype" cannot be used to access the "prototype" object, '
            r'use "@prototype" instead at 2,3'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_super_prototype_accessor(self) -> None:
        atoms = [
            atom.Super(),
            atom.Prototype(Range(Position(2, 3), Position(2, 5))),
        ]

        expected_message = (
            r'^"super::" cannot be used to access the "prototype" object, '
            'use "super.prototype" instead at 2,3'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_super_dot_prototype_accessor(self) -> None:
        atoms = [
            atom.Super(),
            atom.Dot(),
            atom.Property("prototype"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Super(),
                    node.String("prototype"),
                )
            ])
        )

    def test_super_property_of_prototype_accessor(self) -> None:
        atoms = [
            atom.Super(),
            atom.Prototype(),
            atom.Property("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Accessor(
                        node.Super(),
                        node.String("prototype")
                    ),
                    node.String("foo")
                )
            ])
        )

class ParserObjectTest(unittest.TestCase):
    def test_object_empty(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([]),
            ])
        )

    def test_object_one_short_property(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                ]),
            ])
        )

    def test_object_at_short_property(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.At(),
            atom.Property("foo"),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(
                        node.String("foo"),
                        node.Accessor(
                            node.This(),
                            node.String("foo")
                        )
                    ),
                ]),
            ])
        )

    def test_object_one_short_property_opt_comma(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.Comma(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                ]),
            ])
        )

    def test_object_two_short_props(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Id("bar"),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                    node.Prop(node.String("bar"), node.Id("bar")),
                ]),
            ])
        )

    def test_object_two_short_props_opt_comma(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Id("bar"),
            atom.Comma(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                    node.Prop(node.String("bar"), node.Id("bar")),
                ]),
            ])
        )

    def test_object_indent_one_short_property_outdent(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Outdent(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                ]),
            ])
        )

    def test_object_indent_two_comma_separated_short_properties_outdent(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Id("bar"),
            atom.Outdent(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                    node.Prop(node.String("bar"), node.Id("bar")),
                ]),
            ])
        )

    def test_object_indent_two_comma_separated_short_properties_outdent_opt_comma(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Id("bar"),
            atom.Comma(),
            atom.Outdent(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                    node.Prop(node.String("bar"), node.Id("bar")),
                ]),
            ])
        )

    def test_object_indent_two_terminator_separated_short_properties_outdent(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Terminator(),
            atom.Id("bar"),
            atom.Outdent(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                    node.Prop(node.String("bar"), node.Id("bar")),
                ]),
            ])
        )

    def test_object_with_string_key_property(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.String("foo"),
            atom.Colon(),
            atom.Id("foo"),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.Id("foo")),
                ]),
            ])
        )

    def test_object_with_interpolated_string_key(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.StringStart('"'),
            atom.String("foo "),
            atom.Concat(),
            atom.Id("bar"),
            atom.Concat(),
            atom.String(" baz"),
            atom.StringEnd('"'),
            atom.Colon(),
            atom.Id("qux"),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(
                        node.StringInterpolation(
                            '"',
                            [
                                node.String("foo "),
                                node.Id("bar"),
                                node.String(" baz"),
                            ]
                        ),
                        node.Id("qux")
                    ),
                ]),
            ])
        )

    def test_object_with_regular_property(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.Colon(),
            atom.String("foo"),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(node.String("foo"), node.String("foo")),
                ]),
            ])
        )

    def test_object_with_indents(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Colon(),
            atom.Indent(),
            atom.Id("bar"),
            atom.Colon(),
            atom.Id("baz"),
            atom.Outdent(),
            atom.Outdent(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(
                        node.String("foo"),
                        node.Object([
                            node.Prop(
                                node.String("bar"),
                                node.Id("baz")
                            )
                        ])
                    ),
                ])
            ])
        )

    def test_object_with_indents_opt_comma(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Colon(),
            atom.Indent(),
            atom.Id("bar"),
            atom.Colon(),
            atom.Id("baz"),
            atom.Comma(),
            atom.Outdent(),
            atom.Outdent(),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(
                        node.String("foo"),
                        node.Object([
                            node.Prop(
                                node.String("bar"),
                                node.Id("baz")
                            )
                        ])
                    ),
                ])
            ])
        )

class ParserArrayTest(unittest.TestCase):
    def test_array_empty(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                ]),
            ])
        )

    def test_array_inclusive_span(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.Span(),
            atom.Id("bar"),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InclSpanArray(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_array_exclusive_span(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.Splat(),
            atom.Id("bar"),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExclSpanArray(
                    node.Id("foo"),
                    node.Id("bar")
                )
            ])
        )

    def test_array_one_element(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.String("foo"),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                    node.String("foo"),
                ]),
            ])
        )

    def test_array_opt_comma(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.String("foo"),
            atom.Comma(),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                    node.String("foo"),
                ]),
            ])
        )

    def test_array_two_element(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.String("foo"),
            atom.Comma(),
            atom.String("bar"),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                    node.String("foo"),
                    node.String("bar"),
                ]),
            ])
        )

    def test_array_two_element_opt_comma(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.String("foo"),
            atom.Comma(),
            atom.String("bar"),
            atom.Comma(),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                    node.String("foo"),
                    node.String("bar"),
                ]),
            ])
        )

    def test_array_indent_one_element_outdent(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Indent(),
            atom.String("foo"),
            atom.Outdent(),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                    node.String("foo"),
                ]),
            ])
        )

    def test_array_indent_two_comma_separated_elements_outdent(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Indent(),
            atom.String("foo"),
            atom.Comma(),
            atom.String("bar"),
            atom.Outdent(),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                    node.String("foo"),
                    node.String("bar"),
                ]),
            ])
        )

    def test_array_indent_two_comma_separated_elements_outdent_opt_comma(self) -> None:
        atoms = [
            atom.LeftBracket(),
            atom.Indent(),
            atom.String("foo"),
            atom.Comma(),
            atom.String("bar"),
            atom.Comma(),
            atom.Outdent(),
            atom.RightBracket(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Array([
                    node.String("foo"),
                    node.String("bar"),
                ]),
            ])
        )

class ParserExpressionTest(unittest.TestCase):
    def test_identifier(self) -> None:
        atoms = [
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Id("foo")
            ])
        )

    def test_this(self) -> None:
        atoms = [
            atom.This(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.This(),
            ])
        )

    def test_at(self) -> None:
        atoms = [
            atom.At()
        ]

        expected_message = r'"@" must be followed by a property name, use "this" instead'

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_super(self) -> None:
        atoms = [
            atom.Super(),
            atom.ParenCallStart(),
            atom.Id("bar"),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SuperCall([
                    node.Id("bar"),
                ]),
            ])
        )

    def test_at_property(self) -> None:
        atoms = [
            atom.At(),
            atom.Property("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.This(),
                    node.String("foo"),
                )
            ])
        )

    def test_super_property(self) -> None:
        atoms = [
            atom.Super(),
            atom.Dot(),
            atom.Property("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Super(),
                    node.String("foo"),
                )
            ])
        )

    def test_at_dot_property(self) -> None:
        atoms = [
            atom.At(Range(Position(1, 2), Position(1, 3))),
            atom.Dot(),
            atom.Property("foo"),
        ]

        expected_message = (
            r'^"@" must be followed by a property name, remove "\." at 1,2'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_existence(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.Id("foo"),
                )
            ])
        )

    def test_exist_dot(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Dot(),
            atom.Property("bar"),
            atom.DotExist(),
            atom.Property("baz"),
            atom.Dot(),
            atom.Property("qux"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.ExistAccessor(
                        node.Accessor(
                            node.Id("foo"),
                            node.String("bar"),
                        ),
                        node.String("baz"),
                    ),
                    node.String("qux")
                )
            ])
        )

    def test_this_dot_property(self) -> None:
        atoms = [
            atom.This(),
            atom.Dot(),
            atom.Property("foo"),
        ]

        expected_message = (
            r'^"this" cannot be used to access a property, '
            r'use "@" followed by a property name instead'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_this_dot_property_index(self) -> None:
        atoms = [
            atom.This(Range(Position(row=1, column=4), Position(row=1, column=7))),
            atom.IndexStart(),
            atom.Id("foo"),
            atom.IndexEnd(),
        ]

        expected_message = (
            r'^"this" cannot be used to access a property, use "@" instead at 1,4'
        )

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    def test_this_is_expression(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.This(),
            atom.Comma(),
            atom.Id("bar"),
            atom.WhitespaceCallEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.This(),
                        node.Id("bar"),
                    ]
                )
            ])
        )

    def test_paren(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Mul(),
            atom.LeftParen(),
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.RightParen(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.Id("foo"),
                    node.AddExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    )
                )
            ])
        )

    def test_paren_indent(self) -> None:
        atoms = [
            atom.LeftParen(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Outdent(),
            atom.RightParen(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Id("foo"),
            ])
        )

    def test_unary_bitwise_not(self) -> None:
        atoms = [
            atom.BitNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitNotExpr(
                    node.Id("foo"),
                )
            ])
        )

    def test_unary_logical_not(self) -> None:
        atoms = [
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.NotExpr(
                    node.Id("foo"),
                )
            ])
        )

    def test_unary_splat_id(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.Id("bar"),
            atom.Splat(),
            atom.WhitespaceCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.SplatExpr(
                            node.Id("bar"),
                        )
                    ]
                )
            ])
        )

    def test_unary_splat_property(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.Id("bar"),
            atom.Dot(),
            atom.Property("baz"),
            atom.Splat(),
            atom.WhitespaceCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.SplatExpr(
                            node.Accessor(
                                node.Id("bar"),
                                node.String("baz"),
                            )
                        )
                    ]
                )
            ])
        )

    def test_unary_splat_this_property(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.At(),
            atom.Property("bar"),
            atom.Splat(),
            atom.WhitespaceCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.SplatExpr(
                            node.Accessor(
                                node.This(),
                                node.String("bar"),
                            )
                        )
                    ]
                )
            ])
        )

    def test_unary_splat_paren_expr(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.LeftParen(),
            atom.Id("bar"),
            atom.RightParen(),
            atom.Splat(),
            atom.WhitespaceCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.SplatExpr(
                            node.Id("bar"),
                        )
                    ]
                )
            ])
        )

    def test_unary_splat_call_expr(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.Id("bar"),
            atom.ParenCallStart(),
            atom.ParenCallEnd(),
            atom.Splat(),
            atom.WhitespaceCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.SplatExpr(
                            node.Call(
                                node.Id("bar"),
                                [
                                ]
                            )
                        )
                    ]
                )
            ])
        )

    def test_dot_accessor(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Dot(),
            atom.Property("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Id("foo"),
                    node.String("bar"),
                )
            ])
        )

    def test_dot_accessor_after_call(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.WhitespaceCallStart(),
            atom.Id("bar"),
            atom.WhitespaceCallEnd(),
            atom.Dot(),
            atom.Property("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Accessor(
                    node.Call(
                        node.Id("foo"),
                        [
                            node.Id("bar"),
                        ]
                    ),
                    node.String("baz"),
                )
            ])
        )

    def test_regex(self) -> None:
        atoms = [
            atom.Regex("foo", delimiter="/")
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Regex("foo", "/"),
            ])
        )

    def test_empty_regex_interpolation(self) -> None:
        atoms = [
            atom.RegexStart(),
            atom.RegexEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RegexInterpolation([])
            ])
        )

    def test_string_interpolation_nested_double_quote(self) -> None:
        atoms = [
            atom.StringStart('"'),
            atom.String("foo "),
            atom.Concat(),
            atom.StringStart('"'),
            atom.String("bar "),
            atom.Concat(),
            atom.Id("baz"),
            atom.Concat(),
            atom.String(" qux"),
            atom.StringEnd('"'),
            atom.Concat(),
            atom.String(" goo"),
            atom.StringEnd('"'),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.StringInterpolation(
                    '"',
                    [
                        node.String("foo "),
                        node.StringInterpolation(
                            '"',
                            [
                                node.String("bar "),
                                node.Id("baz"),
                                node.String(" qux"),
                            ]
                        ),
                        node.String(" goo"),
                    ]
                ),
            ])
        )

    def test_string_interpolation_empty(self) -> None:
        atoms = [
            atom.StringStart('"'),
            atom.StringEnd('"'),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.StringInterpolation(
                    '"',
                    [
                    ]
                ),
            ])
        )

    def test_number_literal(self) -> None:
        atoms = [
            atom.Number("123")
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Number("123"),
            ])
        )

    def test_null_literal(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Is(),
            atom.Null(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("foo"),
                    [
                        node.IsCmp(
                            node.Null()
                        ),
                    ]
                ),
            ])
        )

    def test_undefined_literal(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Is(),
            atom.Void(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("foo"),
                    [
                        node.IsCmp(
                            node.Void()
                        ),
                    ]
                ),
            ])
        )

    def test_post_if(self) -> None:
        atoms = [
            atom.Return(),
            atom.Id("foo"),
            atom.PostIf(),
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.If(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.LessThanCmp(
                                node.Id("baz")
                            )
                        ]
                    ),
                    node.Block([
                        node.Return(
                            node.Id("foo")
                        )
                    ]),
                    node.Block([])
                ),
            ])
        )

    def test_post_if_expression(self) -> None:
        atoms = [
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.Colon(),
            atom.Id("bar"),
            atom.WhitespaceCallStart(),
            atom.Id("baz"),
            atom.WhitespaceCallEnd(),
            atom.PostIf(),
            atom.Id("qux"),
            atom.RightBrace(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Object([
                    node.Prop(
                        node.String("foo"),
                        node.If(
                            node.Id("qux"),
                            node.Block([
                                node.Call(
                                    node.Id("bar"),
                                    [
                                        node.Id("baz"),
                                    ]
                                ),
                            ]),
                            node.Block([])
                        )
                    ),
                ]),
            ])
        )

    def test_post_if_assignment(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Assign(),
            atom.Id("bar"),
            atom.PostIf(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.If(
                    node.Id("baz"),
                    node.Block([
                        node.AssignExpr(
                            node.Id("foo"),
                            node.Id("bar")
                        )
                    ]),
                    node.Block([])
                ),
            ])
        )

    def test_post_if_delete(self) -> None:
        atoms = [
            atom.Delete(),
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Id("bar"),
            atom.IndexEnd(),
            atom.PostIf(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.If(
                    node.Id("baz"),
                    node.Block([
                        node.DeleteExpr(
                            node.Id("foo"),
                            node.Id("bar")
                        )
                    ]),
                    node.Block([])
                )
            ])
        )

    def test_post_if_associavity_right(self) -> None:
        atoms = [
            atom.Return(),
            atom.Id("foo"),
            atom.PostIf(),
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.PostIf(),
            atom.Id("qux"),
            atom.GreaterThan(),
            atom.Id("goo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.If(
                    node.CompExpr(
                        node.Id("qux"),
                        [
                            node.GreaterThanCmp(
                                node.Id("goo")
                            )
                        ]
                    ),
                    node.Block([
                        node.If(
                            node.CompExpr(
                                node.Id("bar"),
                                [
                                    node.LessThanCmp(
                                        node.Id("baz")
                                    )
                                ]
                            ),
                            node.Block([
                                node.Return(
                                    node.Id("foo")
                                )
                            ]),
                            node.Block([])
                        ),
                    ]),
                    node.Block([
                    ])
                )
            ])
        )

    def test_post_unless(self) -> None:
        atoms = [
            atom.Return(),
            atom.Id("foo"),
            atom.PostUnless(),
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Unless(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.LessThanCmp(
                                node.Id("baz")
                            )
                        ]
                    ),
                    node.Block([
                        node.Return(
                            node.Id("foo")
                        )
                    ]),
                    node.Block([])
                ),
            ])
        )

    def test_post_unless_associavity_right(self) -> None:
        atoms = [
            atom.Return(),
            atom.Id("foo"),
            atom.PostUnless(),
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.PostUnless(),
            atom.Id("qux"),
            atom.GreaterThan(),
            atom.Id("goo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Unless(
                    node.CompExpr(
                        node.Id("qux"),
                        [
                            node.GreaterThanCmp(
                                node.Id("goo")
                            )
                        ]
                    ),
                    node.Block([
                        node.Unless(
                            node.CompExpr(
                                node.Id("bar"),
                                [
                                    node.LessThanCmp(
                                        node.Id("baz")
                                    )
                                ]
                            ),
                            node.Block([
                                node.Return(
                                    node.Id("foo")
                                )
                            ]),
                            node.Block([])
                        ),
                    ]),
                    node.Block([])
                )
            ])
        )

    def test_post_if_with_return_accessor_experssion(self) -> None:
        atoms = [
            atom.Return(),
            atom.Id("foo"),
            atom.Dot(),
            atom.Property("bar"),
            atom.PostIf(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.If(
                    node.Id("baz"),
                    node.Block([
                        node.Return(
                            node.Accessor(
                                node.Id("foo"),
                                node.String("bar"),
                            )
                        )
                    ]),
                    node.Block([])
                )
            ])
        )

    def test_unary_minus(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MinusExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_unary_plus(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PlusExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_binary_exp(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Exp(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_mul(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_float_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_integer_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_rem(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_mod(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_logical_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.And(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AndExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_or(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitOr(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitOrExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_logical_or(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Or(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OrExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_xor(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitXor(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitXorExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_not_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Not(),
            atom.In(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.NotExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    )
                ),
            ])
        )

    def test_binary_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_not_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Not(),
            atom.Of(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.NotExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    )
                ),
            ])
        )

    def test_binary_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_not_instanceof(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Not(),
            atom.InstanceOf(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.NotExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    )
                )
            ])
        )

    def test_bin_exist(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BinExist(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BinExistExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_compare_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_greater_than_or_equal(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_less_than_or_equal(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(node.Id("baz"))
                    ]
                )
            ])
        )

class ParserClassTest(unittest.TestCase):
    def test_class_empty(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                    ]
                ),
            ])
        )

    def test_class_anonymous(self) -> None:
        atoms = [
            atom.Class()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Void(),
                    node.Void(),
                    [
                    ]
                ),
            ])
        )

    def test_class_with_constructor(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Indent(),
            atom.Id("constructor"),
            atom.Colon(),
            atom.ParamStart(),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("constructor"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_class_with_multiple_properties(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Indent(),
            atom.Id("constructor"),
            atom.Colon(),
            atom.ParamStart(),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Terminator(),
            atom.Id("bar"),
            atom.Colon(),
            atom.ParamStart(),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("constructor"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                        node.Prop(
                            node.String("bar"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_class_extends(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Extends(),
            atom.Id("Bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Id("Bar"),
                    [
                    ]
                ),
            ])
        )

    def test_class_anonymous_extends(self) -> None:
        atoms = [
            atom.Class(),
            atom.Extends(),
            atom.Id("Foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Void(),
                    node.Id("Foo"),
                    [
                    ]
                ),
            ])
        )

    def test_class_with_before_block(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Indent(),
            atom.Id("bar"),
            atom.WhitespaceCallStart(),
            atom.This(),
            atom.WhitespaceCallEnd(),
            atom.Terminator(),
            atom.Id("baz"),
            atom.Colon(),
            atom.ThinArrow(),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("baz"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ],
                    node.Block([
                        node.Call(
                            node.Id("bar"),
                            [
                                node.This()
                            ]
                        ),
                    ])
                ),
            ])
        )

    def test_class_with_after_block(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Indent(),
            atom.Id("baz"),
            atom.Colon(),
            atom.ThinArrow(),
            atom.Terminator(),
            atom.Id("bar"),
            atom.WhitespaceCallStart(),
            atom.This(),
            atom.WhitespaceCallEnd(),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("baz"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ],
                    node.Block([
                        node.Call(
                            node.Id("bar"),
                            [
                                node.This()
                            ]
                        ),
                    ])
                ),
            ])
        )

    def test_class_with_before_and_after_block(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Indent(),
            atom.Id("bar"),
            atom.WhitespaceCallStart(),
            atom.This(),
            atom.WhitespaceCallEnd(),
            atom.Terminator(),
            atom.Id("baz"),
            atom.Colon(),
            atom.ThinArrow(),
            atom.Terminator(),
            atom.Id("qux"),
            atom.WhitespaceCallStart(),
            atom.This(),
            atom.WhitespaceCallEnd(),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("baz"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ],
                    node.Block([
                        node.Call(
                            node.Id("bar"),
                            [
                                node.This()
                            ]
                        ),
                        node.Call(
                            node.Id("qux"),
                            [
                                node.This()
                            ]
                        ),
                    ])
                ),
            ])
        )

    def test_class_with_before_block_only(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Indent(),
            atom.Id("bar"),
            atom.WhitespaceCallStart(),
            atom.This(),
            atom.WhitespaceCallEnd(),
            atom.Terminator(),
            atom.Id("baz"),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                    ],
                    node.Block([
                        node.Call(
                            node.Id("bar"),
                            [
                                node.This()
                            ]
                        ),
                        node.Id("baz"),
                    ])
                ),
            ])
        )

    def test_class_with_constructor_no_paren(self) -> None:
        atoms = [
            atom.Class(),
            atom.Id("Foo"),
            atom.Indent(),
            atom.Id("constructor"),
            atom.Colon(),
            atom.ThinArrow(),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("constructor"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ]
                ),
            ])
        )

class SliceParserTest(unittest.TestCase):
    def test_inclusive_copy(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Span(),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InclSliceExpr(
                    node.Id("foo"),
                    node.Void(),
                    node.Void()
                )
            ])
        )

    def test_inclusive_from(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Id("bar"),
            atom.Span(),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InclSliceExpr(
                    node.Id("foo"),
                    node.Id("bar"),
                    node.Void()
                )
            ])
        )

    def test_inclusive_to(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Span(),
            atom.Id("bar"),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InclSliceExpr(
                    node.Id("foo"),
                    node.Void(),
                    node.Id("bar")
                )
            ])
        )

    def test_inclusive_between(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Id("bar"),
            atom.Span(),
            atom.Id("baz"),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InclSliceExpr(
                    node.Id("foo"),
                    node.Id("bar"),
                    node.Id("baz")
                )
            ])
        )

    def test_exclusive_copy(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Splat(),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExclSliceExpr(
                    node.Id("foo"),
                    node.Void(),
                    node.Void()
                )
            ])
        )

    def test_exclusive_from(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Id("bar"),
            atom.Splat(),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExclSliceExpr(
                    node.Id("foo"),
                    node.Id("bar"),
                    node.Void()
                )
            ])
        )

    def test_exclusive_to(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Splat(),
            atom.Id("bar"),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExclSliceExpr(
                    node.Id("foo"),
                    node.Void(),
                    node.Id("bar")
                )
            ])
        )

    def test_exclusive_between(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.IndexStart(),
            atom.Id("bar"),
            atom.Splat(),
            atom.Id("baz"),
            atom.IndexEnd()
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExclSliceExpr(
                    node.Id("foo"),
                    node.Id("bar"),
                    node.Id("baz")
                )
            ])
        )

if __name__ == "__main__":
    unittest.main()
