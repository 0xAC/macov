from macov.core import node
from macov.transform.func_util import read_ids
import unittest

class PrinterTest(unittest.TestCase):
    def test_empty(self) -> None:
        input = node.Function(
            [
            ],
            node.Block([
            ])
        )

        self.assertEqual([], read_ids(input))

    def test_single(self) -> None:
        input = node.Function(
            [
                node.Param(node.Id("foo")),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("foo")], read_ids(input))

    def test_many(self) -> None:
        input = node.Function(
            [
                node.Param(node.Id("foo")),
                node.Param(node.Id("bar")),
                node.Param(node.Id("baz")),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("foo"), node.Id("bar"), node.Id("baz")], read_ids(input))

    def test_object_empty(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Object([
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([], read_ids(input))

    def test_object_single_prop(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Object([
                        node.Prop(node.String("foo"), node.Id("bar")),
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("bar")], read_ids(input))

    def test_object_multiple_props(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Object([
                        node.Prop(node.String("foo"), node.Id("bar")),
                        node.Prop(node.String("baz"), node.Id("qux")),
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("bar"), node.Id("qux")], read_ids(input))

    def test_object_nested(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Object([
                        node.Prop(
                            node.String("foo"),
                            node.Object([
                                node.Prop(node.String("foo"), node.Id("bar")),
                            ])
                        ),
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("bar")], read_ids(input))

    def test_array_empty(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Array([
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([], read_ids(input))

    def test_array_single(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Array([
                        node.Id("foo"),
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("foo")], read_ids(input))

    def test_array_multiple(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Array([
                        node.Id("foo"),
                        node.Id("bar"),
                        node.Id("baz"),
                    ])
                ),
            ],
            node.Block([
            ])
        )

    def test_array_nested(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Array([
                        node.Array([
                            node.Id("foo"),
                            node.Id("bar"),
                            node.Id("baz"),
                        ]),
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("foo"), node.Id("bar"), node.Id("baz")], read_ids(input))

    def test_array_of_objects(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Array([
                        node.Object([
                            node.Prop(node.String("foo"), node.Id("bar")),
                            node.Prop(node.String("baz"), node.Id("qux")),
                        ])
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("bar"), node.Id("qux")], read_ids(input))

    def test_object_of_arrays(self) -> None:
        input = node.Function(
            [
                node.Param(
                    node.Object([
                        node.Prop(
                            node.String("foo"),
                            node.Array([node.Id("baz")]),
                        ),
                    ])
                ),
            ],
            node.Block([
            ])
        )

        self.assertEqual([node.Id("baz")], read_ids(input))
