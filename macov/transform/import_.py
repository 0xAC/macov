from typing import Callable, List, Tuple, TypeVar
from macov.core import node

_Prepared = Tuple[int, node.Param, str]

_ParepareParam = Callable[
    [
        List[node.String],
        List[node.Param],
    ],
    _Prepared,
]

_T = TypeVar("_T")

def _is_empty_object(expr: node.Expr) -> bool:
    if isinstance(expr, node.Object) and not list(expr.props):
        return True

    if isinstance(expr, node.Array) and not list(expr.items):
        return True

    return False

def _set_at(array: List[_T], value: _T, index: int) -> List[_T]:
    return array[:index] + [value] + array[index + 1:]

def _is_symbol(expr: node.Expr, symbol: str) -> bool:
    return isinstance(expr, node.Id) and expr.name == symbol

def _remove_symbol_from_object(object: node.Object, symbol: str) -> node.Object:
    updated_props = (prop.set_value(_remove_symbol_from_expr(prop.value, symbol)) for prop in object.props)
    return object.set_props(prop for prop in updated_props if not _is_empty_object(prop.value))

def _remove_symbol_from_array(array: node.Array, symbol: str) -> node.Array:
    updated_items = (_remove_symbol_from_expr(item, symbol) for item in array.items)
    return array.set_items(item for item in updated_items if not _is_empty_object(item))

def _remove_symbol_from_expr(expr: node.Expr, symbol: str) -> bool:
    if _is_symbol(expr, symbol):
        return node.Object([])

    if isinstance(expr, node.Object):
        return _remove_symbol_from_object(expr, symbol)

    if isinstance(expr, node.Array):
        return _remove_symbol_from_array(expr, symbol)

    return expr

def _remove_symbol_from_param(param: node.Param, symbol: str) -> bool:
    return param.set_value(_remove_symbol_from_expr(param.value, symbol))

def _remove_symbol_from_params(params: List[node.Param], symbol: str) -> int:
    return [_remove_symbol_from_param(param, symbol) for param in params]

def import_prop_as(invocation: node.Call, module_path: str, prop_path: str, symbol: str) -> node.Call:
    def prepare_param(items, params):
        return _prepare_object_to_set(items, params, module_path, prop_path.split("."), symbol)

    return _import_as(invocation, module_path, prepare_param)

def import_module_as(invocation: node.Call, module_path: str, symbol: str) -> node.Call:
    # in case the symbol already exists we need to remove it first
    invocation_without_symbol = remove_import(invocation, symbol)

    def prepare_param(items, params):
        return _prepare_id_to_set(items, params, module_path, symbol)

    return _import_as(invocation_without_symbol, module_path, prepare_param)

def remove_import(invocation: node.Call, symbol: str) -> node.Call:
    array, function = invocation.args
    items, params = array.items, function.params

    updated_params = _remove_symbol_from_params(params, symbol)
    indices_to_remove = [i for i, param in enumerate(updated_params) if _is_empty_object(param.value)]

    new_array = array.set_items(item for i, item in enumerate(items) if not i in indices_to_remove)
    new_function = function.set_params(param for i, param in enumerate(updated_params) if i not in indices_to_remove)

    return invocation.set_args([new_array, new_function])

def _import_as(invocation: node.Call, module_path: str, prepare_param: _ParepareParam) -> node.Call:
    array, function = invocation.args
    items, params = array.items, function.params

    index, param, path = prepare_param(items, params)

    new_array = array.set_items(_set_at(items, node.String(path), index))
    new_function = function.set_params(_set_at(params, param, index))

    return invocation.set_args([new_array, new_function])

def _prepare_id_to_set(items: List[node.String], params: List[node.Param], module_path: str, symbol: str) -> _Prepared:
    index = next((index for index, item in enumerate(items) if item.value == module_path), len(items))

    return index, node.Param(node.Id(symbol)), module_path

def _prepare_object_to_set(
    items: List[node.String],
    params: List[node.Param],
    module_path: str,
    prop_path_items: List[str],
    symbol: str
) -> _Prepared:
    pairs = zip(items, params)

    index, object = next(
        ((index, param.value) for index, (item, param) in enumerate(pairs) if item.value == module_path and param.value.is_(node.Object)),
        (len(items), node.Object([]))
    )

    assert isinstance(object, node.Object)

    return index, node.Param(_set_prop(object, prop_path_items, symbol)), module_path

def _set_prop(object: node.Object, prop_path_items: List[str], symbol: str) -> node.Object:
    head, *tail = prop_path_items

    key = node.String(head)

    if not tail:
        return object.set_prop(key, node.Id(symbol))

    prop = object.get_prop(key, node.Object([]))

    return object.set_prop(node.String(head), _set_prop(prop, tail, symbol))
