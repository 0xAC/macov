import functools

def call(h):
    f = h.func

    def g(*args, **kwds):
        return f, args, kwds

    return g

def result(value):
    return None, value, None

def _wrap(f):
    @functools.wraps(f)
    def g(*args, **kwds):
        h = f

        while h is not None:
            h, args, kwds = h(*args, **kwds)

        return args

    return g

def wrap(f):
    g = _wrap(f)
    g.func = f

    return g
