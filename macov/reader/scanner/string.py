from macov.core import atom
from macov.core.util import Position, Range
from macov.reader.scanner.util import State, Raw, is_block_delimiter
from macov.util import trampoline
from pyrsistent import dq
from typing import List, Optional, Tuple

def scan_string_start(state: State, atoms: List[atom.Atom]) -> Tuple[bool, State, List[atom.Atom]]:
    if state.char in ['"', "'"]:
        new_state, new_atoms = _scan_string_atoms(state)

        return True, new_state, atoms.extend(new_atoms)

    return False, state, atoms

def _append_to_string(string: atom.String, state: State) -> atom.String:
    new_value = string.value + state.char
    new_range = Range(string.range.begin, state.position)

    return atom.String(new_value, string.delimiter, new_range)

def _create_empty_string(state: State) -> atom.String:
    range = Range.from_cursor(state)
    return atom.String("", "", range)

def _create_end_delimiter(start_delimiter: atom.StringStart, state: State) -> atom.StringEnd:
    range = Range(state.position, state.jump_by(start_delimiter.count).position)

    return atom.StringEnd(start_delimiter.char, start_delimiter.count, range)

def _join_pieces(atoms: List[atom.Atom], start: atom.StringStart, end: atom.StringEnd) -> atom.String:
    return atom.String("".join(it.value for it in atoms), start.value, Range(start.range.begin, end.range.end))

@trampoline.wrap
def _concat_pieces(atoms: List[atom.Atom], result: List[atom.Atom]=[]) -> List[atom.Atom]:
    if not atoms:
        return trampoline.result(result)

    head, tail = atoms.left, atoms.popleft()

    if not result:
        return trampoline.call(_concat_pieces)(tail, dq(head))

    position = result[-1].range.end
    concat = atom.Concat(Range(position, position))

    return trampoline.call(_concat_pieces)(tail, result.extend((concat, head)))

def _scan_string_atoms(state: State) -> Tuple[State, List[atom.Atom]]:
    count = 3 if is_block_delimiter(state) else 1

    start_delimiter = atom.StringStart(state.char, count, Range(state.position, state.jump_by(count).position))
    start_from_cursor = state.jump_by(count)
    inital_string = _create_empty_string(start_from_cursor)

    new_state, pieces = _scan_string(start_from_cursor, inital_string, start_delimiter)
    end_delimiter = _create_end_delimiter(start_delimiter, new_state)

    if not any(isinstance(atom, Raw) for atom in pieces):
        return new_state.jump_by(end_delimiter.count), dq(_join_pieces(pieces, start_delimiter, end_delimiter))

    return new_state.jump_by(end_delimiter.count), _concat_pieces(pieces).appendleft(start_delimiter).append(end_delimiter)

def _is_delimiter(delimiter: atom.StringStart, state: State) -> bool:
    if state.char != delimiter.char:
        return False

    if delimiter.count == 1:
        return True

    return is_block_delimiter(state)

@trampoline.wrap
def _scan_string(
    state: State,
    current: atom.String,
    delimiter: atom.StringStart,
    escape: bool=False,
    result: List[atom.Atom]=dq()
) -> Tuple[State, List[atom.Atom]]:
    if not escape and _is_delimiter(delimiter, state):
        new_result = result.extend(dq(current) if current.value != "" else dq())

        return trampoline.result((state, new_result))

    (new_state, raw) = _process_interpolation(state, escape)

    if raw:
        new_result = result.extend((current,) if current.value != "" else ()).append(raw)

        return trampoline.call(_scan_string)(new_state, _create_empty_string(new_state), delimiter, False, new_result)

    new_escape = state.char == "\\" and not escape
    return trampoline.call(_scan_string)(state.next, _append_to_string(current, state), delimiter, new_escape, result)

def _scan_interpolation_start(state: State, start: Position, text: str) -> Tuple[State, Optional[Raw]]:
    if state.char == "{":
        return _scan_interpolation(state.next, start, text + state.char)

    return state, None

def _process_interpolation(state: State, escape: bool) -> Tuple[State, Optional[Raw]]:
    if state.char == "#" and not escape:
        new_state, maybe_atom = _scan_interpolation_start(state.next, state.position, state.char)

        if maybe_atom:
            return new_state, maybe_atom

    return state, None

@trampoline.wrap
def _scan_interpolation(state: State, start: Position, text: str, level: int=1) -> Tuple[State, Raw]:
    def update_level():
        if state.char == "}":
            return level - 1

        if state.char == "{":
            return level + 1

        return level

    if not level:
        return trampoline.result((state, Raw(text[2:-1], position=Position(column=start.column + 2, row=start.row))))

    new_level = update_level()

    found, new_state, strings = scan_string_start(state, dq())

    (new_text, final_state) = ("".join(it.value for it in strings), new_state) if found else (state.char, state.next)

    return trampoline.call(_scan_interpolation)(final_state, start, text + new_text, new_level)
