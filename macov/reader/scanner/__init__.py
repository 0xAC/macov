from macov.core.atom import *
from macov.core.util import compose, dict_by_type
from macov.reader.scanner import operator
from macov.reader.scanner.call import promote_parens_to_calls, add_missing_calls
from macov.reader.scanner.comment import scan_comment_start
from macov.reader.scanner.cond import add_inline_indent_outdent_for_then_else, replace_expressions_with_statements
from macov.reader.scanner.group import fold, unfold, Group
from macov.reader.scanner.number import scan_number_start
from macov.reader.scanner.param import promote_parens_to_params
from macov.reader.scanner.index import promote_bracket_to_indices
from macov.reader.scanner.string import scan_string_start
from macov.reader.scanner.terminator import filter_unwanted_terminators, filter_terminators_before_dot
from macov.reader.scanner.util import PROPERTY_STARTERS, CALLABLE, state, EOF, Raw, Then, atom_set
from macov.util import trampoline
from functools import partial
from pyrsistent import dq, l, plist
from typing import List, NamedTuple

class _Yes(True_):
    "This is for internal scanner use only"

    @property
    def value(self):
        return "yes"

class _No(False_):
    "This is for internal scanner use only"

    @property
    def value(self):
        return "no"

class _On(True_):
    "This is for internal scanner use only"

    @property
    def value(self):
        return "on"

class _Off(False_):
    "This is for internal scanner use only"

    @property
    def value(self):
        return "off"

_STRIP_CALL_BEFORE = atom_set(
    And,
    Else,
    Extends,
    For,
    In,
    InstanceOf,
    Is,
    IsNot,
    Loop,
    Of,
    Or,
    Return,
    Then,
    Until,
    While,
)

_unary_math = atom_set(
    Add,
    Sub,
)

_KEYWORDS = {
    "@": At,
    "Infinity": Infinity,
    "NaN": Nan,
    "and": And,
    "async": Async,
    "await": Await,
    "break": Break,
    "catch": Catch,
    "class": Class,
    "continue": Continue,
    "delete": Delete,
    "else": Else,
    "extends": Extends,
    "false": False_,
    "finally": Finally,
    "for": For,
    "if": If,
    "in": In,
    "instanceof": InstanceOf,
    "is": Is,
    "isnt": IsNot,
    "loop": Loop,
    "new": New,
    "no": _No,
    "not": Not,
    "null": Null,
    "of": Of,
    "off": _Off,
    "on": _On,
    "or": Or,
    "return": Return,
    "super": Super,
    "switch": Switch,
    "then": Then,
    "this": This,
    "throw": Throw,
    "true": True_,
    "try": Try,
    "typeof": TypeOf,
    "undefined": Void,
    "unless": Unless,
    "until": Until,
    "when": When,
    "while": While,
    "yes": _Yes,
    "yield": Yield,
}

@trampoline.wrap
def _verify_disallowed_aliases(atoms):
    if not atoms:
        return trampoline.result(None)

    head, tail = atoms.first, atoms.rest

    if isinstance(head, Bool) and head.value in {"yes", "no", "on", "off"}:
        message = "literal '{}' is not allowed, use '{}' instead".format(head.value, head.type)
        raise LexicalException(message, head.range.begin)

    return trampoline.call(_verify_disallowed_aliases)(tail)

def _verify_disallowed_aliases_start(atoms):
    _verify_disallowed_aliases(atoms)

    return atoms

@trampoline.wrap
def _scan_whitespace(state):
    if state.char == "#":
        _, new_state, _ = scan_comment_start(state, dq())

        return trampoline.call(_scan_whitespace)(new_state)

    if not state.char.isspace():
        return trampoline.result(state)

    return trampoline.call(_scan_whitespace)(state.next)

@trampoline.wrap
def _update_delimiter_stack_for_outdents(state, outdents):
    if not outdents:
        return trampoline.result(state)

    return trampoline.call(_update_delimiter_stack_for_outdents)(state.pop_delimiter(outdents.first), outdents.rest)

def _handle_end_of_line(previous_state, current_state):
    if (current_state.column - 1) % 4 != 0:
        raise LexicalException("indentation is not multiple of 4", current_state.position)

    current_indent = int((current_state.column - 1) / 4)

    if current_indent == previous_state.indent:
        return current_state, [Terminator(Range.from_cursor(previous_state))]

    updated_state = current_state.update_indent(current_indent)

    if current_indent > previous_state.indent:
        if current_indent - previous_state.indent > 1:
            raise LexicalException("double indent", current_state.position)

        indent = Indent(Range.from_cursor(current_state))

        return updated_state.push_delimiter(indent), [indent]

    terminator = Terminator(Range.from_cursor(previous_state))
    outdents = (previous_state.indent - current_indent) * [Outdent(Range.from_cursor(previous_state))]
    return _update_delimiter_stack_for_outdents(updated_state, plist(outdents)), outdents + [terminator]

def _update_indent(previous_state, current_state, atoms):
    if current_state.row == previous_state.row:
        return current_state, atoms

    final_atoms = _filter_trailing_whitespace_call_start(atoms)

    new_state, end_of_line_tokens = _handle_end_of_line(previous_state, current_state)

    return new_state, final_atoms.extend(end_of_line_tokens)

def _scan_whitespace_start(state, atoms):
    if not state.char.isspace():
        return False, state, atoms

    new_state = _scan_whitespace(state.next)

    if new_state.row == state.row and atoms and atoms.right.type != Exist.type and atoms.right.type in CALLABLE:
        return True, new_state, atoms.append(WhitespaceCallStart(Range(state.position, new_state.position)))

    updated_state, updated_atoms = _update_indent(state, new_state, atoms)

    return True, updated_state, updated_atoms

def _scan_id(state, name, start_position):
    if state.char.isalpha() or state.char == "_" or state.char.isdigit():
        return _scan_id(state.next, name + state.char, start_position)

    return state, Id(name, Range(start_position, state.position))

def _transform_id(id, atoms):
    atom_class = _KEYWORDS.get(id.value)

    if atoms and atoms.right.type in PROPERTY_STARTERS:
        return id.to_property()

    return atom_class(id.range) if atom_class else id

def _post_process_inline_block_start(keyword, tokens):
    return dq(InlineIndent(keyword.range)) if tokens and (isinstance(tokens.right, InlineOutdent) or not isinstance(tokens.right, Outdent)) else dq()

def _scan_id_start(state, atoms):
    if not state.char.isalpha() and state.char not in {"$", "_"}:
        return False, state, atoms

    new_state, id = _scan_id(state.next, state.char, state.position)

    stripped_atoms = _filter_trailing_whitespace_call_start(atoms) if isinstance(id, Operator) else atoms
    atom = _transform_id(id, stripped_atoms)

    no_trailing_call_atoms = _filter_trailing_whitespace_call_start(stripped_atoms) if atom.type in _STRIP_CALL_BEFORE else stripped_atoms

    return True, new_state, no_trailing_call_atoms.append(atom)

_SIMPLE_OPERATORS = dict_by_type(
    At,
    BitNot,
    Comma,
)

_OPENING_DELIMITERS = dict_by_type(
    LeftBracket,
    LeftBrace,
    LeftParen,
)

_CLOSING_DELIMITERS = dict_by_type(
    RightBracket,
    RightBrace,
    RightParen,
)

_OPERATORS_MAP = {
    "!": operator.scan_exclamination_mark_start,
    "%": operator.scan_percent_start,
    "&": operator.scan_ampersand_start,
    "*": operator.scan_asterisk_start,
    "+": operator.scan_plus_start,
    "-": operator.scan_dash_start,
    ".": operator.scan_dot_start,
    "/": operator.scan_slash_start,
    ":": operator.scan_colon_start,
    "<": operator.scan_less_than_start,
    "=": operator.scan_equal_to_start,
    ">": operator.scan_greater_than_start,
    "^": operator.scan_caret_start,
    "|": operator.scan_pipeline_start,
    "?": operator.scan_question_mark_start,
}

@trampoline.wrap
def _filter_trailing_whitespace_call_start(tokens):
    if not tokens:
        return trampoline.result(tokens)

    if tokens.right.type == Not.type:
        return trampoline.result(_filter_trailing_whitespace_call_start(tokens.pop()).append(tokens.right))

    if tokens.right.type != WhitespaceCallStart.type:
        return trampoline.result(tokens)

    return trampoline.call(_filter_trailing_whitespace_call_start)(tokens.pop())

def _downgrade_keyword_to_id(head, atoms):
    if head.type == Colon.type and atoms and isinstance(atoms.right, Keyword):
        return atoms.pop().append(atoms.right.to_id())

    return atoms

def _add_call_start_after_exist(atoms):
    head = atoms.right

    return atoms.append(ImplicitCallStart(head.range)) if head.type == Exist.type else atoms

def _scan_operator(state, atoms):
    position = state.position

    def handle_arrow_operator(maybe_arrow, atoms):
        if maybe_arrow.type not in _unary_math | {ThinArrow.type, ThickArrow.type, Regex.type}:
            return _filter_trailing_whitespace_call_start(atoms)

        return atoms

    if state.char == "/":
        next_state = state.next

        if next_state.char == "/":
            further_state = next_state.next

            if further_state.char == "/":
                new_state, new_atoms = operator.scan_block_regex_start(further_state.next, state.position)

                return True, new_state, atoms.extend(new_atoms)

    if state.char in _OPERATORS_MAP:
        new_state, new_atom = _OPERATORS_MAP[state.char](state.next, position, atoms=atoms)

        new_atoms = handle_arrow_operator(new_atom, atoms)
        final_atoms = _downgrade_keyword_to_id(new_atom, new_atoms).append(new_atom)

        return True, new_state, _add_call_start_after_exist(final_atoms)

    atom_class = _SIMPLE_OPERATORS.get(state.char, None)
    if atom_class:
        atom = atom_class(Range(position, position))

        filtered_atoms = _filter_trailing_whitespace_call_start(atoms) if atom.type not in {At.type, BitNot.type} else atoms

        return True, state.next, filtered_atoms.append(atom)

    atom_class = _CLOSING_DELIMITERS.get(state.char, None)
    if atom_class:
        final_atoms = _filter_trailing_whitespace_call_start(atoms)

        closing_delimiter = atom_class(Range(position, position))

        return True, state.pop_delimiter(closing_delimiter).next, final_atoms.append(closing_delimiter)

    atom_class = _OPENING_DELIMITERS.get(state.char, None)
    if atom_class:
        opening_delimiter = atom_class(Range(position, position))
        return True, state.push_delimiter(opening_delimiter).next, atoms.append(opening_delimiter)

    return False, state, atoms

_RULES = [
    _scan_whitespace_start,
    scan_comment_start,
    _scan_id_start,
    scan_number_start,
    scan_string_start,
    _scan_operator,
]

@trampoline.wrap
def _try_rules(rules, state, atoms):
    if not rules:
        return trampoline.result((dq(), state))

    rule, *rest_rules = rules

    found, new_state, extended_atoms = rule(state, atoms)

    if found:
        return trampoline.result((extended_atoms, new_state))

    return trampoline.call(_try_rules)(rest_rules, new_state, atoms)

@trampoline.wrap
def _scan_input(state, atoms=dq()):
    if state.char == EOF:
        outdents = [Outdent(Range(state.position, state.position))] * state.indent
        return trampoline.result(atoms.append(Terminator(Range(state.position, state.position))).extend(outdents))

    extended_atoms, new_state = _try_rules(_RULES, state, atoms)

    if state.position == new_state.position:
        raise Exception("Unexpected end of input, found: {} at {}".format(new_state.char, new_state.position))

    return trampoline.call(_scan_input)(new_state, extended_atoms)

def _first_pass(raw: Raw) -> List[Atom]:
    return _scan_input(state(raw))

_RunState = NamedTuple("_RunState", [("atoms", List[Atom]), ("result", List[Atom])])

@trampoline.wrap
def _run(atoms: List[Atom], result: List[Atom] = dq(), stack: List[_RunState] = l()):
    if not atoms:
        folded = fold(result)

        if not stack:
            return trampoline.result(folded)

        stack_head = stack.first
        new_result = stack_head.result.append(Group(folded))

        return trampoline.call(_run)(stack_head.atoms, new_result, stack.rest)

    head, tail = atoms.left, atoms.popleft()

    if head.type == Raw.type:
        return trampoline.call(_run)(_first_pass(head), dq(), stack.cons(_RunState(atoms=tail, result=result)))

    return trampoline.call(_run)(tail, result.append(head), stack)

_run_passes = compose(
    _run,
    partial(
        unfold,
        transform=compose(
            partial(plist, reverse=True),
            operator.strip_all_around_binary_math,
            filter_unwanted_terminators,
            add_inline_indent_outdent_for_then_else,
            promote_parens_to_calls,
            promote_parens_to_params,
            promote_bracket_to_indices,
            replace_expressions_with_statements,
            add_missing_calls,
            partial(plist, reverse=True),
            filter_terminators_before_dot,
        ),
    ),
    _verify_disallowed_aliases_start,
)

def scan(input: str) -> List[Atom]:
    return list(_run_passes(dq(Raw(input))))
