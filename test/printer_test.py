from macov.core import node
from macov.writer.printer import to_string
from textwrap import dedent
import unittest

class PrinterTest(unittest.TestCase):
    def test_empty(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
            ], (
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node))

    def test_one_param(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([node.String("foo")]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
                "foo"
            ], (
                foo
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node))

    def test_two_params(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                    node.String("bar"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                        node.Param(node.Id("bar")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
                "foo"
                "bar"
            ], (
                foo
                bar
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node))

    def test_object_param(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("foo"), node.Id("foo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
                "foo"
            ], (
                { foo }
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node))

    def test_object_multiprop_short_param(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("bar"), node.Id("bar")),
                                node.Prop(node.String("baz"), node.Id("baz")),
                                node.Prop(node.String("qux"), node.Id("qux")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
                "foo"
            ], (
                { bar, baz, qux }
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node, margin=50))

    def test_object_multiprop_long_param(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("bardeush"), node.Id("bardeush")),
                                node.Prop(node.String("bazdeush"), node.Id("bazdeush")),
                                node.Prop(node.String("quxdeush"), node.Id("quxdeush")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
                "foo"
            ], (
                {
                    bardeush
                    bazdeush
                    quxdeush
                }
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node, margin=30))

    def test_object_multiprop_at_margin_param(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("bardeush"), node.Id("bardeush")),
                                node.Prop(node.String("bazdeush"), node.Id("bazdeush")),
                                node.Prop(node.String("quxdeush"), node.Id("quxdeush")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
                "foo"
            ], (
                { bardeush, bazdeush, quxdeush }
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node, margin=36))

    def test_object_multiprop_right_above_margin(self) -> None:
        output_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("bardeush"), node.Id("bardeush")),
                                node.Prop(node.String("bazdeush"), node.Id("bazdeush")),
                                node.Prop(node.String("quxdeush"), node.Id("quxdeush")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_output = dedent("""
            define [
                "foo"
            ], (
                {
                    bardeush
                    bazdeush
                    quxdeush
                }
            ) ->
        """).strip()

        self.assertEqual(expected_output, to_string(output_node, margin=35))
