from macov.core import atom, node
from macov.reader import parse
import unittest

class ParserFunctionTest(unittest.TestCase):
    def test_without_parens(self) -> None:
        atoms = [
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_empty_with_parens(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_with_at_parameter(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.At(),
            atom.Property("foo"),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Accessor(
                                node.This(),
                                node.String("foo"),
                            )
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_bound(self) -> None:
        atoms = [
            atom.ThickArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                    ],
                    node.Block([
                    ]),
                    is_bound=True
                ),
            ])
        )

    def test_param_list_one_line_one_identifier(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Id("foo"),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_one_line_two_identifiers(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Id("bar"),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                        node.Param(node.Id("bar")),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_indent_outdent_one_identifier(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Outdent(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_indent_outdent_one_identifier_opt_comma(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Outdent(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_indent_two_comma_separated_identifiers_outdent(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Id("bar"),
            atom.Outdent(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                        node.Param(node.Id("bar")),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_indent_two_identifiers_outdent_terminator(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Terminator(),
            atom.Id("bar"),
            atom.Outdent(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                        node.Param(node.Id("bar")),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_indent_two_identifiers_outdent_terminator_comma(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Indent(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Terminator(),
            atom.Id("bar"),
            atom.Outdent(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                        node.Param(node.Id("bar")),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_with_object(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.LeftBrace(),
            atom.Id("foo"),
            atom.RightBrace(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("foo"), node.Id("foo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_with_array(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.RightBracket(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Array([
                                node.Id("foo"),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_with_splat_pre(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Splat(),
            atom.Comma(),
            atom.Id("foo"),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Rest(),
                        ),
                        node.Param(
                            node.Id("foo"),
                        )
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_with_splat_post(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Id("foo"),
            atom.Comma(),
            atom.Splat(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Id("foo"),
                        ),
                        node.Param(
                            node.Rest(),
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_with_assignment(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Id("foo"),
            atom.Assign(),
            atom.Id("bar"),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.AssignExpr(
                                node.Id("foo"),
                                node.Id("bar"),
                            )
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_with_splat_id_expr(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.Id("foo"),
            atom.Splat(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.SplatExpr(
                                node.Id("foo"),
                            )
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_param_list_with_splat_this_expr(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.At(),
            atom.Property("foo"),
            atom.Splat(),
            atom.ParamEnd(),
            atom.ThinArrow(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.SplatExpr(
                                node.Accessor(
                                    node.This(),
                                    node.String("foo"),
                                )
                            )
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ])
        )

    def test_body_inline(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.RightBracket(),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Array([
                                node.Id("foo"),
                            ])
                        ),
                    ],
                    node.Block([
                        node.Id("bar"),
                    ])
                ),
            ])
        )

    def test_body_block(self) -> None:
        atoms = [
            atom.ParamStart(),
            atom.LeftBracket(),
            atom.Id("foo"),
            atom.RightBracket(),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Indent(),
            atom.Id("bar"),
            atom.Outdent(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Array([
                                node.Id("foo"),
                            ])
                        ),
                    ],
                    node.Block([
                        node.Id("bar"),
                    ])
                ),
            ])
        )

if __name__ == "__main__":
    unittest.main()
