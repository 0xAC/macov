from macov.core import atom
from macov.reader.scanner.group import Group
from macov.reader.scanner.util import INDEXABLE
from macov.util import trampoline
from pyrsistent import l
from typing import List, Tuple

@trampoline.wrap
def promote_bracket_to_indices(atoms: List[atom.Atom], result: List[atom.Atom]=l()) -> List[atom.Atom]:
    if not atoms:
        return trampoline.result(result)

    head, tail = atoms.first, atoms.rest

    if head.type in INDEXABLE:
        updated_tail, updated_result = (tail.rest, result.cons(head).cons(tail.first)) if tail and _is_implicit_call_start(tail.first) else (tail, result.cons(head))

        new_atoms, new_result = _upgrade_delimiters(updated_tail, updated_result)

        return trampoline.call(promote_bracket_to_indices)(new_atoms, new_result)

    return trampoline.call(promote_bracket_to_indices)(tail, result.cons(head))

def _upgrade_delimiters(atoms: List[atom.Atom], result: List[atom.Atom]) -> Tuple[List[atom.Atom], List[atom.Atom]]:
    if not atoms:
        return atoms, result

    head, tail = atoms.first, atoms.rest

    if head.type != atom.LeftBracket.type:
        return atoms, result

    left = head

    assert tail

    inside = tail.first

    assert inside.type == Group.type
    assert tail.rest

    right = tail.rest.first

    assert right.type == head.pair

    end = atom.IndexEnd(right.range)
    start = atom.IndexStart(left.range)

    predecessor = result.first

    updated_result = result.rest if _is_implicit_call_start(predecessor) else result

    # pass call_end back to the list of tokens to detect it is indexable
    final_atoms = tail.rest.rest.cons(end)
    new_result = updated_result.cons(start).cons(inside)

    return final_atoms, new_result

def _is_implicit_call_start(head) -> bool:
    return head.type == atom.WhitespaceCallStart.type and head.kind == atom.ImplicitCallStart.kind
