import click
from macov.core.util import LexicalException, SyntaxException
from macov.reader import parser, scanner

@click.group()
def cli():
    pass

@cli.command()
@click.argument("file", type=click.Path(exists=True))
def lint(file):
    with open(file, "r") as f:
        code = f.read()

    try:
        scanner.scan(code)
    except LexicalException as e:
        click.echo('error: {} in file "{}" on line {}, col {}'.format(e.message, file, e.position.row, e.position.column))


@cli.command()
@click.argument("file", type=click.Path(exists=True))
def parse(file):
    with open(file, "r") as f:
        code = f.read()

    try:
        parser.parse(scanner.scan(code))
    except (SyntaxException, LexicalException) as e:
        click.echo('error: {} in file "{}" on line {}, col {}'.format(e.message, file, e.position.row, e.position.column))

if __name__ == "__main__":
    cli()
