from macov.core import atom
from macov.reader.scanner.group import Group, fold, unfold
import unittest

class GroupTest(unittest.TestCase):
    def test_fold(self):
        atoms = (
            atom.Id("a"),
            atom.Id("b"),
            atom.Id("c"),
            atom.LeftParen(),
            atom.Id("d"),
            atom.Id("e"),
            atom.LeftParen(),
            atom.Id("f"),
            atom.Id("g"),
            atom.RightParen(),
            atom.Id("h"),
            atom.RightParen(),
            atom.Id("i"),
            atom.Id("j"),
        )

        self.assertEqual(
            tuple(fold(atoms)),
            (
                atom.Id("j"),
                atom.Id("i"),
                atom.RightParen(),
                Group((
                    atom.Id("h"),
                    atom.RightParen(),
                    Group((
                        atom.Id("g"),
                        atom.Id("f"),
                    )),
                    atom.LeftParen(),
                    atom.Id("e"),
                    atom.Id("d"),
                )),
                atom.LeftParen(),
                atom.Id("c"),
                atom.Id("b"),
                atom.Id("a"),
            )
        )

    def test_unfold(self):
        group = (
            atom.Id("j"),
            atom.Id("i"),
            atom.RightParen(),
            Group((
                atom.Id("h"),
                atom.RightParen(),
                Group((
                    atom.Id("g"),
                    atom.Id("f"),
                )),
                atom.LeftParen(),
                atom.Id("e"),
                atom.Id("d"),
            )),
            atom.LeftParen(),
            atom.Id("c"),
            atom.Id("b"),
            atom.Id("a"),
        )

        self.assertEqual(
            tuple(unfold(group)),
            (
                atom.Id("a"),
                atom.Id("b"),
                atom.Id("c"),
                atom.LeftParen(),
                atom.Id("d"),
                atom.Id("e"),
                atom.LeftParen(),
                atom.Id("f"),
                atom.Id("g"),
                atom.RightParen(),
                atom.Id("h"),
                atom.RightParen(),
                atom.Id("i"),
                atom.Id("j"),
            )
        )

if __name__ == "__main__":
    unittest.main()
