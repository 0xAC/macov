from macov.transform import sort
from macov.core import node
import unittest

class SortTest(unittest.TestCase):
    def test_empty(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input))

    def test_one_symbol(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input))

    def test_two_symbols(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                    node.String("bar"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                        node.Param(node.Id("bar")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("bar"),
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("bar")),
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input))

    def test_two_symbols_reversed(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                    node.String("bar"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("bar")),
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("bar"),
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                        node.Param(node.Id("bar")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input))

    def test_object_param_with_two_properties(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("foo"), node.Id("foo")),
                                node.Prop(node.String("bar"), node.Id("bar")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("bar"), node.Id("bar")),
                                node.Prop(node.String("foo"), node.Id("foo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input))

    def test_sort_with_compare(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("barbaz"),
                    node.String("goo"),
                    node.String("fooquxboo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("barbaz")),
                        node.Param(node.Id("goo")),
                        node.Param(node.Id("fooquxboo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("goo"),
                    node.String("barbaz"),
                    node.String("fooquxboo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("goo")),
                        node.Param(node.Id("barbaz")),
                        node.Param(node.Id("fooquxboo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input, key=lambda module_path: len(module_path)))

    def test_one_symbol(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("foo")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input))

    def test_sort_dangling(self) -> None:
        input = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("c"),
                    node.String("a"),
                    node.String("b"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("c")),
                        node.Param(node.Id("a")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("a"),
                    node.String("c"),
                    node.String("b"),
                ]),
                node.Function(
                    [
                        node.Param(node.Id("a")),
                        node.Param(node.Id("c")),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        self.assertEqual(output, sort(input))

if __name__ == "__main__":
    unittest.main()
