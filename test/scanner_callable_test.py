from macov.core import atom
from macov.reader import scan
import unittest

class ScannerCallableTest(unittest.TestCase):
    def test_id_paren(self) -> None:
        code = "foo(bar)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
            ]
        )

    def test_id_whitespace(self) -> None:
        code = "foo bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_at_paren(self) -> None:
        code = "@(foo)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.ParenCallStart(),
                atom.Id("foo"),
                atom.ParenCallEnd(),
            ]
        )

    def test_at_whitespace(self) -> None:
        code = "@ foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.WhitespaceCallStart(),
                atom.Id("foo"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_this_paren(self) -> None:
        code = "this(foo)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.This(),
                atom.ParenCallStart(),
                atom.Id("foo"),
                atom.ParenCallEnd(),
            ]
        )

    def test_this_whitespace(self) -> None:
        code = "this foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.This(),
                atom.WhitespaceCallStart(),
                atom.Id("foo"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_super_paren(self) -> None:
        code = "super(foo)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Super(),
                atom.ParenCallStart(),
                atom.Id("foo"),
                atom.ParenCallEnd(),
            ]
        )

    def test_super_whitespace(self) -> None:
        code = "super foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Super(),
                atom.WhitespaceCallStart(),
                atom.Id("foo"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_property_paren(self) -> None:
        code = "foo.bar(baz)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Dot(),
                atom.Property("bar"),
                atom.ParenCallStart(),
                atom.Id("baz"),
                atom.ParenCallEnd(),
            ]
        )

    def test_property_whitespace(self) -> None:
        code = "foo.bar baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Dot(),
                atom.Property("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_paren_paren(self) -> None:
        code = "(foo)(bar)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftParen(),
                atom.Id("foo"),
                atom.RightParen(),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
            ]
        )

    def test_paren_whitespace(self) -> None:
        code = "(foo) bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftParen(),
                atom.Id("foo"),
                atom.RightParen(),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_bracket_paren(self) -> None:
        code = "[foo](bar)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBracket(),
                atom.Id("foo"),
                atom.RightBracket(),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
            ]
        )

    def test_bracket_whitespace(self) -> None:
        code = "[foo] bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBracket(),
                atom.Id("foo"),
                atom.RightBracket(),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_call_paren(self) -> None:
        code = "foo(bar)(baz)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
                atom.ParenCallStart(),
                atom.Id("baz"),
                atom.ParenCallEnd(),
            ]
        )

    def test_call_whitespace(self) -> None:
        code = "foo(bar) baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_index_paren(self) -> None:
        code = "foo[bar](baz)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
                atom.ParenCallStart(),
                atom.Id("baz"),
                atom.ParenCallEnd(),
            ]
        )

    def test_index_whitespace(self) -> None:
        code = "foo[bar] baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_exist_paren(self) -> None:
        code = "foo?(bar)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Exist(),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
            ]
        )

    def test_exist_no_space(self) -> None:
        code = "bar?foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Exist(),
                atom.WhitespaceCallStart(),
                atom.Id("foo"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_exist_whitespace(self) -> None:
        code = "foo? bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Exist(),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_exist_after_non_callable_paren(self) -> None:
        code = '"foo"?(bar)'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String("foo"),
                atom.Exist(),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
            ]
        )

    def test_exist_after_non_callable_whitespace(self) -> None:
        code = '"foo"? bar'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String("foo"),
                atom.Exist(),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

if __name__ == "__main__":
    unittest.main()
