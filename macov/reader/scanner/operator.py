from macov.core import atom
from macov.core.util import LexicalException, Position, Range
from macov.reader.scanner.util import EOF, Raw
from macov.util import trampoline
from pyrsistent import l
import re

def _raise_exception(message):
    def func(cursor, start_position, **kwargs):
        raise LexicalException(message, start_position)

    return func

def _create_operator(atom_class):
    def func(cursor, start_position, **kwargs):
        return cursor, atom_class(Range(start_position, cursor.position))

    return func

def _scan_operator(otherwise, operator_map):
    def func(cursor, start_position, **kwargs):
        char = cursor.char

        if char not in operator_map:
            return otherwise(cursor, start_position, **kwargs)

        return operator_map.get(char, otherwise)(cursor.next, start_position, **kwargs)

    return func

def _scan_question_mark_alone(cursor, start_position, atoms):
    if cursor.char == ":":
        next_state = cursor.next

        if next_state.char == ":":
            further_state = next_state.next

            return further_state, atom.PrototypeExist(Range(start_position, further_state.position))

    atom_class = atom.BinExist if atoms and atoms.right.type == atom.WhitespaceCallStart.type else atom.Exist

    return cursor, atom_class(Range(start_position, cursor.position))

scan_question_mark_start = _scan_operator(
    _scan_question_mark_alone,
    {
        "=": _create_operator(atom.ExistAssign),
        ".": _create_operator(atom.DotExist),
    }
)

scan_greater_than_start = _scan_operator(
    _create_operator(atom.GreaterThan),
    {
        "=": _create_operator(atom.GreaterThanOrEqualTo),
        ">": _scan_operator(
            _create_operator(atom.SignRightShift),
            {
                "=": _create_operator(atom.SignRightShiftAssign),
                ">": _scan_operator(
                    _create_operator(atom.ZeroFillRightShift),
                    {
                        "=": _create_operator(atom.ZeroFillRightShiftAssign)
                    }
                ),
            }
        )
    }
)

scan_dash_start = _scan_operator(
    _create_operator(atom.Sub),
    {
        ">": _create_operator(atom.ThinArrow),
        "-": _create_operator(atom.Dec),
        "=": _create_operator(atom.SubAssign),
    }
)

scan_caret_start = _scan_operator(
    _create_operator(atom.BitXor),
    {
        "=": _create_operator(atom.BitXorAssign),
    }
)

scan_plus_start = _scan_operator(
    _create_operator(atom.Add),
    {
        "+": _create_operator(atom.Inc),
        "=": _create_operator(atom.AddAssign),
    }
)

scan_asterisk_start = _scan_operator(
    _create_operator(atom.Mul),
    {
        "*": _scan_operator(
            _create_operator(atom.Exp),
            {
                "=": _create_operator(atom.ExpAssign),
            }
        ),
        "=": _create_operator(atom.MulAssign),
    }
)

scan_percent_start = _scan_operator(
    _create_operator(atom.Rem),
    {
        "%": _scan_operator(
            _create_operator(atom.Mod),
            {
                "=": _create_operator(atom.ModAssign),
            }
        ),
        "=": _create_operator(atom.RemAssign),
    }
)

scan_less_than_start = _scan_operator(
    _create_operator(atom.LessThan),
    {
        "=": _create_operator(atom.LessThanOrEqualTo),
        "<": _scan_operator(
            _create_operator(atom.LeftShift),
            {
                "=": _create_operator(atom.LeftShiftAssign),
            }
        )
    }
)

scan_dot_start = _scan_operator(
    _create_operator(atom.Dot),
    {
        ".": _scan_operator(
            _create_operator(atom.Span),
            {
                ".": _create_operator(atom.Splat),
            }
        )
    }
)

scan_colon_start = _scan_operator(
    _create_operator(atom.Colon),
    {
        ":": _create_operator(atom.Prototype),
    }
)

scan_exclamination_mark_start = _scan_operator(
    _raise_exception("operator '!' is not allowed use 'not` instead"),
    {
        "=": _raise_exception("operator '!=' is not allowed use 'isnt` instead"),
    }
)

scan_equal_to_start = _scan_operator(
    _create_operator(atom.Assign),
    {
        "=": _raise_exception("operator '==' is not allowed use 'is` instead"),
        ">": _create_operator(atom.ThickArrow),
    }
)

scan_ampersand_start = _scan_operator(
    _create_operator(atom.BitAnd),
    {
        "&": _scan_operator(
            _raise_exception("operator '&&' is not allowed use 'and` instead"),
            {
                "=": _raise_exception("operator '&&=' is not allowed")
            }
        ),
        "=": _create_operator(atom.BitAndAssign),
    }
)

scan_pipeline_start = _scan_operator(
    _create_operator(atom.BitOr),
    {
        "|": _scan_operator(
            _raise_exception("operator '||' is not allowed use 'or` instead"),
            {
                "=": _raise_exception("operator '||=' is not allowed")
            }
        ),
        "=": _create_operator(atom.BitOrAssign),
    }
)

# TODO; This is a simple trick for now, the correct regular expression grammar is mentioned here: http://ecma-international.org/ecma-262/5.1/#sec-15.10
# note this does not support block regular expressions or regex interpolation
_REGEX_REGEX = re.compile(r"\/((?![*+?])(?:[^\r\n\[/\\]|\\.|\[(?:[^\r\n\]\\]|\\.)*\])+)\/((?:g(?:im?|mi?)?|i(?:gm?|mg?)?|m(?:gi?|ig?)?)?)")
_POSSIBLY_DIVISION_REGEX = re.compile(r"^/=?\s")

def scan_slash_start(state, start_position, **kwargs):
    till_end_of_line_string = _get_string_till_end_of_line(state, "/")
    regex_match = _REGEX_REGEX.match(till_end_of_line_string)

    if regex_match and not _POSSIBLY_DIVISION_REGEX.match(till_end_of_line_string):
        text = regex_match.group()
        last_char_state = state.jump_by(len(text) - 2)

        return last_char_state.next, atom.Regex(text[1:-1], delimiter="/", range=Range(start_position, last_char_state.position))

    if state.char == "=":
        next_state = state.next

        return next_state, atom.FloatDivAssign(Range(start_position, next_state.position))

    if state.char == "/":
        next_state = state.next

        if next_state.char == "=":
            further_state = next_state.next

            return further_state, atom.IntegerDivAssign(Range(start_position, further_state.position))

        return next_state, atom.IntegerDiv(Range(start_position, next_state.position))

    return state, atom.FloatDiv(Range(start_position, state.position))

@trampoline.wrap
def strip_all_around_binary_math(atoms, result=l()):
    if not atoms:
        return trampoline.result(result)

    head, tail = atoms.first, atoms.rest

    if _is_binary_math(head, tail, result):
        return trampoline.call(strip_all_around_binary_math)(tail, result.rest.cons(head))

    return trampoline.call(strip_all_around_binary_math)(tail, result.cons(head))

@trampoline.wrap
def _scan_interpolation(state, start_position, content="", level=0, escape=False):
    if state.char == "}" and level == 0 and not escape:
        return trampoline.result((state.next, Raw(content, position=start_position)))

    if state.char == "{" and not escape:
        return trampoline.call(_scan_interpolation)(state.next, start_position, content + state.char, level + 1, False)

    if state.char == "}" and not escape:
        return trampoline.call(_scan_interpolation)(state.next, start_position, content + state.char, level - 1, False)

    if state.char == EOF:
        raise LexicalException('unexpected end of input, expected "}"', position=state.position)

    if state.char == "\\":
        return trampoline.call(_scan_interpolation)(state.next, start_position, content + state.char, level, True)

    return trampoline.call(_scan_interpolation)(state.next, start_position, content + state.char, level, False)

@trampoline.wrap
def _add_concats(atoms, result=l()):
    if not atoms:
        return trampoline.result(result)

    if not atoms.rest:
        return trampoline.call(_add_concats)(atoms.rest, result.cons(atoms.first))

    return trampoline.call(_add_concats)(atoms.rest, result.cons(atoms.first).cons(atom.Concat()))

def scan_block_regex_start(state, start_position):
    new_state, atoms = _scan_block_regex(state, start_position)

    if not atoms.rest and atoms.first.type == atom.Regex.type:
        return new_state, atoms

    regex_end = atom.RegexEnd(range=Range(Position(new_state.position.row, new_state.position.column - 3), new_state.position))
    regex_start = atom.RegexStart(range=Range(start_position, Position(start_position.row, start_position.column + 3)))

    return new_state, _add_concats(atoms, l(regex_end)).cons(regex_start)

@trampoline.wrap
def _scan_block_regex(state, start_position, result=l(), content="", closing_delimiter="", escape=False):
    if state.char == "/" and not escape:
        if closing_delimiter == "//":
            next_state = state.next

            if not result:
                regex_range = Range(start_position, next_state.position)

                return trampoline.result((next_state, l(atom.Regex(content, delimiter="///", range=regex_range))))
            else:
                new_result = result if not content else result.cons(atom.Regex(content, delimiter="", range=Range(start_position, state.position)))

                return trampoline.result((next_state, new_result))

        return trampoline.call(_scan_block_regex)(state.next, start_position, result, content, closing_delimiter + "/", False)

    if state.char == EOF:
        raise LexicalException('unexpected end of input, expected "///"', position=state.position)

    if state.char == "\\":
        return trampoline.call(_scan_block_regex)(state.next, start_position, result, content + closing_delimiter + state.char, "", True)

    if state.char == "#" and not escape:
        next_state = state.next

        if next_state.char == "{":
            further_state = next_state.next

            new_result = result if not content else result.cons(atom.Regex(content, delimiter="", range=Range(start_position, state.position)))

            after_interpolation_state, interpolation_content = _scan_interpolation(further_state, further_state.position)

            return trampoline.call(_scan_block_regex)(
                after_interpolation_state,
                start_position=after_interpolation_state.position,
                result=new_result.cons(interpolation_content),
                content="",
                closing_delimiter="",
                escape=False
            )

    return trampoline.call(_scan_block_regex)(state.next, start_position, result, content + closing_delimiter + state.char, "", False)

@trampoline.wrap
def _get_string_till_end_of_line(cursor, result=""):
    if cursor.char == "\n":
        return trampoline.result(result)

    return trampoline.call(_get_string_till_end_of_line)(cursor.next, result + cursor.char)

def _is_binary_math(head, tail, result):
    if head.type not in {atom.Add.type, atom.Sub.type}:
        return False

    if result and result.first.type != atom.WhitespaceCallStart.type:
        return False

    return not tail or tail.first.range.begin != head.range.end
