from macov.core.atom import EMPTY_RANGE, Range
from typing import Iterable
import abc

class Node(abc.ABC):
    type: str = NotImplemented

    def __init__(self, range=EMPTY_RANGE) -> None:
        assert isinstance(range, Range)

        self._range = range

    @property
    def range(self):
        return self._range

    def is_(self, class_):
        return isinstance(self, class_)

    @abc.abstractmethod
    def __eq__(self, other: object) -> bool:
        raise NotImplementedError()

class Stmt(Node, abc.ABC):
    pass

class Block(Node):
    type = "block"

    def __init__(self, statements: Iterable[Stmt], range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._statements = statements

    @property
    def statements(self):
        return self._statements

    def __eq__(self, other) -> bool:
        return isinstance(other, Block) and tuple(self._statements) == tuple(other.statements)

    def __str__(self):
        return "#block(statements=[{}])".format(",".join(str(it) for it in self._statements))

class Expr(Stmt, abc.ABC):
    pass

class Literal(Expr, abc.ABC):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)

    def __eq__(self, other) -> bool:
        return isinstance(other, self.__class__)

    def __str__(self) -> str:
        return self.type

class Void(Literal):
    type = "void"

class Rest(Literal):
    type = "rest"

class Null(Literal):
    type = "void"

class True_(Literal):
    type = "true"

class False_(Literal):
    type = "false"

class This(Literal):
    type = "this"

class Super(Literal):
    type = "super"

class Infinity(Literal):
    type = "infinity"

class Nan(Literal):
    type = "nan"

class ExistExpr(Expr):
    def __init__(self, expr: Expr, range: Range = EMPTY_RANGE) -> None:
        super().__init__(range)
        self._expr = expr

    @property
    def expr(self) -> Expr:
        return self._expr

    def __str__(self) -> str:
        return f"exist[{self._expr}]"

    def __eq__(self, other) -> bool:
        return isinstance(other, ExistExpr) and self._expr == other.expr

class Id(Expr):
    type = "id"

    def __init__(self, name: str, range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    def __str__(self) -> str:
        return "#id[{}]".format(self._name)

    def __eq__(self, other) -> bool:
        return isinstance(other, Id) and self._name == other.name

class Accessor(Expr):
    type = "accessor"

    def __init__(self, expr: Expr, key: Expr, range: Range=EMPTY_RANGE) -> None:
        assert key.type != "prototype"
        super().__init__(range)
        self._expr = expr
        self._key = key

    @property
    def expr(self) -> Expr:
        return self._expr

    @property
    def key(self) -> Expr:
        return self._key

    def __str__(self) -> str:
        return "#{}(expr={}, key={})".format(self.type, self._expr, self._key)

    def __eq__(self, other) -> bool:
        return isinstance(other, Accessor) and self._expr == other.expr and self._key == other.key

class ExistAccessor(Accessor):
    type = "exist_accessor"

    def __eq__(self, other) -> bool:
        return isinstance(other, ExistAccessor) and super().__eq__(other)

class Value(Expr, abc.ABC):
    def __init__(self, value: str, range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._value = value

    @property
    def value(self) -> str:
        return self._value

    def __str__(self) -> str:
        return '"#{}[{}]"'.format(self.type, self._value)

    def __eq__(self, other) -> bool:
        return isinstance(other, Value) and self._value == other.value

class String(Value):
    type = "string"

    def set_value(self, value):
        return String(value)

    def to_string(self) -> "String":
        return self

class Regex(Value):
    type = "regex"

    def __init__(self, content, delimiter, range=EMPTY_RANGE):
        super().__init__(content, range)

        self._delimiter = delimiter

    @property
    def delimiter(self):
        return self._delimiter

    def __eq__(self, other):
        return isinstance(other, Regex) and other.delimiter == self._delimiter and super().__eq__(other)

class Number(Value):
    type = "string"

class StringInterpolation(Expr):
    type = "string_interpolation"

    def __init__(self, delimiter: str, exprs: Iterable[Expr], range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._exprs = exprs
        self._delimiter = delimiter

    @property
    def delimiter(self) -> str:
        return self._delimiter

    @property
    def exprs(self) -> Iterable[Expr]:
        return self._exprs

    def __str__(self) -> str:
        return "string_interpolation(expressions=[{}])".format(",".join(map(str, self._exprs)))

    def __eq__(self, other) -> bool:
        return isinstance(other, StringInterpolation) and tuple(self._exprs) == tuple(other.exprs) and self._delimiter == other.delimiter

class RegexInterpolation(Expr):
    type = "regex_interpolation"

    def __init__(self, exprs: Iterable[Expr], range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._exprs = exprs

    @property
    def exprs(self) -> Iterable[Expr]:
        return self._exprs

    def __str__(self) -> str:
        return "regex_interpolation(expressions=[{}])".format(",".join(map(str, self._exprs)))

    def __eq__(self, other) -> bool:
        return isinstance(other, RegexInterpolation) and tuple(self._exprs) == tuple(other.exprs)

class Array(Expr):
    type = "array"

    def __init__(self, items: Iterable[Expr], range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._items = items

    @property
    def items(self):
        return self._items

    def append_item(self, new_item):
        return Array(self._items + [new_item])

    def set_items(self, new_items):
        return Array(list(new_items))

    def __eq__(self, other) -> bool:
        return isinstance(other, Array) and tuple(self._items) == tuple(other.items)

    def __str__(self):
        return "#array[{}]".format(",".join(str(item) for item in self.items))

class _SpanArray(Expr):
    def __init__(self, left: Expr, right: Expr, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._left = left
        self._right = right

    @property
    def left(self) -> Expr:
        return self._left

    @property
    def right(self) -> Expr:
        return self._right

    def __eq__(self, other) -> bool:
        return isinstance(other, _SpanArray) and self._left == other.left and self._right == other.right

    def __str__(self) -> str:
        return f"{self.type}({self._left}, {self._right})"

class InclSpanArray(_SpanArray):
    type = "incl_span_array"

class ExclSpanArray(_SpanArray):
    type = "excl_span_array"

class _SliceExpr(Expr):
    def __init__(self, expr: Expr, left: Expr, right: Expr, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._expr = expr
        self._left = left
        self._right = right

    @property
    def expr(self) -> Expr:
        return self._expr

    @property
    def left(self) -> Expr:
        return self._left

    @property
    def right(self) -> Expr:
        return self._right

    def __eq__(self, other) -> bool:
        return isinstance(other, _SliceExpr) and self._expr == other.expr and self._left == other.left and self._right == other.right

    def __str__(self) -> str:
        return f"{self.type} {self._expr} ({self._left}, {self._right})"

class InclSliceExpr(_SliceExpr):
    type = "incl_slice_expr"

    def __eq__(self, other):
        return isinstance(other, InclSliceExpr) and super().__eq__(other)

class ExclSliceExpr(_SliceExpr):
    type = "excl_slice_expr"

    def __eq__(self, other):
        return isinstance(other, ExclSliceExpr) and super().__eq__(other)

class Param(Node):
    type = "param"

    def __init__(self, value: Expr, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._value = value

    @property
    def value(self) -> Expr:
        return self._value

    def set_value(self, value):
        return Param(value, self.range)

    def __eq__(self, other) -> bool:
        if not isinstance(other, Param):
            return False

        return self._value == other.value

    def __str__(self) -> str:
        return "#param[{}]".format(self._value)

class _Invocation(Expr, abc.ABC):
    def __init__(self, expr: Expr, args: Iterable[Expr], range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._expr = expr
        self._args = args

    @property
    def expr(self):
        return self._expr

    @property
    def args(self):
        return self._args

    def __eq__(self, other) -> bool:
        return (
            isinstance(other, _Invocation) and
            self._expr == other.expr and
            tuple(self._args) == tuple(other.args)
        )

    def __str__(self):
        return "{} {}({})".format(self.type, self._expr, ",".join(map(str, self._args)))

class Call(_Invocation):
    type = "call"

    def set_args(self, new_args):
        return Call(self.expr, new_args)

    def set_expr(self, new_expr):
        return Call(new_expr, self.args)

    def __eq__(self, other) -> bool:
        return isinstance(other, Call) and super().__eq__(other)

class SuperCall(_Invocation):
    type = "super_call"

    def __init__(self, args: Iterable[Expr], range: Range=EMPTY_RANGE):
        super().__init__(Super(), args, range)

    def set_args(self, new_args):
        return SuperCall(self._expr, new_args)

    def __eq__(self, other) -> bool:
        return isinstance(other, SuperCall) and super().__eq__(other)

class DeleteExpr(Expr):
    type = "delete"

    def __init__(self, expr: Expr, key: Expr, range: Range=EMPTY_RANGE) -> bool:
        super().__init__(range)
        self._expr = expr
        self._key = key

    @property
    def expr(self) -> Expr:
        return self._expr

    @property
    def key(self) -> Expr:
        return self._key

    def __eq__(self, other):
        return isinstance(other, DeleteExpr) and self._key == other.key and self._expr == other.expr

    def __str__(self) -> str:
        return f"delete {self._expr}[{self._key}]"


class New(_Invocation):
    type = "new"

    def __eq__(self, other) -> bool:
        return isinstance(other, New) and super().__eq__(other)

class UnExpr(Expr, abc.ABC):
    def __init__(self, expr: Expr, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)

        self._expr = expr

    @property
    def expr(self) -> Expr:
        return self._expr

    def __str__(self) -> str:
        return "({} {})".format(self.type, self._expr)

    def __eq__(self, other) -> bool:
        return isinstance(other, UnExpr) and self._expr == other.expr

class PrefixIncExpr(UnExpr):
    type = "prefix_inc"

    def __eq__(self, other) -> bool:
        return isinstance(other, PrefixIncExpr) and super().__eq__(other)

class PrefixDecExpr(UnExpr):
    type = "prefix_dec"

    def __eq__(self, other) -> bool:
        return isinstance(other, PrefixDecExpr) and super().__eq__(other)

class SplatExpr(UnExpr):
    type = "splat"

    def __eq__(self, other) -> bool:
        return isinstance(other, SplatExpr) and super().__eq__(other)

class PostfixIncExpr(UnExpr):
    type = "postfix_inc"

    def __eq__(self, other) -> bool:
        return isinstance(other, PostfixIncExpr) and super().__eq__(other)

class PostfixDecExpr(UnExpr):
    type = "postfix_dec"

    def __eq__(self, other) -> bool:
        return isinstance(other, PostfixDecExpr) and super().__eq__(other)

class MinusExpr(UnExpr):
    type = "-"

    def __eq__(self, other) -> bool:
        return isinstance(other, MinusExpr) and super().__eq__(other)

class PlusExpr(UnExpr):
    type = "+"

    def __eq__(self, other) -> bool:
        return isinstance(other, PlusExpr) and super().__eq__(other)

class BitNotExpr(UnExpr):
    type = "~"

    def __eq__(self, other) -> bool:
        return isinstance(other, BitNotExpr) and super().__eq__(other)

class NotExpr(UnExpr):
    type = "not"

    def __eq__(self, other) -> bool:
        return isinstance(other, NotExpr) and super().__eq__(other)

class TypeOf(UnExpr):
    type = "typeof"

    def __eq__(self, other) -> bool:
        return isinstance(other, TypeOf) and super().__eq__(other)

class BinExpr(Expr, abc.ABC):
    def __init__(self, left: Expr, right: Expr, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)

        self._left = left
        self._right = right

    @property
    def left(self) -> Expr:
        return self._left

    @property
    def right(self) -> Expr:
        return self._right

    def __str__(self) -> str:
        return "({} {} {})".format(self._left, self.type, self.right)

    def __eq__(self, other) -> bool:
        return isinstance(other, BinExpr) and self._left == other.left and self._right == other.right

class AssignExpr(BinExpr):
    type = "="

    def __eq__(self, other) -> bool:
        return isinstance(other, AssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} = {}".format(self._left, self._right)

class ModAssignExpr(BinExpr):
    type = "mod_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, ModAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} %%= {}".format(self._left, self._expr)

class RemAssignExpr(BinExpr):
    type = "rem_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, RemAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} %= {}".format(self._left, self._expr)

class BitAndAssignExpr(BinExpr):
    type = "bit_and_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, BitAndAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} &= {}".format(self._left, self._expr)

class ExpAssignExpr(BinExpr):
    type = "exp_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, ExpAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} **= {}".format(self._left, self._expr)

class MulAssignExpr(BinExpr):
    type = "mul_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, MulAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} *= {}".format(self._left, self._expr)

class AddAssignExpr(BinExpr):
    type = "add_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, AddAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} += {}".format(self._left, self._expr)

class SubAssignExpr(BinExpr):
    type = "sub_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, SubAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} -= {}".format(self._left, self._expr)

class IntegerDivAssignExpr(BinExpr):
    type = "integer_div_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, IntegerDivAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} //= {}".format(self._left, self._expr)

class FloatDivAssignExpr(BinExpr):
    type = "float_div_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, FloatDivAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} /= {}".format(self._left, self._expr)

class LeftShiftAssignExpr(BinExpr):
    type = "left_shift_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, LeftShiftAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} <<= {}".format(self._left, self._expr)

class SignRightShiftAssignExpr(BinExpr):
    type = "sign_right_shift_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, SignRightShiftAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} >>= {}".format(self._left, self._expr)

class ZeroFillRightShiftAssignExpr(BinExpr):
    type = "zero_fill_right_shift_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, ZeroFillRightShiftAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} >>>= {}".format(self._left, self._expr)

class ExistAssignExpr(BinExpr):
    type = "exist_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, ExistAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} ?= {}".format(self._left, self._expr)

class BitXorAssignExpr(BinExpr):
    type = "bit_xor_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, BitXorAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} ^= {}".format(self._left, self._expr)

class BitOrAssignExpr(BinExpr):
    type = "bit_or_assign"

    def __eq__(self, other) -> bool:
        return isinstance(other, BitOrAssignExpr) and super().__eq__(other)

    def __str__(self):
        return "{} |= {}".format(self._left, self._expr)

class ExpExpr(BinExpr):
    type = "exp"

    def __eq__(self, other) -> bool:
        return isinstance(other, ExpExpr) and super().__eq__(other)

class AddExpr(BinExpr):
    type = "+"

    def __eq__(self, other) -> bool:
        return isinstance(other, AddExpr) and super().__eq__(other)

class SubExpr(BinExpr):
    type = "-"

    def __eq__(self, other) -> bool:
        return isinstance(other, SubExpr) and super().__eq__(other)

class MulExpr(BinExpr):
    type = "*"

    def __eq__(self, other) -> bool:
        return isinstance(other, MulExpr) and super().__eq__(other)

class FloatDivExpr(BinExpr):
    type = "/"

    def __eq__(self, other) -> bool:
        return isinstance(other, FloatDivExpr) and super().__eq__(other)

class IntegerDivExpr(BinExpr):
    type = "div"

    def __eq__(self, other) -> bool:
        return isinstance(other, IntegerDivExpr) and super().__eq__(other)

class RemExpr(BinExpr):
    type = "%"

    def __eq__(self, other) -> bool:
        return isinstance(other, RemExpr) and super().__eq__(other)

class ModExpr(BinExpr):
    type = "mod"

    def __eq__(self, other) -> bool:
        return isinstance(other, ModExpr) and super().__eq__(other)

class BitAndExpr(BinExpr):
    type = "&"

    def __eq__(self, other) -> bool:
        return isinstance(other, BitAndExpr) and super().__eq__(other)

class AndExpr(BinExpr):
    type = "and"

    def __eq__(self, other) -> bool:
        return isinstance(other, AndExpr) and super().__eq__(other)

class BitOrExpr(BinExpr):
    type = "|"

    def __eq__(self, other) -> bool:
        return isinstance(other, BitOrExpr) and super().__eq__(other)

class OrExpr(BinExpr):
    type = "or"

    def __eq__(self, other) -> bool:
        return isinstance(other, OrExpr) and super().__eq__(other)

class BitXorExpr(BinExpr):
    type = "^"

    def __eq__(self, other) -> bool:
        return isinstance(other, BitXorExpr) and super().__eq__(other)

class LeftShiftExpr(BinExpr):
    type = "left_shift"

    def __eq__(self, other) -> bool:
        return isinstance(other, LeftShiftExpr) and super().__eq__(other)

class SignRightShiftExpr(BinExpr):
    type = "sign_right_shift"

    def __eq__(self, other) -> bool:
        return isinstance(other, SignRightShiftExpr) and super().__eq__(other)

class ZeroFillRightShiftExpr(BinExpr):
    type = "zero_fill_right_shift"

    def __eq__(self, other) -> bool:
        return isinstance(other, ZeroFillRightShiftExpr) and super().__eq__(other)

class InExpr(BinExpr):
    type = "in"

    def __eq__(self, other) -> bool:
        return isinstance(other, InExpr) and super().__eq__(other)

class OfExpr(BinExpr):
    type = "of"

    def __eq__(self, other) -> bool:
        return isinstance(other, OfExpr) and super().__eq__(other)

class InstanceOfExpr(BinExpr):
    type = "instanceof"

    def __eq__(self, other) -> bool:
        return isinstance(other, InstanceOfExpr) and super().__eq__(other)

class BinExistExpr(BinExpr):
    type = "bin_exist"

    def __eq__(self, other) -> bool:
        return isinstance(other, BinExistExpr) and super().__eq__(other)

class Cmp(abc.ABC):
    op: str = NotImplemented

    def __init__(self, expr: Expr) -> None:
        self._expr = expr

    @property
    def expr(self) -> Expr:
        return self._expr

    def __eq__(self, other) -> bool:
        return isinstance(other, Cmp) and self.op == other.op and self._expr == other.expr

    def __str__(self) -> str:
        return "[{} {}]".format(self.op, self._expr)

class IsCmp(Cmp):
    op = "is"

    def __eq__(self, other) -> bool:
        return isinstance(other, IsCmp) and super().__eq__(other)

class IsNotCmp(Cmp):
    op = "isnt"

    def __eq__(self, other) -> bool:
        return isinstance(other, IsNotCmp) and super().__eq__(other)

class GreaterThanOrEqualToCmp(Cmp):
    op = "greater_than_or_equal_to"

    def __eq__(self, other) -> bool:
        return isinstance(other, GreaterThanOrEqualToCmp) and super().__eq__(other)

class LessThanOrEqualToCmp(Cmp):
    op = "less_than_or_equal_to"

    def __eq__(self, other) -> bool:
        return isinstance(other, LessThanOrEqualToCmp) and super().__eq__(other)

class GreaterThanCmp(Cmp):
    op = ">"

    def __eq__(self, other) -> bool:
        return isinstance(other, GreaterThanCmp) and super().__eq__(other)

class LessThanCmp(Cmp):
    op = "<"

    def __eq__(self, other) -> bool:
        return isinstance(other, LessThanCmp) and super().__eq__(other)

class CompExpr(Expr):
    type = "comp"

    def __init__(self, left: Expr, cmps: Iterable[Cmp], range: Range = EMPTY_RANGE) -> None:
        super().__init__(range)

        self._left = left
        self._cmps = cmps

    @property
    def left(self) -> Expr:
        return self._left

    @property
    def cmps(self) -> Iterable[Cmp]:
        return self._cmps

    def __eq__(self, other) -> bool:
        return isinstance(other, CompExpr) and self._left == other.left and tuple(self._cmps) == tuple(other.cmps)

    def __str__(self) -> str:
        return "{} {}".format(self._left, " ".join(map(str, self._cmps)))

class Return(Stmt):
    type = "return"

    def __init__(self, expr: Expr, range: Range = EMPTY_RANGE) -> None:
        super().__init__(range)
        self._expr = expr

    @property
    def expr(self) -> Expr:
        return self._expr

    def __eq__(self, other) -> bool:
        return isinstance(other, Return) and self._expr == other.expr

    def __str__(self) -> str:
        return "#return {}".format(self._expr)

class Throw(Stmt):
    type = "throw"

    def __init__(self, expr: Expr, range: Range = EMPTY_RANGE) -> None:
        super().__init__(range)
        self._expr = expr

    @property
    def expr(self) -> Expr:
        return self._expr

    def __eq__(self, other) -> bool:
        return isinstance(other, Throw) and self._expr == other.expr

    def __str__(self) -> str:
        return "#throw {}".format(self._expr)


class Function(Expr):
    type = "function"

    def __init__(self, params: Iterable[Param], body: Block, is_bound: bool=False, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._params = params
        self._body = body
        self._is_bound = is_bound

    @property
    def params(self) -> Iterable[Param]:
        return self._params

    @property
    def body(self) -> Block:
        return self._body

    @property
    def is_bound(self) -> bool:
        return self._is_bound

    def append_param(self, new_param):
        return Function(self._params + [new_param], self._body)

    def set_params(self, new_params):
        return Function(list(new_params), self._body)

    def __eq__(self, other) -> bool:
        return (
            isinstance(other, Function) and
            self._body == other.body and
            tuple(self._params) == tuple(other.params) and
            self._is_bound == other.is_bound
        )

    def __str__(self):
        return "#function(params=[{}], body={}".format(",".join(map(str, self._params)), self._body)

class CondExpr(Expr):
    def __init__(self, cond: Expr, body: Block, alternative: Block, range: Range = EMPTY_RANGE) -> None:
        super().__init__(range)
        self._cond = cond
        self._alternative = alternative
        self._body = body

    @property
    def cond(self) -> Expr:
        return self._cond

    @property
    def alternative(self) -> Block:
        return self._alternative

    @property
    def body(self) -> Block:
        return self._body

    def __str__(self) -> str:
        return "#if(condition={}, body={}".format(self._cond, self._body)

    def __eq__(self, other) -> bool:
        return (
            isinstance(other, CondExpr) and
            self._body == other.body and
            self._cond == other.cond and
            self._alternative == other.alternative
        )

class If(CondExpr):
    type = "if"

    def __eq__(self, other) -> bool:
        return isinstance(other, If) and super().__eq__(other)

class Unless(CondExpr):
    type = "unless"

    def __eq__(self, other) -> bool:
        return isinstance(other, Unless) and super().__eq__(other)

class Prop(Node):
    type = "property"

    def __init__(self, key: String, value: Expr, range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._key = key
        self._value = value

    @property
    def key(self):
        return self._key

    @property
    def value(self):
        return self._value

    def set_value(self, value):
        return Prop(self._key, value, self.range)

    def __eq__(self, other):
        if not isinstance(other, Prop):
            return False

        return self._key == other.key and self._value == other.value

    def __str__(self) -> str:
        return f"{self._key}: {self._value}"

class Class(Stmt):
    type = "class"

    def __init__(self, name: str, parent: Expr, props: Iterable[Prop], body=Block([]), range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._name = name
        self._parent = parent
        self._props = props
        self._body = body

    @property
    def name(self) -> str:
        return self._name

    @property
    def parent(self) -> Expr:
        return self._parent

    @property
    def body(self) -> Block:
        return self._body

    @property
    def props(self) -> Iterable[Prop]:
        return self._props

    def __eq__(self, other) -> bool:
        return (
            isinstance(other, Class) and
            self._name == other.name and
            self._parent == other.parent and
            self._body == other.body and
            tuple(self._props) == tuple(other.props)
        )

    def __str__(self) -> str:
        return '#class(name="{}" props={} body={})'.format(self._name, ",".join(map(str, self._props)), self._body)

class Object(Expr):
    type = "object"

    def __init__(self, props: Iterable[Prop], range: Range=EMPTY_RANGE) -> None:
        super().__init__(range)
        self._props = props

    @property
    def props(self) -> Iterable[Prop]:
        return self._props

    def get_prop(self, key, default):
        return next((prop.value for prop in self._props if prop.key == key), default)

    def set_prop(self, key, value):
        index = next((index for index, prop in enumerate(self._props) if prop.key == key), len(self._props))

        return Object(self._props[:index] + [Prop(key, value)] + self._props[index + 1:])

    def set_props(self, props):
        return Object(list(props), self.range)

    def __eq__(self, other) -> bool:
        return isinstance(other, Object) and tuple(self._props) == tuple(other.props)

    def __str__(self):
        return "#object[{}]".format(",".join("{}:{}".format(prop.key, prop.value) for prop in self._props))
