from functools import reduce
from macov.core import node
from typing import Iterable

def flat_map(f, xs):
    return (y for ys in xs for y in f(ys))

def _read_object_ids(object: node.Object) -> Iterable[node.Id]:
    return flat_map(_read_expr_ids, (prop.value for prop in object.props))

def _read_array_ids(array: node.Array) -> Iterable[node.Id]:
    return flat_map(_read_expr_ids, array.items)

def _read_expr_ids(expr: node.Expr) -> Iterable[node.Id]:
    if isinstance(expr, node.Id):
        return (expr,)

    if isinstance(expr, node.Object):
        return _read_object_ids(expr)

    if isinstance(expr, node.Array):
        return _read_array_ids(expr)

    return ()

def read_ids(func: node.Function) -> Iterable[node.Id]:
    return list(flat_map(_read_expr_ids, (param.value for param in func.params)))
