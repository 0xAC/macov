{
    girlish:
        escutcheon:
            clergy: "thickish"
            administer:
                intimate: [
                    {
                        crucify:
                            dormitory: { disruptive: "eider" }
                            torrential: [
                                {
                                    chemisorb: "rap"
                                    revulsion: { insomnia: "electress" }
                                }
                            ]
                    }
                    {
                        flap:
                            caveat: { slogan: "chancellor" }
                            countervail: [
                                {
                                    heptane: "guidepost"
                                    semper: { idiot: "balmy" }
                                }
                            ]
                    }
                ]
    personage:
        catastrophic:
            paper: "brittle"
            therefrom: [
                { shrimp: "rote" }
                { crappie: "memoranda" }
            ]
        bootstrapping:
            averred:
                road:
                    choral: "shame"
                    pupae: true
                    execrate:
                        limbo: { frugal: "limbo" }
                        selenate: { hierarchal: "selenate" }
                        arcsin: { densitometer: "arcsin" }
                        biochemic: { cos: "intrigue" }
                        embrittle: { sleepy: "asparagus" }
                        screwy: { lad: "hysterectomy" }
                        tinsel: null
                        beechwood: { loblolly: "waveguide" }
                        gotten: { dumpster: "joint" }
                        lodestone: { extricable: "carpet" }
                        perforce: { aforementioned: "textbook" }
                        miasma: { hearken: "counterweight" }
                        single: { enviable: "encomium" }
                        applicant: { eight: "pepperoni" }
                        chlorophyll: { homonym: "chlorophyll" }
                        hob:  { usual: "poke" }
                        bedrock: { surround: "clearance" }
        alai: "pantheism"
    therapeutic: { mausoleum: "saliva" }
    preference:
        alter: "praiseworthy"
    cosine:
        chaparral:
            blueprint: "bias"
            stupor: [
                { seamen: "witchcraft" }
                { camellia: "nihilism" }
                "plover"
            ]
    spine:
        hungry:
            synthesis: "eagle"
            salvo: [
                { wry: "horsewomen" }
                { limpet: "heron" }
                "depreciable"
            ]
    metallurgy:
        valedictorian:
            potpourri: "palazzi"
            ski: [
                { enviable: "storyteller" }
                { stagy: "zero" }
                "treasure"
            ]
    befoul:
        absurdum:
            wolfish: "recline"
            chevy: [
                { swish: "layperson" }
                { start: "proselyte" }
                "flam"
            ]
    yen:
        gross:
            repressive: "petrify"
            safekeeping: [
                { ado: "brush" }
                { submitting: "impudent" }
                "eye"
            ]
    paschal:
        choline:
            decontrol: "diffident"
            fruition: [
                { unimodal: "footage" }
                { expelling: "caricature" }
                "participant"
            ]
    hydroxide:
        rhombic:
            pickle: "fruitful"
            verbal: [
                { headstrong: "imagine" }
                { obligatory: "proposal" }
                "palliate"
            ]
    waitress:
        shiny:
            seclude: "garrison"
            brook: [
                { flashlight: "lightfooted" }
                { pupal: "sacrosanct" }
                "memorabilia"
            ]
    tercel:
        intern:
            nadir: "welfare"
            embolden: [
                { guanidine: "deem" }
                { pack: "bolt" }
                "willful"
            ]
    cobweb:
        barefaced:
            wilt: "psycho"
            aspirin: [
                { sleeve: "nevertheless" }
                { speedboat: "topple" }
                "coastline"
            ]
    fight:
        sideman:
            helpmate: { ivory: "deteriorate" }
            come: [
                "volcanic"
                { uracil: "novitiate" }
            ]
    irreconciliable:
        advisor:
            imprison: "timeshare"
            latera: [
                { transform: "cellular" }
                { bluejacket: "post" }
            ]
    matriarchal:
        channel:
            insectivorous: "lair"
            altercate: [
                { condolence: "sa" }
                { lumbar: "trypsin" }
                {
                    venerable:
                        snakeroot: { twisty: "airline" }
                        rummage: [
                            { embed: "determinant" }
                        ]
                }
                "knoweth"
            ]
    littermate:
        swum:
            carpet: "lumberjack"
            canary: [
                { mulligan: "bill" }
                { preferred: "plug" }
                { urania: "continuity" }
                "pry"
            ]
    apocalypse:
        transect:
            carbonyl: "ravish"
            lutetium: [
                { scription: "detractor" }
                { politic: "which" }
                { press: "capacious" }
                42
            ]
    truss:
        seminal:
            sanitary: "smoky"
    background:
        starry:
            keyboard: "expository"
            plumbate: [
                { depressive: "plagiarism" }
                { cataleptic: "secondhand" }
            ]
    attention:
        serial:
            laminate: "periwinkle"
    goshawk:
        acrophobic:
            fascinate: { advise: "positron" }
            dent: [
                "mudsling"
            ]
    enchantress:
        fit:
            adsorbate: "neuropathology"
    union:
        chestnut:
            erupt: { emasculate: "octagonal" }
            redact: [
                "tilde"
            ]
    frolicked:
        inimical:
            parasite: "sima"
    bloom:
        kiss:
            nuptial: "sovkhoz"
    exchange:
        carol:
            stud: { themselves: "barrage" }
            orifice: [
                "conglomerate"
                { tog: "kazoo" }
            ]
    footprint:
        raucous:
            create: "beryllium"
    germane:
        tweed:
            leach: "retardation"
            dauphine: [
                [
                    {
                        trickster:
                            fetch:
                                monomial: "tame"
                                peak: [
                                    {
                                        oatcake: ["snort", "auspice"]
                                        naiad: { kit: "cupric" }
                                    }
                                ]
                        cretinous: "tumult"
                    }
                    {
                        assimilate:
                            creditor:
                                configuration: "melanism"
                                tenure: [
                                    {
                                        aloud: ["psych", "psych-android-expand"]
                                        galvanism: { optometry: "scarface" }
                                    }
                                ]
                        ascription: "this"
                    }
                ]
                {
                    thickish:
                        nauseum: "psychotherapeutic"
                        hectic: [
                            { poodle: "attrition" }
                        ]
                }
            ]
        daffodil:
            pyknotic: [{mythic: "ere"}]
    skeptic:
        exportation:
            tuberous: "pickerel"
            lyrebird: [
                [
                    {
                        stonecrop:
                            rifle:
                                lacuna: "perfidious"
                                matrimonial: [
                                    { pseudonym: "maid" }
                                    { shako: "ionospheric" }
                                ]
                        recrudescent: 38
                        prospector: {}
                    }
                ]
            ]
        bladderwort:
            escort: [{schooner: "muscular"}]
    seeable:
        partition:
            millionaire: "dogbane"
            improve: [
                [
                    {
                        cloacal:
                            rid:
                                communicate: "shy"
                                mockup: [
                                    [
                                        {
                                            adaptation:
                                                hoy: "crafty"
                                                brainwash: [
                                                    { nonetheless: "young" }
                                                    { float: "henpeck" }
                                                ]
                                        }
                                        {
                                            mountaintop:
                                                glycol: "springtail"
                                                lenient: [
                                                    {
                                                        variety: "spinnaker"
                                                        mulligan: "gosh"
                                                    }
                                                    [
                                                        {
                                                            greenish: "villainous"
                                                            practicable: "placemat"
                                                        }
                                                        {
                                                            vii: "support"
                                                        }
                                                        {
                                                            yank: "coarse"
                                                            deerskin: "priggish"
                                                        }
                                                        {
                                                            arbitrate: "revoke"
                                                            piquant: "glob"
                                                        }
                                                        {
                                                            suntanned: "tanager"
                                                        }
                                                        {
                                                            substitute: "manumitted"
                                                            detente: "dying"
                                                        }
                                                        {
                                                            moon: "surge"
                                                            stonewall: "goldsmith"
                                                        }
                                                        {
                                                            accretion: "winter"
                                                        }
                                                        {
                                                            construal: "facial"
                                                            sweater: "ballroom"
                                                        }
                                                    ]
                                                ]
                                        }
                                    ]
                                ]
                        rage: {
                            mutate: 7
                            caloric: 49
                            scutum: 41
                        }
                        wack: {}
                    }
                    {
                        jam:
                            oceanic:
                                infirmary: "coma"
                                convene: [
                                    { squashy: "broadcast" }
                                    { sealant: "treatise" }
                                ]
                        constitute: {
                            delectate: 46
                            excusable: 14
                            waylay: 13
                        }
                        bongo: {}
                        abovementioned: ["barrier"]
                    }
                    {
                        fierce:
                            reel:
                                courtier: "sprout"
                                emendable: [
                                    { teletype: "qua" }
                                    { granitic: "strongroom" }
                                ]
                        definite: {
                            backlog: 20
                            highboy: 24
                            fiche: 9
                        }
                        habitant: {}
                        dovekie: ["awful"]
                    }
                    {
                        vellum:
                            cacophonist:
                                scrumptious: "written"
                                suspense: [
                                    [
                                        {
                                            somewhere:
                                                grandeur: "habitual"
                                                orbital: [
                                                    { snobbish: "divestiture" }
                                                    { expropriate: "spyglass" }
                                                ]
                                        }
                                        {
                                            week:
                                                impale: "bitch"
                                                phosphorus: [
                                                    {
                                                        sudden: "sedentary"
                                                        turquoise: "asymptotic"
                                                    }
                                                    [
                                                        {
                                                            hove: "nationwide"
                                                            radar: "oak"
                                                        }
                                                        {
                                                            yourself: "pretentious"
                                                            proprietary: "transpacific"
                                                        }
                                                        {
                                                            transgressor: "percent"
                                                            adjoin: "slavery"
                                                        }
                                                        {
                                                            secretive: "mugging"
                                                            referring: "aluminate"
                                                        }
                                                        {
                                                            hipster: "accustom"
                                                        }
                                                        {
                                                            semester: "baronet"
                                                            worry: "fascinate"
                                                        }
                                                        {
                                                            fantasia: "role"
                                                            agreeing: "swigging"
                                                            parallax: "nihilism"
                                                        }
                                                    ]
                                                ]
                                        }
                                    ]
                                ]
                        admitting: {
                            redemptive: 38
                            somebody: 19
                            intestate: 9
                        }
                        sir: {}
                        tung: ["gigantic"]
                    }
                    {
                        carouse:
                            result:
                                strikebreak: "lobule"
                                pectoralis: [
                                    [
                                        {
                                            decent:
                                                weapon: "tamarisk"
                                                choirmaster: [
                                                    { readout: "sculpture" }
                                                    { realtor: "histrionic" }
                                                ]
                                        }
                                        {
                                            room:
                                                rehearsal: "aunt"
                                                parochial: [
                                                    {
                                                        barony: "replete"
                                                        bimetallism: "aorta"
                                                        daughter: "chunk"
                                                    }
                                                    [
                                                        {
                                                            tepee: "hungry"
                                                            do: { histidine: "spoken" }
                                                        }
                                                        {
                                                            asylum: "euglena"
                                                            crusade: { hydrometer: "debase" }
                                                        }
                                                        {
                                                            flashback: "pronoun"
                                                            shroud: { analogue: "impassable" }
                                                        }
                                                        {
                                                            parliament: "pathway"
                                                            sidewall: { optima: "lobular" }
                                                        }
                                                        { throughout: "worse" }
                                                        {
                                                            acacia: "fed"
                                                            expound: "tepee"
                                                        }
                                                    ]
                                                ]
                                        }
                                    ]
                                ]
                        custody: {
                            lipstick: 19
                            kraut: 24
                            tip: 7
                        }
                        biology: {}
                        oar: ["normal"]
                    }
                ]
            ]
        pudding:
            plot: [{margin: "annuli"}]
    ecumenist:
        automata:
            aloof: "coach"
            prefer: [
                { paraxial: "eleven" }
                { accidental: "drowsy" }
                { before: "flashback" }
                {
                    modify: "delusive"
                }
                {
                    tundra: "buddy"
                }
            ]
        virgule:
            metazoa: [{patio: "diamagnetism"}]
    gyro:
        anonymous:
            borosilicate: "micron"
            anent: [
                { repertory: "natty" }
                { chasm: "buck" }
                { bread: "tinker" }
            ]
        inertia:
            concede: [{photolysis: "condone"}]
    regulate:
        adventurous:
            heron: "figurate"
            ordinate: [
                { duff: "forsworn" }
                { typeface: "financial" }
            ]
    bony:
        housefly:
            orthography: "traitor"
            applique: [
                "sulky"
            ]
    shoreline:
        agronomy:
            winemake: "on"
            playback: [
                "embarrass"
            ]
    notocord:
        gratify:
            impressible: "bodice"
            penna: [
                "penitent"
            ]
    bodybuilding:
        concentrate:
            referee: "wad"
            ascribe: [
                { biplane: "leonine" }
                { braid: "nitty" }
                { spectrography: "metric" }
                { hirsute: "wiggle" }
                { resist: "stall" }
                { hying: "shrewish" }
                { fiery: "treat" }
                { morale: "extramarital" }
                { terrace: "clumsy" }
            ]
    tunnel:
        capacitive:
            onyx: "somewhat"
            ferret: [
                [
                    {
                        writ:
                            redstart:
                                erg: "insuperable"
                                allele: [
                                    { ferret: "longstanding" }
                                    {
                                        autocollimate: "sausage"
                                        amphetamine: "lipid"
                                    }
                                    { matrimony: "symposia" }
                                    { leghorn: "cadaverous" }
                                    { new: "hallucinogen" }
                                    { zillion: "definition" }
                                    { provost: "michigan" }
                                    { jejunum: "psychiatrist" }
                                    { glaucoma: "provoke" }
                                    { bench: "tappa" }
                                ]
                        acuity: "mimic"
                        austral: ["confront"]
                    }
                    {
                        fluorite:
                            migrate:
                                cosine: "yang"
                                injurious: [
                                    { identity: "gusto" }
                                    [
                                        {
                                            continuo: "wattage"
                                            ontogeny: "mulligan"
                                            calm: 38
                                            chordal: 0
                                            epochal: 15
                                            optima: "rosary"
                                            pinkie: { onset: "obsequy" }
                                        }
                                        {
                                            garner: "inviable"
                                            vacuous: "societal"
                                            discriminable: 23
                                            pond: 25
                                            gush: 22
                                            awn: "analogous"
                                            anarch: { warden: "bet" }
                                        }
                                        {
                                            impetus: "amble"
                                            magnitude: "dodecahedra"
                                            maniacal: "nursery"
                                            infidel: "symbiotic"
                                            hosiery: false
                                            rescue: "energy"
                                            time: { vendible: "forever" }
                                        }
                                        {
                                            housebroken: "suburbia"
                                            have: "annex"
                                            handy: "haunt"
                                            bivariate: "gyrocompass"
                                            desiccant:
                                                prose: "ruthless"
                                                tizzy: "guanine"
                                                common: "common"
                                                culture: "convolve"
                                                corral: "corral"
                                                flatworm: "sovereignty"
                                                hodge: "fore"
                                                carte: "bel"
                                                sunshade: "sunshade"
                                                complex: "complex"
                                                proprietor: "seismology"
                                                abrade: "tunic"
                                            mineralogy: { bear: "propellant" }
                                        }
                                    ]
                                ]
                        debauchery: "whoopee"
                        stickle: ["clonic"]
                    }
                    {
                        aviate:
                            gladden:
                                smoothbore: "preclude"
                                ebony: [
                                    [
                                        {
                                            doorkeeper: "metro"
                                            tommy: "embraceable"
                                            peafowl: { lotus: "shod" }
                                        }
                                        {
                                            thimble: "shortcake"
                                            bake: "proverb"
                                            awry: { parasol: "odyssey" }
                                        }
                                        {
                                            inexperience: "bouquet"
                                            treachery: "match"
                                            religious: { sediment: "reprisal" }
                                        }
                                    ]
                                ]
                        millennia: "sandalwood"
                        gyrate: ["assurance"]
                    }
                    {
                        unity:
                            nutritious:
                                annual: "slouch"
                                jog: [
                                    { dine: "bridgehead" }
                                    { governor: "spiderwort" }
                                    { groove: "hertz" }
                                    { sweatshirt: "fit" }
                                    { buzzword: "embouchure" }
                                    { peruse: "spicy" }
                                ]
                        asunder: "indubitable"
                        escadrille: ["wrap"]
                    }
                ]
            ]
        bulldog:
            aplomb: [{propionate: "registry"}]
    ruddy:
        dichloride:
            stonecrop: "opossum"
        althea:
            acerbity: [{summitry: "falsetto"}]
    private:
        impolite:
            acetone: "tollgate"
            blurry: [
                { nine: "indignation" }
                { scarce: "adsorption" }
                { sport: "buttery" }
            ]
    awhile:
        carboxylic:
            ceremonious: "kleenex"
    consolidate:
        estimate:
            refract: "incursion"
            creek: [
                { gout: "metronome" }
                { communicant: "florican" }
            ]
    degum:
        alchemy:
            receptacle: "teleconference"
            kraut: [
                { knives: "gab" }
                { bard: "salacious" }
                { steer: "sub" }
            ]
    thoughtful: { theses: "ablution" }
    portal:
        flashy:
            primal: "dick"
            velvet: [
                { inventive: "hatchet" }
                { olivine: "home" }
                { parenthesis: "salmonberry" }
            ]
        impoverish: "beechwood"
    huge:
        incomplete:
            stake: "censorial"
            inscription: [
                { pigeonberry: { coffee: "accede" } }
                { cloy: { glutamate: "areawide" } }
                null
                { silken: "gist" }
                { dense: "eke" }
                { dessert: { hookup: "beet" } }
            ]
    sorghum:
        weedy:
            roadbed: "impatient"
    bouquet: [
        "insurrect"
        "maltreat"
        "adenoma"
        "boatswain"
    ]
    entice:
        car:
            guess: "colloquia"
            boycott: [
                [
                    {
                        scurry:
                            gabardine:
                                houseboat: "universal"
                                throttle: [
                                    {
                                        potential: ["blubber", "isochronous"]
                                        moss: { wellington: "dementia" }
                                    }
                                ]
                        humility: "fingertip"
                    }
                    {
                        sprout:
                            grouchy:
                                conversion: "neutronium"
                                goddess: [
                                    {
                                        endometriosis: ["play", "play-android-expand"]
                                        launder: { comparator: "many" }
                                    }
                                ]
                        haphazard: "establish"
                    }
                ]
                {
                    wallboard:
                        deport: "formulate"
                        stone: [
                            { sill: "dodecahedral" }
                        ]
                }
            ]
        hath:
            blob: [{search: "purview"}]
    deed:
        proctor:
            squall: "whereupon"
            invariable: [
                [
                    {
                        prodigal:
                            cable:
                                associate: "gear"
                                lemur: [
                                    { sacrament: "apothegm" }
                                    { grant: "tousle" }
                                ]
                        homotopy: 39
                        permitted: {}
                    }
                ]
            ]
        hysteron:
            carob: [{fluke: "kite"}]
    noble:
        whodunit:
            inglorious: "shield"
            sputter: [
                [
                    {
                        toroid:
                            boletus:
                                indentation: "upright"
                                vertices: [
                                    [
                                        {
                                            pupal:
                                                extremum: "arrangeable"
                                                decryption: [
                                                    { stopcock: "digestible" }
                                                    { astute: "binaural" }
                                                ]
                                        }
                                        {
                                            consultative:
                                                attire: "cute"
                                                reinforce: [
                                                    {
                                                        comma: "civic"
                                                        scour: "auger"
                                                    }
                                                    [
                                                        {
                                                            levity: "denominate"
                                                            seriatim: "trail"
                                                        }
                                                        {
                                                            sherry: "congregate"
                                                        }
                                                        {
                                                            freedom: "telegraph"
                                                            narrow: "commensurate"
                                                        }
                                                        {
                                                            crocus: "imperceptible"
                                                            swishy: "triable"
                                                        }
                                                        {
                                                            sunny: "barkeep"
                                                        }
                                                        {
                                                            euphorbia: "thyself"
                                                            gonadotropic: "thirst"
                                                        }
                                                        {
                                                            gloom: "southernmost"
                                                            stalk: "imposture"
                                                        }
                                                        {
                                                            midweek: "inversion"
                                                        }
                                                        {
                                                            brushfire: "communicable"
                                                            schism: "repellent"
                                                        }
                                                    ]
                                                ]
                                        }
                                    ]
                                ]
                        hark: {
                            blood: 11
                            committeeman: 48
                            oldy: 9
                        }
                        thousandfold: {}
                    }
                    {
                        profound:
                            blotch:
                                muse: "pinscher"
                                chargeable: [
                                    { motor: "jamboree" }
                                    { way: "immemorial" }
                                ]
                        pew: {
                            northerly: 5
                            befuddle: 16
                            chandler: 46
                        }
                        armpit: {}
                        caddy: ["satiate"]
                    }
                    {
                        inceptor:
                            plushy:
                                soiree: "idolatry"
                                decorticate: [
                                    { surpass: "shuddery" }
                                    { boat: "monastery" }
                                ]
                        nichrome: {
                            ductwork: 18
                            recruit: 47
                            juggle: 25
                        }
                        bluish: {}
                        pentane: ["fishery"]
                    }
                    {
                        sturgeon:
                            watery:
                                scathing: "include"
                                brethren: [
                                    [
                                        {
                                            pap:
                                                wold: "gain"
                                                inexperience: [
                                                    { guerdon: "dubious" }
                                                    { rebellious: "theologian" }
                                                ]
                                        }
                                        {
                                            bestiary:
                                                midshipman: "sophism"
                                                swivel: [
                                                    {
                                                        tablespoonful: "chirp"
                                                        fierce: "chase"
                                                    }
                                                    [
                                                        {
                                                            poisonous: "euglena"
                                                            luminance: "retention"
                                                        }
                                                        {
                                                            rotate: "exorcism"
                                                            puzzle: "populist"
                                                        }
                                                        {
                                                            mushroom: "nitrous"
                                                            fracture: "southward"
                                                        }
                                                        {
                                                            puppy: "goober"
                                                            bipartisan: "laura"
                                                        }
                                                        {
                                                            calculate: "affair"
                                                        }
                                                        {
                                                            paschal: "implantation"
                                                            coldhearted: "aquarium"
                                                        }
                                                        {
                                                            romp: "dreadful"
                                                            chalk: "mitt"
                                                            rooky: "prolong"
                                                        }
                                                    ]
                                                ]
                                        }
                                    ]
                                ]
                        pupa: {
                            housefly: 45
                            causal: 29
                            repartee: 2
                        }
                        movie: {}
                        limpet: ["citrate"]
                    }
                    {
                        concerti:
                            surgery:
                                proctor: "corpsmen"
                                plugging: [
                                    [
                                        {
                                            driven:
                                                crest: "font"
                                                extrema: [
                                                    { waveform: "assign" }
                                                    { sourdough: "appellate" }
                                                ]
                                        }
                                        {
                                            torpor:
                                                ether: "client"
                                                snigger: [
                                                    {
                                                        wean: "amethyst"
                                                        scum: "sonny"
                                                        kneel: "white"
                                                    }
                                                    [
                                                        {
                                                            opportune: "keelson"
                                                            antiphonal: { austenite: "roughcast" }
                                                        }
                                                        {
                                                            craggy: "heliocentric"
                                                            sacrosanct: { demystify: "hydrometer" }
                                                        }
                                                        {
                                                            bearberry: "beeswax"
                                                            weedy: { officialdom: "bathroom" }
                                                        }
                                                        {
                                                            gazette: "iterate"
                                                            uphold: { rock: "recruit" }
                                                        }
                                                        { pensive: "waterline" }
                                                        {
                                                            estrange: "transportation"
                                                            corset: "proscription"
                                                        }
                                                    ]
                                                ]
                                        }
                                    ]
                                ]
                        scum: {
                            contaminate: 12
                            daybed: 47
                            cornfield: 11
                        }
                        swampy: {}
                        nephew: ["racemose"]
                    }
                ]
            ]
        chairmen:
            historian: [{youngster: "lacuna"}]
    straw:
        foolproof:
            bemoan: "until"
            restaurant: [
                { professor: "contravene" }
                { grieve: "prurient" }
                { siltstone: "geochemical" }
                {
                    leapfrog: "slaughter"
                }
                {
                    chimera: "doctrinal"
                }
            ]
        supposable:
            none: [{astrophysical: "hatred"}]
    leafy:
        orange:
            carcinoma: "candid"
            inauthentic: [
                { medial: "lummox" }
                { bake: "paramagnet" }
                { casino: "camellia" }
            ]
        inter:
            true: [{seizure: "bacteria"}]
    florican:
        lukewarm:
            menhaden: "dogmatism"
            twaddle: [
                { implicate: "oxcart" }
                { iconoclasm: "sat" }
            ]
    carbide:
        process:
            thallium: "bourgeoisie"
            farewell: [
                "claim"
            ]
    buttermilk:
        graviton:
            reconnaissance: "widen"
            rectitude: [
                "vulture"
            ]
    must:
        perform:
            grail: "dear"
            sunny: [
                "chock"
            ]
    hysteron:
        freeze:
            onomatopoeia: "counterflow"
            din: [
                { college: "pickup" }
                { brownish: "piggish" }
                { propensity: "forgetting" }
                { aurora: "melt" }
                { nostril: "windy" }
                { inundate: "minutemen" }
                { struggle: "haplology" }
                { indicate: "abalone" }
                { reducible: "endogamous" }
            ]
    exorcism:
        enchantress:
            wig: "shifty"
            syphilitic: [
                [
                    {
                        envious:
                            riverine:
                                legitimate: "raincoat"
                                wainscot: [
                                    { sagittal: "zest" }
                                    {
                                        heathen: "stateroom"
                                        missive: "cloddish"
                                    }
                                    { abbas: "verdant" }
                                    { coon: "don" }
                                    { yarrow: "extensor" }
                                    { garner: "incantation" }
                                    { homozygous: "rickshaw" }
                                    { pejorative: "ostracod" }
                                    { stationmaster: "fleabane" }
                                    { recess: "wistful" }
                                ]
                        comatose: "proper"
                        devastate: ["epithelial"]
                    }
                    {
                        needham:
                            prig:
                                antler: "slothful"
                                lure: [
                                    { orography: "oxcart" }
                                    [
                                        {
                                            cornbread: "rancid"
                                            bonze: "abrasive"
                                            dextrose: 45
                                            patriarch: 23
                                            deadwood: 19
                                            deign: "talkie"
                                            applicant: { abridgment: "chi" }
                                        }
                                        {
                                            mission: "squeak"
                                            gazette: "transport"
                                            depose: 15
                                            scatterbrain: 39
                                            domain: 47
                                            stinkbug: "sing"
                                            raincoat: { crazy: "duration" }
                                        }
                                        {
                                            screechy: "wagoneer"
                                            peal: "convene"
                                            bluebook: "input"
                                            talcum: "glom"
                                            storekeep: false
                                            tag: "inductee"
                                            armament: { taxied: "harmonic" }
                                        }
                                        {
                                            martial: "dreamboat"
                                            bibliography: "ma"
                                            fortune: "salmonberry"
                                            hop: "dual"
                                            misogyny:
                                                lumbermen: "prokarlumbermenotic"
                                                actuate: "litany"
                                                hydrophone: "hydrophone"
                                                social: "eastern"
                                                shaman: "shaman"
                                                copolymer: "businessman"
                                                sensitive: "stagecoach"
                                                ecumenist: "slavery"
                                                laughter: "laughter"
                                                adorn: "adorn"
                                                grammatic: "globule"
                                                bulb: "mung"
                                            mutilate: { exhortation: "paunch" }
                                        }
                                    ]
                                ]
                        feint: "gauche"
                        derail: ["de"]
                    }
                    {
                        lift:
                            melodrama:
                                gyp: "abstention"
                                elephantine: [
                                    [
                                        {
                                            abrupt: "surrey"
                                            iodate: "serape"
                                            footage: { colza: "diatonic" }
                                        }
                                        {
                                            stricken: "deduce"
                                            penguin: "menfolk"
                                            couch: { arc: "tridiagonal" }
                                        }
                                        {
                                            still: "torrent"
                                            first: "equatorial"
                                            knurl: { we: "expound" }
                                        }
                                    ]
                                ]
                        policemen: "shirt"
                        wacke: ["lay"]
                    }
                    {
                        progressive:
                            counterproductive:
                                nightclub: "prowl"
                                quaff: [
                                    { tollgate: "stool" }
                                    { mana: "miterwort" }
                                    { slogan: "newt" }
                                    { confect: "electoral" }
                                    { wildflower: "greenhouse" }
                                    { preachy: "victorious" }
                                ]
                        tropospheric: "mother"
                        expansible: ["guzzle"]
                    }
                ]
            ]
        appearance:
            incommutable: [{rotenone: "subtle"}]
    lurid:
        ken:
            chin: "hundredth"
        circumcircle:
            denotation: [{iridium: "accountant"}]
    mirror:
        bassinet:
            uproar: "empower"
            hepatitis: [
                { gelable: "sweet" }
                { herbivorous: "boletus" }
                { jellyfish: "pacify" }
            ]
    grandiloquent:
        televise:
            bribery: "pacifist"
    bullhide:
        multiplexor:
            contribution: "fraction"
            newspapermen: [
                { textural: "lopseed" }
                { twine: "band" }
            ]
    rabbinate:
        ventricle:
            hindsight: "coypu"
            bye: [
                { sulfur: "brigand" }
                { pterodactyl: "alacrity" }
                { shuffle: "theism" }
            ]
    putrid: { hydrosphere: "aster" }
    posy:
        invade:
            helmsmen: "dispensate"
            rampage: [
                { backscatter: "carriage" }
                { whisper: "ordain" }
                { cologne: "beep" }
            ]
        rotenone: "proteolytic"
    hamster:
        checkerberry:
            retroactive: "concurring"
            chrysalis: [
                { polarogram: { gherkin: "audiotape" } }
                { esteem: { breakage: "beebread" } }
                null
                { heath: "shown" }
                { executrix: "aberrate" }
                { hard: { millinery: "necromantic" } }
            ]
    traverse:
        sledge:
            runway: "corralled"
    presto: [
        "tanh"
        "embrittle"
        "muzzle"
        "checkerboard"
    ]
}

