from macov.core import node
from macov.transform.import_ import *
import unittest

class ImportTest(unittest.TestCase):
    def test_add_to_empty(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("bar")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_module_as(input_node, "foo/bar", "bar")

        self.assertEqual(expected_node, output_node)

    def test_add_to_non_empty(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("boo/qux"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("qux"),
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("boo/qux"),
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("qux"),
                        ),
                        node.Param(
                            node.Id("bar"),
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_module_as(input_node, "foo/bar", "bar")

        self.assertEqual(expected_node, output_node)

    def test_add_to_existing(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("bar")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_module_as(input_node, "foo/bar", "bar")

        self.assertEqual(input_node, output_node)

    def test_replace_existing(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/goo"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("qux")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/dog"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("qux")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_module_as(input_node, "foo/dog", "qux")

        self.assertEqual(expected_node, output_node)

    def test_rename_existing(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("qux")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("bar")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_module_as(input_node, "foo/bar", "bar")

        self.assertEqual(expected_node, output_node)

    def test_add_prop_to_empty(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([node.String("foo/bar")]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("goo"), node.Id("goo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_prop_as(input_node, "foo/bar", "goo", "goo")

        self.assertEqual(expected_node, output_node)

    def test_add_prop_if_parent_imported(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([node.String("foo/bar")]),
                node.Function(
                    [
                        node.Param(
                            node.Id("qux")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("qux"),
                        ),
                        node.Param(
                            node.Object([
                                node.Prop(node.String("goo"), node.Id("goo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_prop_as(input_node, "foo/bar", "goo", "goo")

        self.assertEqual(expected_node, output_node)

    def test_add_prop_to_non_empty(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("bar/baz"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("baz")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("bar/baz"),
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("baz"),
                        ),
                        node.Param(
                            node.Object([
                                node.Prop(node.String("goo"), node.Id("goo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_prop_as(input_node, "foo/bar", "goo", "goo")

        self.assertEqual(expected_node, output_node)

    def test_add_prop_to_existing_object(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("qux"), node.Id("qux")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("qux"), node.Id("qux")),
                                node.Prop(node.String("goo"), node.Id("goo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_prop_as(input_node, "foo/bar", "goo", "goo")

        self.assertEqual(expected_node, output_node)

    def test_override_prop(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("goo"), node.Id("qux")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("goo"), node.Id("goo")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_prop_as(input_node, "foo/bar", "goo", "goo")

        self.assertEqual(expected_node, output_node)

    def test_add_property_nested_to_empty(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                ]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(
                                    node.String("goo"),
                                    node.Object([
                                        node.Prop(node.String("qux"), node.Id("qux")),
                                    ])
                                ),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_prop_as(input_node, "foo/bar", "goo.qux", "qux")

        self.assertEqual(expected_node, output_node)

    def test_override_nested_property(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(
                                    node.String("goo"),
                                    node.Object([
                                        node.Prop(node.String("qux"), node.Id("qux")),
                                    ])
                                ),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(
                                    node.String("goo"),
                                    node.Object([
                                        node.Prop(node.String("qux"), node.Id("baz")),
                                    ])
                                ),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = import_prop_as(input_node, "foo/bar", "goo.qux", "baz")

        self.assertEqual(expected_node, output_node)

    def test_remove_simple_import(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Id("bar")
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = remove_import(input_node, "bar")

        self.assertEqual(expected_node, output_node)

    def test_remove_import_by_property(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("baz"), node.Id("baz")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = remove_import(input_node, "baz")

        self.assertEqual(expected_node, output_node)

    def test_remove_property_import_not_last(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                    node.String("goo/qux"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("baz"), node.Id("baz")),
                            ]),
                        ),
                        node.Param(
                            node.Object([
                                node.Prop(node.String("qux"), node.Id("qux")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("goo/qux"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(node.String("qux"), node.Id("qux")),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = remove_import(input_node, "baz")
        self.assertEqual(expected_node, output_node)

    def test_remove_import_by_property_nested(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(
                                    node.String("baz"),
                                    node.Object([
                                        node.Prop(
                                            node.String("fox"),
                                            node.Id("fox")
                                        )
                                    ])
                                ),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([]),
                node.Function(
                    [
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = remove_import(input_node, "fox")

        self.assertEqual(expected_node, output_node)

    def test_remove_import_by_property_nested_not_last(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(
                                    node.String("box"),
                                    node.Id("box")
                                ),
                                node.Prop(
                                    node.String("baz"),
                                    node.Object([
                                        node.Prop(
                                            node.String("fox"),
                                            node.Id("fox")
                                        )
                                    ])
                                ),
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Object([
                                node.Prop(
                                    node.String("box"),
                                    node.Id("box")
                                )
                            ])
                        ),
                    ],
                    node.Block([
                    ])
                ),
            ]
        )

        output_node = remove_import(input_node, "fox")

        self.assertEqual(expected_node, output_node)

    def test_remove_import_by_array(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Array([node.Id("baz")]),
                        ),
                    ],
                    node.Block([
                    ])
                )
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                ]),
                node.Function(
                    [],
                    node.Block([
                    ])
                )
            ]
        )

        output_node = remove_import(input_node, "baz")

        self.assertEqual(expected_node, output_node)

    def test_remove_import_by_array_not_last(self) -> None:
        input_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Array([
                                node.Id("baz"),
                                node.Id("bar"),
                            ]),
                        ),
                    ],
                    node.Block([
                    ])
                )
            ]
        )

        expected_node = node.Call(
            node.Id("define"),
            [
                node.Array([
                    node.String("foo/bar"),
                ]),
                node.Function(
                    [
                        node.Param(
                            node.Array([
                                node.Id("bar"),
                            ]),
                        ),
                    ],
                    node.Block([
                    ])
                )
            ]
        )

        output_node = remove_import(input_node, "baz")

        self.assertEqual(expected_node, output_node)
