from macov.core import atom
from macov.reader.scanner.util import ACCESSORS, atom_set
from macov.util import trampoline
from pyrsistent import l

@trampoline.wrap
def filter_unwanted_terminators(atoms, filtered_atoms=l()):
    if not atoms:
        return trampoline.result(filtered_atoms)

    init, last = atoms.rest, atoms.first

    if last.type == atom.Terminator.type and _should_skip_terminator(before=init, after=filtered_atoms):
        return trampoline.call(filter_unwanted_terminators)(init, filtered_atoms)

    return trampoline.call(filter_unwanted_terminators)(init, filtered_atoms.cons(last))

@trampoline.wrap
def filter_terminators_before_dot(atoms, filtered_atoms=l()):
    if not atoms:
        return trampoline.result(filtered_atoms)

    head, tail = atoms.first, atoms.rest

    if head.type in ACCESSORS and filtered_atoms and filtered_atoms.first.type == atom.Terminator.type:
        return trampoline.call(filter_terminators_before_dot)(tail, filtered_atoms.rest.cons(head))

    return trampoline.call(filter_terminators_before_dot)(tail, filtered_atoms.cons(head))

def _should_skip_terminator(before, after):
    if not (before and after):
        return True

    if before.first.type in _SKIP_TERMINATORS_AFTER or after.first.type in _SKIP_TERMINATORS_BEFORE:
        return True

    return False

_SKIP_TERMINATORS_AFTER = atom_set(
    atom.Comma,
    atom.Indent,
    atom.LeftBrace,
    atom.LeftBracket,
    atom.LeftParen,
    atom.ParamStart,
    atom.ParenCallStart,
    atom.StringStart,
    atom.WhitespaceCallStart,
)

_SKIP_TERMINATORS_BEFORE = atom_set(
    atom.Concat,
    atom.Else,
    atom.Outdent,
    atom.ParamEnd,
    atom.ParenCallEnd,
    atom.RightBrace,
    atom.RightBracket,
    atom.RightParen,
    atom.StringEnd,
    atom.Terminator,
    atom.WhitespaceCallEnd,
)
