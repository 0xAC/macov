from macov.core import atom
from macov.reader import scan
import unittest

class ScannerIndexableTest(unittest.TestCase):
    def test_id(self) -> None:
        code = "foo[bar]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

    def test_at(self) -> None:
        code = "@[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_this(self) -> None:
        code = "this[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.This(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_super(self) -> None:
        code = "super[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Super(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_property(self) -> None:
        code = "foo.bar[baz]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Dot(),
                atom.Property("bar"),
                atom.IndexStart(),
                atom.Id("baz"),
                atom.IndexEnd(),
            ]
        )

    def test_paren(self) -> None:
        code = "(foo)[bar]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftParen(),
                atom.Id("foo"),
                atom.RightParen(),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

    def test_number(self) -> None:
        code = "123[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number("123"),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_infinity(self) -> None:
        code = "Infinity[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Infinity(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_nan(self) -> None:
        code = "NaN[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Nan(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_string(self) -> None:
        code = '"foo"[bar]'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String("foo"),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

    def test_interpolated_string(self) -> None:
        code = '"#{foo}"[bar]'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.Id("foo"),
                atom.StringEnd('"'),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

    def test_regex(self) -> None:
        code = "/[a-z]/[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Regex("[a-z]", delimiter="/"),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_bool(self) -> None:
        code = "false[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.False_(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_null(self) -> None:
        code = "null[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Null(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_undefeined(self) -> None:
        code = "undefined[foo]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Void(),
                atom.IndexStart(),
                atom.Id("foo"),
                atom.IndexEnd(),
            ]
        )

    def test_brace(self) -> None:
        code = "{foo}[bar]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.RightBrace(),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

    def test_call(self) -> None:
        code = "foo(bar)[baz]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Id("bar"),
                atom.ParenCallEnd(),
                atom.IndexStart(),
                atom.Id("baz"),
                atom.IndexEnd(),
            ]
        )

    def test_bracket(self) -> None:
        code = "[foo][bar]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBracket(),
                atom.Id("foo"),
                atom.RightBracket(),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

    def test_index(self) -> None:
        code = "foo[bar][baz]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
                atom.IndexStart(),
                atom.Id("baz"),
                atom.IndexEnd(),
            ]
        )

    def test_exist(self) -> None:
        code = "foo?[bar]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Exist(),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

    def test_soak(self) -> None:
        code = "foo::[bar]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Prototype(),
                atom.IndexStart(),
                atom.Id("bar"),
                atom.IndexEnd(),
            ]
        )

if __name__ == "__main__":
    unittest.main()
