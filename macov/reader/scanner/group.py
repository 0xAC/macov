from macov.core import atom
from macov.util import trampoline
from functools import reduce
from pyrsistent import l, plist

class Group:
    type = "group"

    def __init__(self, atoms):
        self._atoms = plist(atoms)

    @property
    def atoms(self):
        return self._atoms

    def __eq__(self, other):
        return isinstance(other, Group) and tuple(self._atoms) == tuple(other.atoms)

    def __str__(self):
        return "group[ {} ]".format(" ".join(map(str, self._atoms)))

def unfold(atoms, transform=lambda it: it):
    return _unfold(transform, l(), l(), plist(transform(atoms)))

def fold(atoms):
    _, new_atoms = reduce(lambda state, atom: _fold(atom, *state), plist(atoms), (l(), l()))

    return new_atoms

@trampoline.wrap
def _unfold(transform, accumulator, global_stack, local_stack):
    if not local_stack:
        if not global_stack:
            return trampoline.result(accumulator)

        return trampoline.call(_unfold)(transform, accumulator, global_stack.rest, global_stack.first)

    head, tail = local_stack.first, local_stack.rest

    if isinstance(head, Group):
        return trampoline.call(_unfold)(transform, accumulator, global_stack.cons(tail), plist(transform(head.atoms)))

    return trampoline.call(_unfold)(transform, accumulator.cons(head), global_stack, tail)

def _fold(head, global_stack, local_stack):
    if isinstance(head, atom.OpeningDelimiter):
        return global_stack.cons(local_stack.cons(head)), l()

    if isinstance(head, atom.ClosingDelimiter):
        return global_stack.rest, global_stack.first.cons(Group(local_stack)).cons(head)

    return global_stack, local_stack.cons(head)
