from macov.core import atom, node
from macov.reader import parse
import unittest

class ParserInvocationTest(unittest.TestCase):
    def test_simple_invocation(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                    ]
                ),
            ])
        )

    def test_paren_expression_invocation(self) -> None:
        atoms = [
            atom.LeftParen(),
            atom.Id("foo"),
            atom.RightParen(),
            atom.ParenCallStart(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                    ]
                ),
            ])
        )

    def test_new_expression(self) -> None:
        atoms = [
            atom.New(),
            atom.Id("bar"),
            atom.ParenCallStart(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.New(
                    node.Id("bar"),
                    [
                    ]
                ),
            ])
        )

    def test_invocation_with_one_parameter(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.String("bar"),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.String("bar"),
                    ]
                ),
            ])
        )

    def test_invocation_with_two_parameters(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.String("bar"),
            atom.Comma(),
            atom.String("baz"),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.String("bar"),
                        node.String("baz"),
                    ]
                ),
            ])
        )

    def test_invocation_with_two_parameters_opt_comma(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.String("bar"),
            atom.Comma(),
            atom.String("baz"),
            atom.Comma(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.String("bar"),
                        node.String("baz"),
                    ]
                ),
            ])
        )

    def test_invocation_with_two_parameters_terminator_separated(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.String("bar"),
            atom.Terminator(),
            atom.String("baz"),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.String("bar"),
                        node.String("baz"),
                    ]
                ),
            ])
        )

    def test_invocation_with_array(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.LeftBracket(),
            atom.String("bar"),
            atom.Comma(),
            atom.String("baz"),
            atom.RightBracket(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.Array([
                            node.String("bar"),
                            node.String("baz"),
                        ]),
                    ]
                ),
            ])
        )

    def test_invocation_with_function(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.ParamStart(),
            atom.Id("bar"),
            atom.Comma(),
            atom.Id("baz"),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.Function(
                            [
                                node.Param(node.Id("bar")),
                                node.Param(node.Id("baz")),
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    @unittest.skip("DELETE ME")
    def test_invocation_with_block_function_as_last_parameter(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.Id("bar"),
            atom.Comma(),
            atom.ParamStart(),
            atom.Id("baz"),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Indent(),
            atom.Id("qux"),
            atom.Outdent(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.Id("bar"),
                        node.Function(
                            [
                                node.Param(node.Id("baz")),
                            ],
                            node.Block([
                                node.Id("qux")
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_invocation_with_object(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.LeftBrace(),
            atom.Id("bar"),
            atom.Comma(),
            atom.Id("baz"),
            atom.RightBrace(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.Object([
                            node.Prop(node.String("bar"), node.Id("bar")),
                            node.Prop(node.String("baz"), node.Id("baz")),
                        ]),
                    ]
                ),
            ])
        )

    def test_invocation_with_array_and_function_and_object(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.ParenCallStart(),
            atom.LeftBracket(),
            atom.String("bar"),
            atom.Comma(),
            atom.String("baz"),
            atom.RightBracket(),
            atom.Comma(),
            atom.ParamStart(),
            atom.Id("qux"),
            atom.Comma(),
            atom.Id("goo"),
            atom.ParamEnd(),
            atom.ThinArrow(),
            atom.Comma(),
            atom.LeftBrace(),
            atom.Id("boo"),
            atom.Comma(),
            atom.Id("tux"),
            atom.RightBrace(),
            atom.ParenCallEnd(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.Array([
                            node.String("bar"),
                            node.String("baz"),
                        ]),
                        node.Function(
                            [
                                node.Param(node.Id("qux")),
                                node.Param(node.Id("goo")),
                            ],
                            node.Block([
                            ])
                        ),
                        node.Object([
                            node.Prop(node.String("boo"), node.Id("boo")),
                            node.Prop(node.String("tux"), node.Id("tux")),
                        ]),
                    ]
                ),
            ])
        )

if __name__ == "__main__":
    unittest.main()
