from macov.core.atom import *
from macov.util import trampoline
from pyrsistent import l
from typing import Callable, List, Tuple

EOF = chr(0)

class _Cursor:
    def __init__(self, input: str, offset: int, column: int, row: int, indent: int) -> None:
        self._input = input
        self._offset = offset
        self._column = column
        self._row = row
        self._indent = indent
        self.char = self._input[self._offset]

    @property
    def next(self) -> "_Cursor":
        new_column = 1 if self.char == "\n" else self._column + 1
        new_row = self._row + 1 if self.char == "\n" else self._row

        return _Cursor(self._input, self._offset + 1, new_column, new_row, self._indent)

    @property
    def column(self) -> int:
        return self._column

    @property
    def row(self) -> int:
        return self._row

    @property
    def indent(self) -> int:
        return self._indent

    @property
    def position(self) -> Position:
        return Position(row=self._row, column=self._column)

    def jump_by(self, offset: int) -> "_Cursor":
        if offset == 0:
            return self

        return self.next.jump_by(offset - 1)

    def update_indent(self, indent: int) -> "_Cursor":
        return _Cursor(self._input, self._offset, self._column, self._row, indent)

    def __eq__(self, other: object) -> bool:
        return isinstance(other, _Cursor) and self._row == other.row and self._column == other.column

class Raw(Atom):
    type = "raw"
    range = EMPTY_RANGE

    def __init__(self, content: str, position: Position=Position(row=1, column=1)):
        self._content = content
        self._position = position

    @property
    def position(self):
        return self._position

    @property
    def value(self):
        return f"#{{{self._content}}}"

    @property
    def content(self):
        return self._content

    def __str__(self):
        return "raw[{}]".format(self._content)

def _cursor(raw: Raw) -> "_Cursor":
    return _Cursor(raw.content.strip("\n") + "\n" + EOF, 0, raw.position.column, raw.position.row, 0)

class State:
    def __init__(self, cursor, delimiter_stack):
        self._cursor = cursor
        self._delimiter_stack = delimiter_stack
        self.char = cursor.char

    @property
    def next(self) -> "State":
        return State(self._cursor.next, self._delimiter_stack)

    @property
    def column(self) -> int:
        return self._cursor.column

    @property
    def row(self) -> int:
        return self._cursor.row

    @property
    def indent(self) -> int:
        return self._cursor.indent

    @property
    def position(self) -> Position:
        return self._cursor.position

    def jump_by(self, offset: int) -> "State":
        return State(self._cursor.jump_by(offset), self._delimiter_stack)

    def update_indent(self, indent: int) -> "State":
        return State(self._cursor.update_indent(indent), self._delimiter_stack)

    def push_delimiter(self, delimiter) -> "State":
        return State(self._cursor, self._delimiter_stack.cons(delimiter))

    def pop_delimiter(self, delimiter) -> "State":
        if delimiter.type != self._delimiter_stack.first.pair:
            message = "expected '{}' but found '{}'".format(self._delimiter_stack.first.pair, delimiter.display_type)
            raise LexicalException(message, delimiter.range.begin)

        return State(self._cursor, self._delimiter_stack.rest)

def state(raw):
    return State(_cursor(raw), l())

CALLABLE = atom_set(
    At,
    Exist,
    Id,
    ParenCallEnd,
    Property,
    RightBracket,
    RightParen,
    Super,
    This,
)

INDEXABLE = CALLABLE | atom_set(
    False_,
    IndexEnd,
    Infinity,
    Nan,
    Null,
    Number,
    ParenCallEnd,
    Regex,
    RightBrace,
    Prototype,
    String,
    StringEnd,
    True_,
    Void,
)

ACCESSORS = atom_set(
    Dot,
    DotExist,
    PrototypeExist,
    Prototype,
)

PROPERTY_STARTERS = ACCESSORS | atom_set(
    At
)

_Push = Callable[[List[Atom], Atom], List[Atom]]
_NextAtom = Callable[[List[Atom]], Tuple[Atom, List[Atom]]]

class Then(Keyword):
    "This is for internal scanner use only"

    type = "then"

def is_block_delimiter(state: State) -> bool:
    return state.char == state.next.char == state.next.next.char

@trampoline.wrap
def scan_interpolation(state: State, start: Position, text: str, level: int=1) -> Tuple[State, Raw]:
    def update_level():
        if state.char == "}":
            return level - 1

        if state.char == "{":
            return level + 1

        return level

    if not level:
        return trampoline.result((state, Raw(text[2:-1], position=Position(column=start.column + 2, row=start.row))))

    new_level = update_level()

    found, new_state, strings = scan_string_start(state, dq())

    (new_text, final_state) = ("".join(it.value for it in strings), new_state) if found else (state.char, state.next)

    return trampoline.call(scan_interpolation)(final_state, start, text + new_text, new_level)
