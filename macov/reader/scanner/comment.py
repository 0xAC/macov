from macov.util import trampoline

def scan_comment_start(state, atoms):
    if state.char == "#":
        if state.next.char == "#" and state.next.next.char == "#":
            new_state, comment_value = scan_block_comment(state.next.next.next)
        else:
            new_state, comment_value = scan_comment(state.next)

        return True, new_state, atoms

    return False, state, atoms

@trampoline.wrap
def scan_comment(state, value=""):
    if state.char == "\n":
        return trampoline.result((state, value))

    return trampoline.call(scan_comment)(state.next, value + state.char)

@trampoline.wrap
def scan_block_comment(state, value=""):
    if state.char != "#":
        return trampoline.call(scan_block_comment)(state.next, value + state.char)

    if state.next.char != "#":
        return trampoline.call(scan_block_comment)(state.next, value + state.char)

    if state.next.next.char != "#":
        return trampoline.call(scan_block_comment)(state.next, value + state.char)

    return trampoline.result((state.next.next.next, value))
