from macov.core import node
from macov.reader import parse, scan
from textwrap import dedent
import unittest

class ParserIntegrationTest(unittest.TestCase):
    def test_simplest(self) -> None:
        code = """define [], () ->"""

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                        ]),
                        node.Function(
                            [
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_function_with_splats_param(self) -> None:
        code = "(foo...) ->"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.SplatExpr(
                                node.Id("foo")
                            )
                        )
                    ],
                    node.Block([
                    ])
                )
            ])
        )

    def test_post_inc(self) -> None:
        code = "foo++"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.PostfixIncExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_post_dec(self) -> None:
        code = "foo--"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.PostfixDecExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_pre_inc(self) -> None:
        code = "++foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.PrefixIncExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_pre_dec(self) -> None:
        code = "--foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.PrefixDecExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_regex(self):
        code = "/foo/"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Regex("foo", delimiter="/")
            ])
        )

    def test_block_regex(self):
        code = "///foo///"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Regex("foo", delimiter="///")
            ])
        )

    def test_block_regex_with_interpolation(self):
        code = "///foo#{ bar }baz///"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.RegexInterpolation([
                    node.Regex("foo", delimiter=""),
                    node.Id("bar"),
                    node.Regex("baz", delimiter=""),
                ])
            ])
        )

    def test_multiline(self):
        code = dedent("""
            define [
            ], (
            ) ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                        ]),
                        node.Function(
                            [
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_multiline_single_value(self):
        code = dedent("""
            define [
                "foo"
            ], (
                foo
            ) ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                            node.String("foo"),
                        ]),
                        node.Function(
                            [
                                node.Param(node.Id("foo")),
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_oneline_single_value(self):
        code = """define ["foo"], (foo) ->"""

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                            node.String("foo"),
                        ]),
                        node.Function(
                            [
                                node.Param(node.Id("foo")),
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_multiline_two_values(self):
        code = dedent("""
            define [
                "foo"
                "bar"
            ], (
                foo
                bar
            ) ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                            node.String("foo"),
                            node.String("bar"),
                        ]),
                        node.Function(
                            [
                                node.Param(node.Id("foo")),
                                node.Param(node.Id("bar")),
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_assignment_simple(self):
        code = "foo = bar"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.AssignExpr(node.Id("foo"), node.Id("bar")),
            ])
        )

    def test_accessor(self) -> None:
        code = dedent("""
            foo.bar
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Accessor(
                    node.Id("foo"),
                    node.String("bar"),
                ),
            ]),
        )

    def test_multiple_lines(self) -> None:
        code = dedent("""
            foo
            bar
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Id("foo"),
                node.Id("bar"),
            ]),
        )

    def test_return_statement_empty(self) -> None:
        code = "return"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Return(
                    node.Void()
                )
            ])
        )

    def test_throw(self) -> None:
        code = "throw new foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Throw(
                    node.New(
                        node.Id("foo"),
                        [
                        ]
                    )
                ),
            ])
        )

    def test_type_of(self) -> None:
        code = "foo typeof bar"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.TypeOf(
                            node.Id("bar")
                        ),
                    ]
                ),
            ])
        )

    def test_post_if(self) -> None:
        code = "return foo if bar < baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.If(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.LessThanCmp(
                                node.Id("baz")
                            )
                        ]
                    ),
                    node.Block([
                        node.Return(
                            node.Id("foo")
                        )
                    ]),
                    node.Block([])
                ),
            ])
        )

    def test_post_if_with_return_accessor_experssion(self) -> None:
        code = "return foo.bar if baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.If(
                    node.Id("baz"),
                    node.Block([
                        node.Return(
                            node.Accessor(
                                node.Id("foo"),
                                node.String("bar"),
                            )
                        )
                    ]),
                    node.Block([])
                )
            ])
        )

    def test_post_unless(self) -> None:
        code = "return foo unless bar < baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Unless(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.LessThanCmp(
                                node.Id("baz")
                            )
                        ]
                    ),
                    node.Block([
                        node.Return(
                            node.Id("foo")
                        )
                    ]),
                    node.Block([])
                ),
            ])
        )

    def test_return_statement(self) -> None:
        code = "return foo + bar * baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Return(
                    node.AddExpr(
                        node.Id("foo"),
                        node.MulExpr(
                            node.Id("bar"),
                            node.Id("baz"),
                        )
                    )
                )
            ])
        )

    def test_string_interpolation_nested_double_quote(self) -> None:
        code = r'"foo #{ "bar #{ baz } qux" } goo"'

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.StringInterpolation(
                    '"',
                    [
                        node.String("foo "),
                        node.StringInterpolation(
                            '"',
                            [
                                node.String("bar "),
                                node.Id("baz"),
                                node.String(" qux"),
                            ]
                        ),
                        node.String(" goo"),
                    ]
                ),
            ])
        )

    def test_string_interpolation_empty(self) -> None:
        code = r'"#{ }"'

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.StringInterpolation(
                    '"',
                    [
                    ]
                ),
            ])
        )

    def test_number_literal(self) -> None:
        code = "123"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Number("123"),
            ])
        )

    def test_nan_literal(self) -> None:
        code = "NaN"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Nan(),
            ])
        )

    def test_infinity_literal(self) -> None:
        code = "Infinity"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Infinity(),
            ])
        )

    def test_true_literal(self) -> None:
        code = "true"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.True_(),
            ])
        )

    def test_false_literal(self) -> None:
        code = "false"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.False_(),
            ])
        )

    def test_null_literal(self) -> None:
        code = "foo is null"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("foo"),
                    [
                        node.IsCmp(
                            node.Null()
                        ),
                    ]
                ),
            ])
        )

    def test_undefined_literal(self) -> None:
        code = "foo is undefined"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("foo"),
                    [
                        node.IsCmp(
                            node.Void()
                        ),
                    ]
                ),
            ])
        )

    def test_assignment(self) -> None:
        code = dedent("""
            foo = bar
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.AssignExpr(
                    node.Id("foo"),
                    node.Id("bar"),
                ),
            ]),
        )

    def test_assignment_property(self) -> None:
        code = dedent("""
            foo.bar = baz
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.AssignExpr(
                    node.Accessor(
                        node.Id("foo"),
                        node.String("bar"),
                    ),
                    node.Id("baz"),
                ),
            ]),
        )

    def test_invocation_with_one_element_array_and_function_with_object(self):
        code = dedent("""
            define [
                "foo"
            ], (
                { foo }
            ) ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                            node.String("foo"),
                        ]),
                        node.Function(
                            [
                                node.Param(
                                    node.Object([
                                        node.Prop(node.String("foo"), node.Id("foo")),
                                    ])
                                ),
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_invocation_with_one_element_array_and_function_with_regular_property_object(self):
        code = dedent("""
            define [
                "foo"
            ], (
                { foo: foo }
            ) ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                            node.String("foo"),
                        ]),
                        node.Function(
                            [
                                node.Param(
                                    node.Object([
                                        node.Prop(node.String("foo"), node.Id("foo")),
                                    ])
                                ),
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

    def test_invocation_with_one_element_array_and_function_with_string_key_property_object(self):
        code = dedent("""
            define [
                "foo"
            ], (
                { "qux": qux, "bar": bar }
            ) ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("define"),
                    [
                        node.Array([
                            node.String("foo"),
                        ]),
                        node.Function(
                            [
                                node.Param(
                                    node.Object([
                                        node.Prop(node.String("qux"), node.Id("qux")),
                                        node.Prop(node.String("bar"), node.Id("bar")),
                                    ])
                                ),
                            ],
                            node.Block([
                            ])
                        ),
                    ]
                ),
            ])
        )

class IntegrationClassTest(unittest.TestCase):
    def test_with_constructor(self):
        code = dedent("""
            class Foo
                constructor: () ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("constructor"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_with_constructor_no_paren(self):
        code = dedent("""
            class Foo
                constructor: ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("constructor"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_extends(self) -> None:
        code = dedent("""
            class Foo extends Bar
                constructor: ->
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Id("Bar"),
                    [
                        node.Prop(
                            node.String("constructor"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_nested_with_trailing_expression(self) -> None:
        code = dedent("""
            class Foo
                constructor: ->

                class Bar
                    baz: ->
                        new Foo goo, qux

                fox.mux
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Class(
                    node.Id("Foo"),
                    node.Void(),
                    [
                        node.Prop(
                            node.String("constructor"),
                            node.Function(
                                [
                                ],
                                node.Block([
                                ])
                            )
                        )
                    ],
                    node.Block([
                        node.Class(
                            node.Id("Bar"),
                            node.Void(),
                            [
                                node.Prop(
                                    node.String("baz"),
                                    node.Function(
                                        [
                                        ],
                                        node.Block([
                                            node.New(
                                                node.Id("Foo"),
                                                [
                                                    node.Id("goo"),
                                                    node.Id("qux"),
                                                ]
                                            )
                                        ])
                                    )
                                )
                            ]
                        ),
                        node.Accessor(
                            node.Id("fox"),
                            node.String("mux")
                        )
                    ])
                )
            ])
        )

class FunctionParserScannerIntegrationTest(unittest.TestCase):
    def test_body_block(self) -> None:
        code = dedent("""
            ([foo]) ->
                bar
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Array([
                                node.Id("foo"),
                            ])
                        ),
                    ],
                    node.Block([
                        node.Id("bar"),
                    ])
                ),
            ])
        )

    def test_bound(self) -> None:
        code = dedent("""
            (foo) =>
                bar
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Function(
                    [
                        node.Param(
                            node.Id("foo")
                        ),
                    ],
                    node.Block([
                        node.Id("bar"),
                    ]),
                    is_bound=True
                ),
            ])
        )

class ParserExpressionTest(unittest.TestCase):
    def test_identifier(self) -> None:
        code = "foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Id("foo")
            ])
        )

    def test_super(self) -> None:
        code = "super bar"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.SuperCall([
                    node.Id("bar")
                ]),
            ])
        )

    def test_this(self) -> None:
        code = "this"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.This(),
            ])
        )

    def test_at_prototype(self) -> None:
        code = "@::foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Accessor(
                    node.Accessor(
                        node.This(),
                        node.String("prototype")
                    ),
                    node.String("foo")
                )
            ])
        )

    def test_at_property(self) -> None:
        code = "@foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Accessor(
                    node.This(),
                    node.String("foo"),
                )
            ])
        )

    def test_existence(self) -> None:
        code = "foo?"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.ExistExpr(
                    node.Id("foo"),
                )
            ])
        )

    def test_paren(self) -> None:
        code = "foo * (bar + baz)"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.MulExpr(
                    node.Id("foo"),
                    node.AddExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    )
                )
            ])
        )

    def test_dot_accessor(self) -> None:
        code = "foo.bar"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Accessor(
                    node.Id("foo"),
                    node.String("bar"),
                )
            ])
        )

    def test_unary_minus(self) -> None:
        code = "-foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.MinusExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_unary_plus(self) -> None:
        code = "+foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.PlusExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_unary_bitwise_not(self) -> None:
        code = "~foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.BitNotExpr(
                    node.Id("foo"),
                ),
            ])
        )

    def test_unary_logical_not(self) -> None:
        code = "not foo"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.NotExpr(
                    node.Id("foo"),
                ),
            ])
        )

    def test_unary_splat_expr(self) -> None:
        code = "foo bar..."

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Call(
                    node.Id("foo"),
                    [
                        node.SplatExpr(
                            node.Id("bar")
                        ),
                    ]
                ),
            ])
        )

    def test_binary_exp(self) -> None:
        code = "bar ** baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.ExpExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_add(self) -> None:
        code = "bar + baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.AddExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_sub(self) -> None:
        code = "bar - baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.SubExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_mul(self) -> None:
        code = "bar * baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.MulExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_float_div(self) -> None:
        code = "bar / baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.FloatDivExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_integer_div(self) -> None:
        code = "bar // baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.IntegerDivExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_rem(self) -> None:
        code = "bar % baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.RemExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_mod(self) -> None:
        code = "bar %% baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.ModExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_and(self) -> None:
        code = "bar & baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_logical_and(self) -> None:
        code = "bar and baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.AndExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_or(self) -> None:
        code = "bar | baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.BitOrExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_logical_or(self) -> None:
        code = "bar or baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.OrExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_xor(self) -> None:
        code = "bar ^ baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.BitXorExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_left_shift(self) -> None:
        code = "bar << baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.LeftShiftExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_zero_fill_right_shift(self) -> None:
        code = "bar >>> baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_bitwise_sign_right_shift(self) -> None:
        code = "bar >> baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.SignRightShiftExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_in(self) -> None:
        code = "bar in baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.InExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_not_in(self) -> None:
        code = "bar not in baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.NotExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    )
                ),
            ])
        )

    def test_binary_of(self) -> None:
        code = "bar of baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.OfExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_not_of(self) -> None:
        code = "bar not of baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.NotExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    )
                ),
            ])
        )

    def test_binary_instance_of(self) -> None:
        code = "bar instanceof baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.InstanceOfExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_binary_not_instance_of(self) -> None:
        code = "bar not instanceof baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.NotExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    )
                ),
            ])
        )

    def test_bin_exist(self) -> None:
        code = "bar ? baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.BinExistExpr(
                    node.Id("bar"),
                    node.Id("baz"),
                ),
            ])
        )

    def test_compare_is(self) -> None:
        code = "bar is baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_is_not(self) -> None:
        code = "bar isnt baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_greater_than_or_equal(self) -> None:
        code = "bar >= baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_less_than_or_equal(self) -> None:
        code = "bar <= baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_greater_than(self) -> None:
        code = "bar > baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(node.Id("baz"))
                    ]
                )
            ])
        )

    def test_compare_less_than(self) -> None:
        code = "bar < baz"

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(node.Id("baz"))
                    ]
                )
            ])
        )

class ObjectIntegrationTest(unittest.TestCase):
    def test_object_with_indents(self) -> None:
        code = dedent("""
            {
                foo:
                    bar: baz
            }
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Object([
                    node.Prop(
                        node.String("foo"),
                        node.Object([
                            node.Prop(
                                node.String("bar"),
                                node.Id("baz")
                            )
                        ])
                    ),
                ])
            ])
        )

class IfExpressionTest(unittest.TestCase):
    def test_if_then_else_inline(self) -> None:
        code = dedent("""foo = if bar then baz else qux""")

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.AssignExpr(
                    node.Id("foo"),
                    node.If(
                        node.Id("bar"),
                        node.Block([
                            node.Id("baz"),
                        ]),
                        node.Block([
                            node.Id("qux"),
                        ])
                    )
                ),
            ])
        )

    def test_if_else_if_else_inline(self) -> None:
        code = dedent("""if bar then baz else if qux then fox else goo""")

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.If(
                    node.Id("bar"),
                    node.Block([
                        node.Id("baz"),
                    ]),
                    node.Block([
                        node.If(
                            node.Id("qux"),
                            node.Block([
                                node.Id("fox"),
                            ]),
                            node.Block([
                                node.Id("goo"),
                            ])
                        ),
                    ])
                ),
            ])
        )

    def test_if_then_else_block(self) -> None:
        code = dedent("""
            if bar
                baz
            else
                qux
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.If(
                    node.Id("bar"),
                    node.Block([
                        node.Id("baz"),
                    ]),
                    node.Block([
                        node.Id("qux"),
                    ])
                ),
            ])
        )

    def test_if_then_else_block_multiple_expressions(self) -> None:
        code = dedent("""
            if bar
                baz
                qux
            else
                fox
                goo
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.If(
                    node.Id("bar"),
                    node.Block([
                        node.Id("baz"),
                        node.Id("qux"),
                    ]),
                    node.Block([
                        node.Id("fox"),
                        node.Id("goo"),
                    ])
                ),
            ])
        )

    def test_unless_then_else_inline(self) -> None:
        code = dedent("""foo = unless bar then baz else qux""")

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.AssignExpr(
                    node.Id("foo"),
                    node.Unless(
                        node.Id("bar"),
                        node.Block([
                            node.Id("baz"),
                        ]),
                        node.Block([
                            node.Id("qux"),
                        ])
                    )
                ),
            ])
        )

    def test_unless_then_else_block(self) -> None:
        code = dedent("""
            unless bar
                baz
            else
                qux
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Unless(
                    node.Id("bar"),
                    node.Block([
                        node.Id("baz"),
                    ]),
                    node.Block([
                        node.Id("qux"),
                    ])
                ),
            ])
        )

    def test_unless_then_else_block_multiple_expressions(self) -> None:
        code = dedent("""
            unless bar
                baz
                qux
            else
                fox
                goo
        """)

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Unless(
                    node.Id("bar"),
                    node.Block([
                        node.Id("baz"),
                        node.Id("qux"),
                    ]),
                    node.Block([
                        node.Id("fox"),
                        node.Id("goo"),
                    ])
                ),
            ])
        )

    def test_unless_else_unless_else_inline(self) -> None:
        code = dedent("""unless bar then baz else unless qux then fox else goo""")

        self.assertEqual(
            parse(scan(code)),
            node.Block([
                node.Unless(
                    node.Id("bar"),
                    node.Block([
                        node.Id("baz"),
                    ]),
                    node.Block([
                        node.Unless(
                            node.Id("qux"),
                            node.Block([
                                node.Id("fox"),
                            ]),
                            node.Block([
                                node.Id("goo"),
                            ])
                        ),
                    ])
                ),
            ])
        )

if __name__ == "__main__":
    unittest.main()
