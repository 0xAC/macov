from macov.core import node
from typing import List

def _sort_object(object: node.Object) -> node.Object:
    sorted_props = sorted(object.props, key=lambda prop: prop.key.value)

    return node.Object(sorted_props)

def _sort_params(params: List[node.Param]) -> List[node.Param]:
    return list(node.Param(_sort_object(param.value)) if isinstance(param.value, node.Object) else param for param in params)

def sort(invocation: node.Call, key=lambda x: x) -> node.Call:
    sort_key = key
    (array, function) = invocation.args

    items = array.items
    params = function.params

    paired_items = items[0:len(params)]
    dangling_items = items[len(params):]

    (new_items, new_params) = (paired_items, params) if len(paired_items) == 0 else zip(*sorted(zip(paired_items, params), key=lambda it: sort_key(it[0].value)))

    return node.Call(invocation.expr, [node.Array(list(new_items) + dangling_items), node.Function(_sort_params(new_params), function.body)])
