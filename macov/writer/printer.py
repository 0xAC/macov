import sys
from itertools import chain

from macov.core import node

_indent = 4 * " "

def _print_call(call, context):
    return context.print(call.expr) + " " + ", ".join(context.print(arg) for arg in call.args)

def _print_id(id, context):
    return id.name

def _print_array(array, context):
    sub_context = context.inc_indent_level()
    inner = sub_context.join_lines(sub_context.print(item) for item in array.items)

    return f"[{inner}{context.new_line}]"

def _print_function(function, context):
    sub_context = context.inc_indent_level()
    inner = sub_context.join_lines(sub_context.print(param) for param in function.params)

    return f"({inner}{context.new_line}) ->"

def _print_string(string, context):
    return '"{}"'.format(string.value)

def _print_object(object, context):
    sub_context = context.inc_indent_level()
    props = [sub_context.print(prop) for prop in object.props]
    single_line = f"{{ {', '.join(props)} }}"

    if not context.exceeds_margin(single_line):
        return single_line

    inner = sub_context.join_lines(props)

    return f"{{{inner}{context.new_line}}}"

def _print_param(param: node.Param, context) -> str:
    return context.print(param.value)

def _print_key(key: node.String) -> str:
    return key.value

def _print_property(prop, context):
    key = _print_key(prop.key)
    value = context.print(prop.value)

    return "{}: {}".format(key, value) if key != value else key

def _to_string(entity, context):
    return getattr(sys.modules[__name__], "_print_{}".format(entity.type))(entity, context)

class _Context:
    def __init__(self, margin, indent_level, to_string):
        self._margin = margin
        self._to_string = to_string
        self._indent_level = indent_level

    @property
    def new_line(self):
        return "\n" + _indent * self._indent_level

    @property
    def indent_level(self):
        return self._indent_level

    @property
    def margin(self):
        return self._margin

    @property
    def limit(self):
        if self._margin == 0:
            return 0

        return self._margin - len(_indent) * self._indent_level

    def set_indent_level(self, new_indent_level):
        return _Context(self.margin, new_indent_level, self._to_string)

    def inc_indent_level(self):
        return self.set_indent_level(self.indent_level + 1)

    def dec_indent_level(self):
        return self.set_indent_level(self.indent_level - 1)

    def set_to_string(self, new_to_string):
        return _Context(self.margin, self.indent_level, new_to_string)

    def print(self, entity):
        return self._to_string(entity, self)

    def join_lines(self, lines):
        return self.new_line.join(chain(("",), lines))

    def exceeds_margin(self, line):
        if self._margin == 0:
            return False

        return len(line) > self.limit

def to_string(entity, margin=0):
    context = _Context(margin, indent_level=0, to_string=_to_string)

    return context.print(entity)
