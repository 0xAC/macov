from macov.core import atom, node
from macov.core.util import dict_by_type, SyntaxException
from macov.reader.parser import parse
import unittest

_unary_operators_disallowed_before_left_exp = dict_by_type(
    atom.TypeOf(),
    atom.BitNot(),
    atom.Not(),
)

class PrefixDecrementTest(unittest.TestCase):
    def test_simple(self) -> None:
        atoms = [
            atom.Dec(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PrefixDecExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_precedence_higher_than_exist(self) -> None:
        atoms = [
            atom.Dec(),
            atom.Id("foo"),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.PrefixDecExpr(
                        node.Id("foo")
                    )
                )
            ])
        )

    def test_precedence_higher_than_exp(self) -> None:
        atoms = [
            atom.Dec(),
            atom.Id("foo"),
            atom.Exp(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpExpr(
                    node.PrefixDecExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                )
            ])
        )

class PrefixIncrementTest(unittest.TestCase):
    def test_simple(self) -> None:
        atoms = [
            atom.Inc(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PrefixIncExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_precedence_higher_than_exist(self) -> None:
        atoms = [
            atom.Inc(),
            atom.Id("foo"),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.PrefixIncExpr(
                        node.Id("foo")
                    )
                )
            ])
        )

    def test_precedence_higher_than_exp(self) -> None:
        atoms = [
            atom.Inc(),
            atom.Id("foo"),
            atom.Exp(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpExpr(
                    node.PrefixIncExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                )
            ])
        )

class PostDecrementTest(unittest.TestCase):
    def test_simple(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Dec(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PostfixDecExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_precedence_higher_than_exist(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Dec(),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.PostfixDecExpr(
                        node.Id("bar")
                    )
                )
            ])
        )

    def test_precedence_higher_than_exp(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Exp(),
            atom.Id("bar"),
            atom.Dec(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpExpr(
                    node.Id("foo"),
                    node.PostfixDecExpr(
                        node.Id("bar")
                    )
                )
            ])
        )

class PostIncrementTest(unittest.TestCase):
    def test_simple(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Inc(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PostfixIncExpr(
                    node.Id("foo")
                )
            ])
        )

    def test_precedence_higher_than_exist(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Inc(),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.PostfixIncExpr(
                        node.Id("bar")
                    )
                )
            ])
        )

    def test_precedence_higher_than_exp(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Exp(),
            atom.Id("bar"),
            atom.Inc(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpExpr(
                    node.Id("foo"),
                    node.PostfixIncExpr(
                        node.Id("bar")
                    )
                )
            ])
        )

class ExistParserText(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Exist(),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.ExistExpr(
                        node.Id("foo")
                    )
                )
            ])
        )

    def test_precedence_lower_than_dot_accessor(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Dot(),
            atom.Property("bar"),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.Accessor(
                        node.Id("foo"),
                        node.String("bar")
                    )
                )
            ])
        )

    def test_precedence_lower_than_at_accessor(self) -> None:
        atoms = [
            atom.At(),
            atom.Property("foo"),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExistExpr(
                    node.Accessor(
                        node.This(),
                        node.String("foo")
                    )
                )
            ])
        )

    def test_precedence_higher_than_exp(self) -> None:
        atoms = [
            atom.Id("foo"),
            atom.Exp(),
            atom.Id("bar"),
            atom.Exist(),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpExpr(
                    node.Id("foo"),
                    node.ExistExpr(
                        node.Id("bar"),
                    ),
                ),
            ])
        )

class ExpParserTest(unittest.TestCase):
    def test_associativity_right_to_left(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Exp(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ExpExpr(
                    node.Id("bar"),
                    node.ExpExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    @unittest.skip("TODO: add test cases for lower precedence")
    def test_precedence_lower_than(self) -> None:
        pass

    def test_unparen_unary_expression_not_allowed_on_the_left(self) -> None:
        for type, atom_ in _unary_operators_disallowed_before_left_exp.items():
            with self.subTest(type=type):
                atoms = [
                    atom_,
                    atom.Id("foo"),
                    atom.Exp(),
                    atom.Id("bar"),
                ]

                expected_message = r"^unparenthesized unary expression can't appear on the left-hand side of '\*\*'"

                with self.assertRaisesRegex(SyntaxException, expected_message):
                    parse(atoms)

    @unittest.skip("TODO")
    def test_unparen_unary_expression_not_allowed_on_the_left_decrement(self) -> None:
        atoms = [
            atom.Dec(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        expected_message = "unparenthesized unary expression can't appear on the left-hand side of '**'"

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    @unittest.skip("TODO")
    def test_unparen_unary_expression_not_allowed_on_the_left_inrement(self) -> None:
        atoms = [
            atom.Inc(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        expected_message = "unparenthesized unary expression can't appear on the left-hand side of '**'"

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    @unittest.skip("TODO")
    def test_unparen_unary_expression_not_allowed_on_the_left_minus(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        expected_message = "unparenthesized unary expression can't appear on the left-hand side of '**'"

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    @unittest.skip("TODO")
    def test_unparen_unary_expression_not_allowed_on_the_left_plus(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        expected_message = "unparenthesized unary expression can't appear on the left-hand side of '**'"

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    @unittest.skip("TODO")
    def test_unparen_unary_expression_not_allowed_on_the_left_type_of(self) -> None:
        atoms = [
            atom.TypeOf(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        expected_message = "unparenthesized unary expression can't appear on the left-hand side of '**'"

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    @unittest.skip("TODO")
    def test_unparen_unary_expression_not_allowed_on_the_left_do(self) -> None:
        atoms = [
            atom.Do(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        expected_message = "unparenthesized unary expression can't appear on the left-hand side of '**'"

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

    @unittest.skip("TODO")
    def test_unparen_unary_expression_not_allowed_on_the_left_delete(self) -> None:
        atoms = [
            atom.Delete(),
            atom.Id("baz"),
            atom.Exp(),
            atom.Id("foo"),
        ]

        expected_message = "unparenthesized unary expression can't appear on the left-hand side of '**'"

        with self.assertRaisesRegex(SyntaxException, expected_message):
            parse(atoms)

class BitNotParserTest(unittest.TestCase):
    def test_associativity_right_to_left(self) -> None:
        atoms = [
            atom.BitNot(),
            atom.BitNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitNotExpr(
                    node.BitNotExpr(
                        node.Id("foo"),
                    )
                ),
            ])
        )

class NotParserTest(unittest.TestCase):
    def test_associativity_right_to_left(self) -> None:
        atoms = [
            atom.Not(),
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.NotExpr(
                    node.NotExpr(
                        node.Id("foo"),
                    )
                ),
            ])
        )

class MulParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
            atom.Mul(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.MulExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_logical_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.Mul(),
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.Id("baz"),
                    node.NotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_lower_than_bit_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.Mul(),
            atom.BitNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.Id("baz"),
                    node.BitNotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.Mul(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.Id("bar"),
                    node.MulExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.Mul(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.Id("bar"),
                    node.MulExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_float_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.MulExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_integer_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.MulExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_rem(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
            atom.Rem(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.MulExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_mod(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
            atom.Mod(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.MulExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class FloatDivParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.FloatDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_logical_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.Id("baz"),
                    node.NotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_lower_than_bit_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.BitNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.Id("baz"),
                    node.BitNotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.Id("bar"),
                    node.FloatDivExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.Id("bar"),
                    node.FloatDivExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_mul(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
            atom.Mul(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.FloatDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_integer_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.FloatDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_rem(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
            atom.Rem(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.FloatDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_mod(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
            atom.Mod(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.FloatDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class IntegerDivParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.IntegerDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_logical_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.Id("baz"),
                    node.NotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_lower_than_bit_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.BitNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.Id("baz"),
                    node.BitNotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.Id("bar"),
                    node.IntegerDivExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.Id("bar"),
                    node.IntegerDivExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_mul(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
            atom.Mul(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.IntegerDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_float_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.IntegerDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_rem(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
            atom.Rem(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.IntegerDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_mod(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
            atom.Mod(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.IntegerDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class RemParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
            atom.Rem(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.RemExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_logical_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.Rem(),
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.Id("baz"),
                    node.NotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_lower_than_bit_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.Rem(),
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.Id("baz"),
                    node.NotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.Rem(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.Id("bar"),
                    node.RemExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.Rem(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.Id("bar"),
                    node.RemExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_mul(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
            atom.Mul(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.RemExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_float_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.RemExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_integer_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.RemExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_mod(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
            atom.Mod(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.RemExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class ModParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
            atom.Mod(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.ModExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_logical_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.Mod(),
            atom.Not(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.Id("baz"),
                    node.NotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_lower_than_bit_not(self) -> None:
        atoms = [
            atom.Id("baz"),
            atom.Mod(),
            atom.BitNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.Id("baz"),
                    node.BitNotExpr(
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.Mod(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.Id("bar"),
                    node.ModExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.Mod(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.Id("bar"),
                    node.ModExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_mul(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
            atom.Mul(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.ModExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_float_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
            atom.FloatDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.ModExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_integer_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
            atom.IntegerDiv(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.ModExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_rem(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
            atom.Rem(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.ModExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class MinusParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MinusExpr(
                    node.MinusExpr(
                        node.Id("foo")
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_mul(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("foo"),
            atom.Mul(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.MinusExpr(
                        node.Id("foo"),
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_float_div(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("foo"),
            atom.FloatDiv(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.MinusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_integer_div(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("foo"),
            atom.IntegerDiv(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.MinusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_rem(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("foo"),
            atom.Rem(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.MinusExpr(
                        node.Id("foo"),
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_mod(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("foo"),
            atom.Mod(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.MinusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_lower_than_exp(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Id("foo"),
            atom.Exp(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MinusExpr(
                    node.ExpExpr(
                        node.Id("foo"),
                        node.Id("bar")
                    ),
                ),
            ])
        )

    def test_precedence_equal_to_unary_plus(self) -> None:
        atoms = [
            atom.Sub(),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MinusExpr(
                    node.PlusExpr(
                        node.Id("foo")
                    )
                ),
            ])
        )

class PlusParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Add(),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PlusExpr(
                    node.PlusExpr(
                        node.Id("foo")
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_mul(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("foo"),
            atom.Mul(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.MulExpr(
                    node.PlusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_float_div(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("foo"),
            atom.FloatDiv(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.FloatDivExpr(
                    node.PlusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_integer_div(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("foo"),
            atom.IntegerDiv(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.IntegerDivExpr(
                    node.PlusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_rem(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("foo"),
            atom.Rem(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.RemExpr(
                    node.PlusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_higher_than_mod(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("foo"),
            atom.Mod(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ModExpr(
                    node.PlusExpr(
                        node.Id("foo")
                    ),
                    node.Id("bar")
                ),
            ])
        )

    def test_precedence_lower_than_exp(self) -> None:
        atoms = [
            atom.Add(),
            atom.Id("foo"),
            atom.Exp(),
            atom.Id("bar"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PlusExpr(
                    node.ExpExpr(
                        node.Id("foo"),
                        node.Id("bar")
                    ),
                ),
            ])
        )

    def test_precedence_equal_to_unary_minus(self) -> None:
        atoms = [
            atom.Add(),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.PlusExpr(
                    node.MinusExpr(
                        node.Id("foo")
                    )
                ),
            ])
        )

class AddParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.AddExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_mul(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.MulExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_float_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.FloatDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_integer_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.IntegerDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_rem(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.RemExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_mod(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.ModExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.Id("bar"),
                    node.AddExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.Id("bar"),
                    node.AddExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.Id("bar"),
                    node.AddExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.AddExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class SubParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.SubExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_mul(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mul(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.MulExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_float_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.FloatDiv(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.FloatDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_integer_div(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IntegerDiv(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.IntegerDivExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_rem(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Rem(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.RemExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_mod(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Mod(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SubExpr(
                    node.ModExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.Id("bar"),
                    node.SubExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.Id("bar"),
                    node.SubExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.Sub(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.Id("bar"),
                    node.SubExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.Add(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AddExpr(
                    node.SubExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class LeftShiftParserTestCase(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.LeftShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.AddExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.SubExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.Id("bar"),
                    node.LeftShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.Id("bar"),
                    node.LeftShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.Id("bar"),
                    node.LeftShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.LeftShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.LeftShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class ZeroFillRightShiftParserTestCase(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.ZeroFillRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.AddExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.SubExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.Id("bar"),
                    node.ZeroFillRightShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.Id("bar"),
                    node.ZeroFillRightShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.Id("bar"),
                    node.ZeroFillRightShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.ZeroFillRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.ZeroFillRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class SignRightShiftParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.SignRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_add(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Add(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.AddExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_sub(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Sub(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.SignRightShiftExpr(
                    node.SubExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.Id("bar"),
                    node.SignRightShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.Id("bar"),
                    node.SignRightShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_higher_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.SignRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.Id("bar"),
                    node.SignRightShiftExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

    def test_precedence_equal_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.LeftShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.LeftShiftExpr(
                    node.SignRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.ZeroFillRightShift(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.ZeroFillRightShiftExpr(
                    node.SignRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class InstanceOfParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.ZeroFillRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.SignRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.LeftShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.InstanceOfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.InstanceOfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.InstanceOfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.InstanceOfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.InstanceOfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.InstanceOfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_equal_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class InParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.ZeroFillRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.SignRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.LeftShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.InExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.InExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.InExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.InExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.InExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.InExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_equal_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class OfParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_zero_fill_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.ZeroFillRightShift(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.ZeroFillRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_sign_right_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.SignRightShift(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.SignRightShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_left_shift(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LeftShift(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OfExpr(
                    node.LeftShiftExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.OfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.OfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.OfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.OfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.OfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.Of(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.OfExpr(
                                node.Id("baz"),
                                node.Id("foo"),
                            )
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_equal_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.InstanceOf(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InstanceOfExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_equal_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.In(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.InExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

class LessThanParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.Id("baz")
                        ),
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_lower_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.CompExpr(
                        node.Id("baz"),
                        [
                            node.LessThanCmp(
                                node.Id("foo"),
                            ),
                        ]
                    )
                ),
            ])
        )

    def test_precedence_equal_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.Id("baz")
                        ),
                        node.LessThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.Id("baz")
                        ),
                        node.IsCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanCmp(
                            node.Id("baz")
                        ),
                        node.IsNotCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

class LessThanOrEqualToParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.LessThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_lower_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.CompExpr(
                        node.Id("baz"),
                        [
                            node.LessThanOrEqualToCmp(
                                node.Id("foo"),
                            ),
                        ]
                    )
                ),
            ])
        )

    def test_precedence_equal_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.IsCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.LessThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.IsNotCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

class GreaterThanParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_lower_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.GreaterThanCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.GreaterThanCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    [
                        node.GreaterThanCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.CompExpr(
                        node.Id("baz"),
                        [
                            node.GreaterThanCmp(
                                node.Id("foo"),
                            ),
                        ]
                    )
                ),
            ])
        )

    def test_precedence_equal_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.Id("baz")
                        ),
                        node.LessThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.Id("baz")
                        ),
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.Id("baz")
                        ),
                        node.IsCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanCmp(
                            node.Id("baz")
                        ),
                        node.IsNotCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

class GreaterThanOrEqualToParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_lower_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.CompExpr(
                        node.Id("baz"),
                        [
                            node.GreaterThanOrEqualToCmp(
                                node.Id("foo"),
                            ),
                        ]
                    )
                ),
            ])
        )

    def test_precedence_equal_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.LessThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.IsCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.GreaterThanOrEqualToCmp(
                            node.Id("baz")
                        ),
                        node.IsNotCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

class IsParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.Id("baz")
                        ),
                        node.IsCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_lower_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.IsCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.IsCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    [
                        node.IsCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.CompExpr(
                        node.Id("baz"),
                        [
                            node.IsCmp(
                                node.Id("foo"),
                            ),
                        ]
                    )
                ),
            ])
        )

    def test_precedence_equal_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.Id("baz")
                        ),
                        node.LessThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.Id("baz")
                        ),
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsCmp(
                            node.Id("baz")
                        ),
                        node.IsNotCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

class IsNotParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.Id("baz")
                        ),
                        node.IsNotCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_lower_than_in(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.In(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.IsNotCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Of(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.OfExpr(
                        node.Id("bar"),
                        node.Id("baz")
                    ),
                    [
                        node.IsNotCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_lower_than_instance_of(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.InstanceOf(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.InstanceOfExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    [
                        node.IsNotCmp(
                            node.Id("foo"),
                        ),
                    ]
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.IsNot(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.Id("bar"),
                    node.CompExpr(
                        node.Id("baz"),
                        [
                            node.IsNotCmp(
                                node.Id("foo"),
                            ),
                        ]
                    )
                ),
            ])
        )

    def test_precedence_equal_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.LessThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.Id("baz")
                        ),
                        node.LessThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.LessThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.Id("baz")
                        ),
                        node.LessThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.GreaterThan(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.Id("baz")
                        ),
                        node.GreaterThanOrEqualToCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

    def test_precedence_equal_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.Is(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.CompExpr(
                    node.Id("bar"),
                    [
                        node.IsNotCmp(
                            node.Id("baz")
                        ),
                        node.IsCmp(
                            node.Id("foo")
                        ),
                    ]
                )
            ])
        )

class BitwiseAndParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.BitAndExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_less_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThan(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.LessThanCmp(
                                node.Id("baz"),
                            )
                        ]
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_less_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.LessThanOrEqualTo(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.LessThanOrEqualToCmp(
                                node.Id("baz"),
                            )
                        ]
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_greater_than(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThan(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.GreaterThanCmp(
                                node.Id("baz"),
                            )
                        ]
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_greater_than_or_equal_to(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.GreaterThanOrEqualTo(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.GreaterThanOrEqualToCmp(
                                node.Id("baz"),
                            )
                        ]
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_is(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Is(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.IsCmp(
                                node.Id("baz"),
                            )
                        ]
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_is_not(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.IsNot(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitAndExpr(
                    node.CompExpr(
                        node.Id("bar"),
                        [
                            node.IsNotCmp(
                                node.Id("baz"),
                            )
                        ]
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_xor(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitXor(),
            atom.Id("baz"),
            atom.BitAnd(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitXorExpr(
                    node.Id("bar"),
                    node.BitAndExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

class BitwiseXorParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitXor(),
            atom.Id("baz"),
            atom.BitXor(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitXorExpr(
                    node.BitXorExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_bitwise_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitAnd(),
            atom.Id("baz"),
            atom.BitXor(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitXorExpr(
                    node.BitAndExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_bitwise_or(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitOr(),
            atom.Id("baz"),
            atom.BitXor(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitOrExpr(
                    node.Id("bar"),
                    node.BitXorExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

class BitwiseOrParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitOr(),
            atom.Id("baz"),
            atom.BitOr(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitOrExpr(
                    node.BitOrExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_bitwise_xor(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitXor(),
            atom.Id("baz"),
            atom.BitOr(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BitOrExpr(
                    node.BitXorExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_logical_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.And(),
            atom.Id("baz"),
            atom.BitOr(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AndExpr(
                    node.Id("bar"),
                    node.BitOrExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

class LogicalAndParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.And(),
            atom.Id("baz"),
            atom.And(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AndExpr(
                    node.AndExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_bitwise_or(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BitOr(),
            atom.Id("baz"),
            atom.And(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.AndExpr(
                    node.BitOrExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_logical_or(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Or(),
            atom.Id("baz"),
            atom.And(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OrExpr(
                    node.Id("bar"),
                    node.AndExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

class LogicalOrParserTest(unittest.TestCase):
    def test_associativity_left_to_right(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.Or(),
            atom.Id("baz"),
            atom.Or(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OrExpr(
                    node.OrExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_lower_than_logical_and(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.And(),
            atom.Id("baz"),
            atom.Or(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.OrExpr(
                    node.AndExpr(
                        node.Id("bar"),
                        node.Id("baz"),
                    ),
                    node.Id("foo"),
                ),
            ])
        )

    def test_precedence_higher_than_bin_exist(self) -> None:
        atoms = [
            atom.Id("bar"),
            atom.BinExist(),
            atom.Id("baz"),
            atom.Or(),
            atom.Id("foo"),
        ]

        self.assertEqual(
            parse(atoms),
            node.Block([
                node.BinExistExpr(
                    node.Id("bar"),
                    node.OrExpr(
                        node.Id("baz"),
                        node.Id("foo"),
                    ),
                ),
            ])
        )

class BinExistParserTest(unittest.TestCase):
    @unittest.skip("work in progress")
    def test_placeholder(self) -> None:
        pass

if __name__ == "__main__":
    unittest.main()
