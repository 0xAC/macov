from functools import reduce
from typing import Protocol, Sequence

class Position:
    def __init__(self, row, column):
        self._column = column
        self._row = row

    @property
    def column(self):
        return self._column

    @property
    def row(self):
        return self._row

    def __repr__(self):
        return "{},{}".format(self._row, self._column)

    def __eq__(self, other):
        return isinstance(other, Position) and self._column == other.column and self._row == other.row

EMPTY_POSITION = Position(0, 0)

class Range:
    def __init__(self, begin, end):
        self._begin = begin
        self._end = end

    @property
    def begin(self):
        return self._begin

    @property
    def end(self):
        return self._end

    def from_cursor(cursor):
        return Range(cursor.position, cursor.position)

    def __str__(self):
        return "{}:{}".format(self._begin, self._end)

    def __eq__(self, other):
        return isinstance(other, Range) and self._begin == other.begin and self._end == other.end

EMPTY_RANGE = Range(EMPTY_POSITION, EMPTY_POSITION)

class WithRange(Protocol):
    """
    Interface for objects with range property
    """

    @property
    def range(self) -> Range:
        pass

class WithType(Protocol):
    """
    Interface for objects with type property
    """

    @property
    def type(self) -> str:
        pass

class LexicalException(Exception):
    def __init__(self, message, position):
        Exception.__init__(self, "{} at {}".format(message, position))
        self._message = message
        self._position = position

    @property
    def message(self):
        return self._message

    @property
    def position(self):
        return self._position

class SyntaxException(Exception):
    def __init__(self, message, atom):
        Exception.__init__(self, "{} at {}".format(message, atom.range.begin))
        self._message = message
        self._atom = atom

    @property
    def message(self):
        return self._message

    @property
    def position(self):
        return self._atom.range.begin

def compose(*functions):
    def composed(arg):
        return reduce(lambda arg_, it: it(arg_), functions, arg)

    return composed

def dict_by_type(*items: Sequence[WithType]):
    return {it.type: it for it in items}

def atom_set(*atoms):
    return {it.type for it in atoms}
