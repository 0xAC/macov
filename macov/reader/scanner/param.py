from macov.core.atom import Atom, LeftParen, ThinArrow, ThickArrow, RightParen, CallStart, ParamStart, ParamEnd
from macov.reader.scanner.group import Group
from macov.util import trampoline
from pyrsistent import l
from typing import List, Tuple

@trampoline.wrap
def promote_parens_to_params(atoms: List[Atom], result: List[Atom]=l()) -> List[Atom]:
    if not atoms:
        return trampoline.result(result)

    head, tail = atoms.first, atoms.rest

    if head.type not in {ThickArrow.type, ThinArrow.type}:
        return trampoline.call(promote_parens_to_params)(tail, result.cons(head))

    new_atoms, new_result = _strip_call_start_and_upgrade_parens(tail, result.cons(head))
    return trampoline.call(promote_parens_to_params)(new_atoms, new_result)

def _start(atoms: List[Atom], result: List[Atom]) -> Tuple[List[Atom], List[Atom]]:
    if not atoms:
        return False, atoms, result

    head, tail = atoms.first, atoms.rest

    if head.type != RightParen.type:
        return False, atoms, result

    right_paren = head

    assert tail

    inside = tail.first

    assert inside.type == Group.type
    assert tail.rest

    left_paren = tail.rest.first

    assert left_paren.type == LeftParen.type

    new_result = result.cons(ParamEnd(right_paren.range)).cons(inside).cons(ParamStart(left_paren.range))

    return True, tail.rest.rest, new_result

def _strip_call_start_and_upgrade_parens(atoms: List[Atom], result: List[Atom]) -> Tuple[List[Atom], List[Atom]]:
    if not atoms:
        return atoms, result

    head, tail = atoms.first, atoms.rest

    # after right paren may be call start which need to be removed if the paren becomes param_end
    no_call_start_atoms = tail if isinstance(head, CallStart) else atoms
    found, new_atoms, new_result = _start(no_call_start_atoms, result)

    return (new_atoms, new_result) if found else (atoms, result)
