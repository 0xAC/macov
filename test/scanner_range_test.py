from macov.core import atom
from macov.core.util import Position, Range
from macov.reader import scan
from textwrap import dedent
import unittest

class ScannerRangeTest(unittest.TestCase):
    def test_id_range(self) -> None:
        code = dedent("""
            foo
                bar
                    baz
        """)

        self.assertEqual(
            tuple((it.value, it.range) for it in scan(code) if it.type == "id"),
            (
                ("foo", Range(Position(row=1, column=1), Position(row=1, column=4))),
                ("bar", Range(Position(row=2, column=5), Position(row=2, column=8))),
                ("baz", Range(Position(row=3, column=9), Position(row=3, column=12))),
            )
        )

    def test_colon_range(self) -> None:
        code = "a: b"

        self.assertEqual(
            tuple((it.value, it.range) for it in scan(code) if it.type == atom.Colon.type),
            (
                (":", Range(Position(row=1, column=2), Position(row=1, column=3))),
            )
        )

    def test_prototype_range(self) -> None:
        code = "a::b"

        self.assertEqual(
            tuple((it.value, it.range) for it in scan(code) if it.type == atom.Prototype.type),
            (
                ("::", Range(Position(row=1, column=2), Position(row=1, column=4))),
            )
        )

    def test_interpolated_expression(self) -> None:
        code = 'foo "#{ bar }"'

        self.assertEqual(
            tuple((it.value, it.range) for it in scan(code)),
            (
                ("foo", Range(Position(row=1, column=1), Position(row=1, column=4))),
                ("whitespace_call_start", Range(Position(row=1, column=4), Position(row=1, column=5))),
                ('"', Range(Position(row=1, column=5), Position(row=1, column=6))),
                ("bar", Range(Position(row=1, column=9), Position(row=1, column=12))),
                ('"', Range(Position(row=1, column=14), Position(row=1, column=15))),
                ("whitespace_call_end", Range(Position(row=1, column=15), Position(row=1, column=15))),
            )
        )

if __name__ == "__main__":
    unittest.main()
