from macov.core.util import *
from typing import Type
import abc

class Atom(abc.ABC):
    def __init__(self, value: str, range: Range=EMPTY_RANGE) -> None:
        self._value = value
        self._range = range

    @property
    def range(self):
        return self._range

    @property
    def value(self):
        return self._value

    def __eq__(self, other) -> bool:
        return isinstance(other, Atom) and self.type == other.type and self.value == other.value

    def __str__(self) -> str:
        return self._value

    def __unicode__(self):
        return self._value

class Value(Atom):
    def __init__(self, value: str, range: Range) -> None:
        Atom.__init__(self, value, range)

    def __eq__(self, other):
        return isinstance(other, Value) and super().__eq__(other)

class String(Value):
    type = "string"

    def __init__(self, content: str, delimiter='"', range: Range=EMPTY_RANGE) -> None:
        self._delimiter = delimiter
        self._content = content
        Value.__init__(self, content, range)

    @property
    def delimiter(self):
        return self._delimiter

    @property
    def content(self):
        return self._content

    @property
    def value(self):
        return "{0}{1}{0}".format(self._delimiter, self._content)

    def to_string(self) -> "String":
        return self

    def __eq__(self, other) -> bool:
        return isinstance(other, String) and self._content == other.content

    def __str__(self) -> str:
        return "string[{}]".format(self.value)

class Property(Atom):
    type = "property"

    def __init__(self, name, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, name, range)

    def __str__(self) -> str:
        return "property[{}]".format(self.value)

    def __eq__(self, other) -> bool:
        return isinstance(other, Property) and self.value == other.value

class Id(Atom):
    type = "id"

    def __init__(self, name, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, name, range)

    def to_string(self) -> String:
        return String(self.value, "", self._range)

    def to_property(self) -> Property:
        return Property(self.value, self.range)

    def __str__(self) -> str:
        return "id[{}]".format(self.value)

    def __eq__(self, other) -> bool:
        return isinstance(other, Id) and self.value == other.value

class Number(Value):
    type = "number"

    def __init__(self, value, range: Range=EMPTY_RANGE) -> None:
        Value.__init__(self, value, range)

    def __str__(self) -> str:
        return "number[{}]".format(self.value)

class Operator(Atom, abc.ABC):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, self.type, range)

    @property
    @abc.abstractmethod
    def type(self):
        pass

class StringDelimiter(Operator, abc.ABC):
    def __init__(self, char: str, count: int=1, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)
        self._char = char
        self._count = count

    @property
    def value(self) -> str:
        return self._char * self._count

    @property
    def char(self) -> str:
        return self._char

    @property
    def count(self) -> int:
        return self._count

string_start_type = "string_start"
string_end_type = "string_end"

class Concat(Operator):
    type = "concat"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

    @property
    def value(self):
        return ""

class Keyword(Atom, abc.ABC):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, self.type, range)

    @property
    @abc.abstractmethod
    def type(self):
        pass

    def to_id(self):
        return Id(self.value, self.range)

    def __str__(self) -> str:
        return self.type

class This(Keyword):
    type = "this"

    def __value__(self) -> str:
        return "this"

    def __str__(self) -> str:
        return "this"

class At(Keyword):
    type = "@"

    def __value__(self) -> str:
        return "@"

    def __str__(self) -> str:
        return "@"

class Super(Keyword):
    type = "super"

    def __value__(self) -> str:
        return "super"

    def __str__(self) -> str:
        return "super"

class Null(Value, Keyword):
    type = "null"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Value.__init__(self, "null", range)

    def __str__(self) -> str:
        return "null"

class Void(Value, Keyword):
    type = "void"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        super().__init__("undefined", range)

    def __str__(self) -> str:
        return "void"

class Bool(Value):
    pass

class True_(Bool, Keyword):
    type = "true"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        super().__init__("true", range)

    def __str__(self) -> str:
        return self.type

class False_(Bool, Keyword):
    type = "false"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Value.__init__(self, "false", range)

    def __str__(self) -> str:
        return self.type

class Infinity(Number, Keyword):
    type = "infinity"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        super().__init__("Infinity", range)

    def __str__(self) -> str:
        return "Infinity"

class Nan(Number, Keyword):
    type = "nan"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        super().__init__("NaN", range)

    def __str__(self) -> str:
        return "NaN"

class Async(Keyword):
    type = "async"

class Await(Keyword):
    type = "await"

class Break(Keyword):
    type = "break"

class Catch(Keyword):
    type = "catch"

class Class(Keyword):
    type = "class"

class Continue(Keyword):
    type = "continue"

class Delete(Keyword):
    type = "delete"

class Extends(Keyword):
    type = "extends"

class Finally(Keyword):
    type = "finally"

class For(Keyword):
    type = "for"

class If(Keyword):
    type = "if"

class Loop(Keyword):
    type = "loop"

class New(Keyword):
    type = "new"

class Switch(Keyword):
    type = "switch"

class Throw(Keyword):
    type = "throw"

class Try(Keyword):
    type = "try"

class TypeOf(Keyword, Operator):
    type = "typeof"

class Until(Keyword):
    type = "until"

class When(Keyword):
    type = "when"

class While(Keyword):
    type = "while"

class Yield(Keyword):
    type = "yield"

class PostIf(Atom):
    type = "post_if"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, "if", range)

    def __str__(self) -> str:
        return "post_if"

class PostUnless(Atom):
    type = "post_unless"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, "unless", range)

    def __str__(self) -> str:
        return "post_unless"

class Else(Keyword):
    type = "else"

class Unless(Keyword):
    type = "unless"

class Return(Keyword):
    type = "return"

class Compare(Operator):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class LessThan(Compare):
    type = "<"

class GreaterThan(Compare):
    type = ">"

class GreaterThanOrEqualTo(Compare):
    type = "greater_than_or_equal_to"

class LessThanOrEqualTo(Compare):
    type = "less_than_or_equal_to"

class Is(Compare, Keyword):
    type = "is"

class IsNot(Compare, Keyword):
    type = "isnt"

class InstanceOf(Compare, Keyword):
    type = "instanceof"

class In(Compare, Keyword):
    type = "in"

class Of(Compare, Keyword):
    type = "of"

class Splat(Operator):
    type = "splat"

class Span(Operator):
    type = "span"

class Delimiter(Atom, abc.ABC):
    pair: Type["Delimiter"] = NotImplemented

class OpeningDelimiter(Delimiter, abc.ABC):
    pair: Type["ClosingDelimiter"] = NotImplemented

class ClosingDelimiter(Delimiter, abc.ABC):
    pair: Type["OpeningDelimiter"] = NotImplemented

class StringStart(StringDelimiter, OpeningDelimiter):
    type = string_start_type
    pair = string_end_type

class StringEnd(StringDelimiter, ClosingDelimiter):
    type = string_end_type
    pair = string_start_type

class Regex(Value):
    type = "regex"

    def __init__(self, text: str, delimiter="/", range: Range=EMPTY_RANGE) -> None:
        Value.__init__(self, text, range)
        self._delimiter = delimiter

    @property
    def delimiter(self):
        return self._delimiter

    def __eq__(self, other):
        return isinstance(other, Regex) and other.delimiter == self._delimiter and super().__eq__(other)

    def __str__(self) -> str:
        return f"{self._delimiter}{self.value}{self._delimiter}"

_regex_start_type = "regex_start"
_regex_end_type = "regex_end"

class RegexStart(OpeningDelimiter):
    type = _regex_start_type
    pair = _regex_end_type

    def __init__(self, range=EMPTY_RANGE):
        super().__init__("///", range)

class RegexEnd(ClosingDelimiter):
    type = _regex_end_type
    pair = _regex_start_type

    def __init__(self, range=EMPTY_RANGE):
        super().__init__("///", range)

_left_bracket_type = "["
_right_bracket_type = "]"

class LeftBracket(Operator, OpeningDelimiter):
    type = _left_bracket_type
    pair = _right_bracket_type

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class RightBracket(Operator, ClosingDelimiter):
    type = _right_bracket_type
    pair = _left_bracket_type
    display_type = type

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

_left_brace_type = "{"
_right_brace_type = "}"

class LeftBrace(Operator, OpeningDelimiter):
    type = _left_brace_type
    pair = _right_brace_type
    display_type = type

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class RightBrace(Operator, ClosingDelimiter):
    type = _right_brace_type
    pair = _left_brace_type
    display_type = type

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

_left_paren_type = "("
_right_paren_type = ")"

class LeftParen(Operator, OpeningDelimiter):
    type = _left_paren_type
    pair = _right_paren_type
    display_type = type

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

    def __str__(self) -> str:
        return "left_paren"

class RightParen(Operator, ClosingDelimiter):
    type = _right_paren_type
    pair = _left_paren_type
    display_type = type

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

    def __str__(self) -> str:
        return "right_paren"

_param_start_type = "param_start"
_param_end_type = "param_end"

class ParamStart(Operator, OpeningDelimiter):
    type = _param_start_type
    pair = _param_end_type
    display_type = "[param_start (]"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class ParamEnd(Operator, ClosingDelimiter):
    type = _param_end_type
    pair = _param_start_type
    display_type = "[param_end )]"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

_index_start_type = "index_start"
_index_end_type = "index_end"
class IndexStart(Operator, OpeningDelimiter):
    type = _index_start_type
    pair = _index_end_type

class IndexEnd(Operator, ClosingDelimiter):
    type = _index_end_type
    pair = _index_start_type

class CallStart(Operator, abc.ABC):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class CallEnd(Operator, abc.ABC):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

_paren_call_start_type = "paren_call_start"
_paren_call_end_type = "paren_call_end"

class ParenCallStart(CallStart):
    kind = "paren"
    type = _paren_call_start_type
    pair = _paren_call_end_type
    display_type = "[call_start (]"

    def __str__(self) -> str:
        return "[call_start (]"

class ParenCallEnd(CallEnd):
    kind = "paren"
    type = _paren_call_end_type
    pair = _paren_call_start_type
    display_type = "[call_end )]"

    def __str__(self) -> str:
        return "[call_end )]"

_whitespace_call_start_type = "whitespace_call_start"
_whitespace_call_end_type = "whitespace_call_end"

class WhitespaceCallStart(CallStart):
    kind = "whitespace"
    type = _whitespace_call_start_type
    pair = _whitespace_call_end_type

    def __str__(self) -> str:
        return "call_start"

class ImplicitCallStart(CallStart):
    kind = "implicit"
    type = _whitespace_call_start_type
    pair = _whitespace_call_end_type

    def __str__(self) -> str:
        return "call_start"

class WhitespaceCallEnd(CallEnd):
    kind = "whitespace"
    type = _whitespace_call_end_type
    pair = _whitespace_call_start_type

    def __str__(self) -> str:
        return "call_end"

_indent_type = "indent"
_outdent_type = "outdent"

class Indent(OpeningDelimiter):
    type = _indent_type
    pair = _outdent_type
    display_type = "indent"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, "", range)

    def __str__(self) -> str:
        return "indent"

class Outdent(ClosingDelimiter):
    type = _outdent_type
    pair = _indent_type
    display_type = "outdent"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, "", range)

    def __str__(self) -> str:
        return "outdent"

class InlineIndent(Indent):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Indent.__init__(self, range)

    def __str__(self) -> str:
        return "%{}".format(super().__str__())

class InlineOutdent(Outdent):
    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Outdent.__init__(self, range)

    def __str__(self) -> str:
        return "%{}".format(super().__str__())

class ThinArrow(Operator):
    type = "thin_arrow"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class ThickArrow(Operator):
    type = "thick_arrow"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Comma(Operator):
    type = ","

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Dot(Operator):
    type = "."

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Prototype(Operator):
    type = "prototype"
    value = "::"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Add(Operator):
    type = "+"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Inc(Operator):
    type = "inc"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Sub(Operator):
    type = "-"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Dec(Operator):
    type = "dec"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Mul(Operator):
    type = "*"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Exp(Operator):
    type = "exp"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class FloatDiv(Operator):
    type = "/"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class IntegerDiv(Operator):
    type = "div"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Mod(Operator):
    type = "mod"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Rem(Operator):
    type = "%"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class And(Operator, Keyword):
    type = "and"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Or(Operator, Keyword):
    type = "or"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class BitAnd(Operator):
    type = "&"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class BitOr(Operator):
    type = "|"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class BitXor(Operator):
    type = "^"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Not(Operator, Keyword):
    type = "not"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class BitNot(Operator, Keyword):
    type = "~"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class LeftShift(Operator):
    type = "left_shift"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

    def __str__(self) -> str:
        return "<<"

class SignRightShift(Operator):
    type = "sign_right_shift"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

    def __str__(self) -> str:
        return ">>"

class ZeroFillRightShift(Operator):
    type = "zero_fill_right_shift"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

    def __str__(self) -> str:
        return ">>>"

class Colon(Operator):
    type = ":"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

class Exist(Operator):
    type = "?"

class DotExist(Operator):
    type = "dot_exist"

    def __str__(self) -> str:
        return "?."

class PrototypeExist(Operator):
    type = "prototype_exist"

    def __str__(self) -> str:
        return "?::"

class BinExist(Operator):
    type = "bin_exist"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)

    def __str__(self) -> str:
        return "?"

class Terminator(Atom):
    type = "terminator"

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Atom.__init__(self, "", range)

    def __str__(self) -> str:
        return "terminator"

class Epsilon(Atom):
    type = "empty"

    def __init__(self) -> None:
        Atom.__init__(self, "", EMPTY_RANGE)

    def __str__(self) -> str:
        return "\u03b5"

class ModAssign(Operator):
    type = "mod_assign"

    def __str__(self) -> str:
        return "%%="

class RemAssign(Operator):
    type = "rem_assign"

    def __str__(self) -> str:
        return "%="

class BitAndAssign(Operator):
    type = "bit_and_assign"

    def __str__(self) -> str:
        return "&="

class ExpAssign(Operator):
    type = "exp_assign"

    def __str__(self) -> str:
        return "**="

class MulAssign(Operator):
    type = "mul_assign"

    def __str__(self) -> str:
        return "*="

class AddAssign(Operator):
    type = "add_assign"

    def __str__(self) -> str:
        return "+="

class SubAssign(Operator):
    type = "sub_assign"

    def __str__(self) -> str:
        return "-="

class IntegerDivAssign(Operator):
    type = "integer_div_assign"

    def __str__(self) -> str:
        return "//="

class FloatDivAssign(Operator):
    type = "float_div_assign"

    def __str__(self) -> str:
        return "/="

class LeftShiftAssign(Operator):
    type = "left_shift_assign"

    def __str__(self) -> str:
        return "<<="

class SignRightShiftAssign(Operator):
    type = "sign_right_shift_assign"

    def __str__(self) -> str:
        return ">>="

class ZeroFillRightShiftAssign(Operator):
    type = "zero_fill_right_shift_assign"

    def __str__(self) -> str:
        return ">>>="

class ExistAssign(Operator):
    type = "exist_assign"

    def __str__(self) -> str:
        return "?="

class BitXorAssign(Operator):
    type = "bit_xor_assign"

    def __str__(self) -> str:
        return "^="

class BitOrAssign(Operator):
    type = "bit_or_assign"

    def __str__(self) -> str:
        return "|="

class Assign(Operator):
    type = "="

    def __init__(self, range: Range=EMPTY_RANGE) -> None:
        Operator.__init__(self, range)
