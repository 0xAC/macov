from macov.core import atom
from macov.core.util import Range
from macov.reader.scanner.util import ACCESSORS, Then
from macov.util import trampoline
from pyrsistent import l, plist

@trampoline.wrap
def add_inline_indent_outdent_for_then_else(input, output=l(), stack=l()):
    if not input:
        return trampoline.result(plist(_handle_end_of_input(output, stack), reverse=True))

    head, tail = input.first, input.rest

    if head.type == atom.Terminator.type and stack:
        return _handle_terminator(head, tail, output, stack)

    args = _handle_indents(head, *_handle_atom(head, *_handle_outdents(head, tail, output, stack)))

    return trampoline.call(add_inline_indent_outdent_for_then_else)(*args)

@trampoline.wrap
def replace_expressions_with_statements(atoms, result=l()):
    if not atoms:
        return trampoline.result(result)

    head, tail = atoms.first, atoms.rest

    if head.type == atom.Terminator.type:
        new_atoms, new_result = _replace_expressions_with_statements_in_line(tail, result.cons(head))

        return trampoline.call(replace_expressions_with_statements)(new_atoms, new_result)

    if not result:
        new_atoms, new_result = _replace_expressions_with_statements_in_line(atoms, result)

        return trampoline.call(replace_expressions_with_statements)(new_atoms, new_result)

    return trampoline.call(replace_expressions_with_statements)(tail, result.cons(head))

@trampoline.wrap
def _handle_outdents(head, tail, output, stack):
    if head.type != atom.Else.type or not stack:
        return trampoline.result((tail, output, stack))

    if stack.first.type == atom.Else.type:
        return trampoline.call(_handle_outdents)(head, tail, output.cons(atom.InlineOutdent(head.range)), stack.rest)

    return trampoline.result((tail, output.cons(atom.InlineOutdent(head.range)), stack.rest))

def _handle_indents(head, tail, output, stack):
    if head.type not in {Then.type, atom.Else.type}:
        return tail, output, stack

    if tail and tail.first.type == atom.Indent.type:
        return tail.rest, output.cons(tail.first), stack

    indent = atom.InlineIndent(head.range)

    return tail, output.cons(indent), stack.cons(head)

def _handle_atom(head, tail, output, stack):
    return tail, (output if head.type == Then.type else output.cons(head)), stack

def _handle_terminator(head, tail, output, stack):
    if not stack:
        return trampoline.call(add_inline_indent_outdent_for_then_else)(tail, output.cons(head), stack)

    return trampoline.call(add_inline_indent_outdent_for_then_else)(tail.cons(head), output.cons(atom.InlineOutdent(head.range)), stack.rest)

@trampoline.wrap
def _handle_end_of_input(output, stack):
    if not stack:
        return trampoline.result(output)

    position = output.first.range.end
    outdent = atom.InlineOutdent(Range(position, position))

    return trampoline.call(_handle_end_of_input)(output.cons(outdent), stack.rest)

_POST_VARIANTS = {
    "if": atom.PostIf,
    "unless": atom.PostUnless,
}

@trampoline.wrap
def _replace_expressions_with_statements_in_line(atoms, result):
    if not atoms:
        return trampoline.result((atoms, result))

    head, tail = atoms.first, atoms.rest

    if head.type in {atom.If.type, atom.Unless.type} and tail:
        final_tail = tail.rest if tail.first.type == atom.WhitespaceCallStart.type else tail

        return trampoline.result((final_tail, result.cons(_POST_VARIANTS[head.type](head.range))))

    if _is_post_terminator(head, result):
        return trampoline.result((atoms, result))

    return trampoline.call(_replace_expressions_with_statements_in_line)(tail, result.cons(head))

def _is_post_terminator(head, result):
    if head.type == atom.Indent.type:
        return True

    if head.type != atom.Terminator.type:
        return False

    if not result:
        return True

    return result.first.type not in ACCESSORS
