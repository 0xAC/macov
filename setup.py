from setuptools import find_packages, setup

setup(
    name="macov",
    version="0.0.1",
    description="CoffeeScript linter and transformer",
    python_requires=">= 3.6",
    packages=find_packages(exclude=["test"]),
    install_requires=[
        "ply",
        "click",
        "typing>=3.6",
        "typing_extensions>=3.6",
        "pyrsistent>=0.14",
    ],
    entry_points={
        "console_scripts": [
            "macov=macov.__main__:cli"
        ]
    }
)
