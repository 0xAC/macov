from macov.core import atom
from macov.core.util import LexicalException
from macov.reader import scan
from textwrap import dedent
import re
import unittest

_assign_operators = {
    "=": atom.Assign,
    "%%=": atom.ModAssign,
    "%=": atom.RemAssign,
    "&=": atom.BitAndAssign,
    "**=": atom.ExpAssign,
    "*=": atom.MulAssign,
    "+=": atom.AddAssign,
    "-=": atom.SubAssign,
    "//=": atom.IntegerDivAssign,
    "/=": atom.FloatDivAssign,
    "<<=": atom.LeftShiftAssign,
    ">>=": atom.SignRightShiftAssign,
    ">>>=": atom.ZeroFillRightShiftAssign,
    "?=": atom.ExistAssign,
    "^=": atom.BitXorAssign,
    "|=": atom.BitOrAssign,
}

_not_allowed_assign_operators = (
    "||=",
    "&&=",
)

def _for_each_not_allowed_operator(func):
    def wrapper(self):
        for operator in _not_allowed_assign_operators:
            with self.subTest(operator=operator):
                func(self, operator)

    return wrapper

def _for_each_assign_operator(func):
    def wrapper(self):
        for operator, atom_class in _assign_operators.items():
            with self.subTest(operator=operator):
                func(self, operator, atom_class)

    return wrapper

class AssignTest(unittest.TestCase):
    @_for_each_assign_operator
    def test_simple_assignment(self, operator, atom_class) -> None:
        code = f"foo {operator} bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom_class(),
                atom.Id("bar"),
            ]
        )

    @_for_each_not_allowed_operator
    def test_not_allowed_operator(self, operator) -> None:
        code = f"foo {operator} bar"

        expected_message = f"^operator '{re.escape(operator)}' is not allowed at \d+,\d+$"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_simple_assignment_invocation_simple(self) -> None:
        code = "foo = bar()"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.Id("bar"),
                atom.ParenCallStart(),
                atom.ParenCallEnd(),
            ]
        )

    def test_simple_assignment_invocation_double_simple(self) -> None:
        code = "foo = bar()()"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.Id("bar"),
                atom.ParenCallStart(),
                atom.ParenCallEnd(),
                atom.ParenCallStart(),
                atom.ParenCallEnd(),
            ]
        )

    def test_simple_assignment_invocation_whitespace(self) -> None:
        code = "foo = bar baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_simple_assignment_invocation_whitespace_two_params(self) -> None:
        code = "foo = bar baz, boo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.Comma(),
                atom.Id("boo"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_simple_assignment_invocation_double_whitespace(self) -> None:
        code = "foo = bar baz qux"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallStart(),
                atom.Id("qux"),
                atom.WhitespaceCallEnd(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_simple_assignment_grouped_invocation(self) -> None:
        code = "foo = (bar baz) qux"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.LeftParen(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.RightParen(),
                atom.WhitespaceCallStart(),
                atom.Id("qux"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_simple_assignment_two_grouped_invocations(self) -> None:
        code = "foo = (bar baz) (qux boo)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.LeftParen(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.RightParen(),
                atom.WhitespaceCallStart(),
                atom.LeftParen(),
                atom.Id("qux"),
                atom.WhitespaceCallStart(),
                atom.Id("boo"),
                atom.WhitespaceCallEnd(),
                atom.RightParen(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_simple_assignment_two_invocations_one_grouped_one_whitespace(self) -> None:
        code = "foo = (bar baz) qux boo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.LeftParen(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.RightParen(),
                atom.WhitespaceCallStart(),
                atom.Id("qux"),
                atom.WhitespaceCallStart(),
                atom.Id("boo"),
                atom.WhitespaceCallEnd(),
                atom.WhitespaceCallEnd(),
            ]
        )

class ScannerTest(unittest.TestCase):
    def test_empty(self) -> None:
        code = ""

        self.assertSequenceEqual(
            scan(code),
            [
            ]
        )

    def test_comment(self) -> None:
        code = "foo# ignore me"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo")
            ]
        )

    def test_block_comment_inline(self) -> None:
        code = "### foo ### bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar")
            ]
        )

    def test_block_comment_multiline(self) -> None:
        code = dedent("""
            ###
            foo
            ### bar
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar")
            ]
        )

    def test_comment_does_not_interfere_with_indentation(self) -> None:
        code = dedent("""
            foo # some
                # comment
            bar
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Terminator(),
                atom.Id("bar"),
            ]
        )

    def test_ignore_indent_followed_by_a_comment(self) -> None:
        code = dedent("""
            foo(
                    # comment
            )
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.ParenCallEnd(),
            ]
        )

    def test_double_indent(self) -> None:
        code = dedent("""
            foo
                    bar
        """)

        with self.assertRaisesRegex(LexicalException, "^double indent"):
            scan(code)

    def test_block_string_double_quote(self) -> None:
        code = dedent('''
            """
            some text
            """
        ''')

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String("\nsome text\n"),
            ]
        )

    def test_block_string_single_quote(self) -> None:
        code = dedent("""
            '''
            some text
            '''
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String("\nsome text\n"),
            ]
        )

    def test_string_empty_double_quote(self) -> None:
        code = r'""'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(""),
            ]
        )

    def test_string_empty_single_quote(self) -> None:
        code = r"''"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(""),
            ]
        )

    def test_string_empty_block_double_quote(self) -> None:
        code = r"''''''"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(""),
            ]
        )

    def test_string_empty_block_single_quote(self) -> None:
        code = r'""""""'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(""),
            ]
        )

    def test_string_escape_double_quote(self) -> None:
        code = r'"foo\"bar"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r'foo\"bar'),
            ]
        )

    def test_string_interpolation_empty(self) -> None:
        code = r'"#{ }"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.StringEnd('"'),
            ]
        )

    def test_string_interpolation_double_concat_double_quote(self) -> None:
        code = r'"foo #{ "bar" } baz"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.String("foo "),
                atom.Concat(),
                atom.String("bar"),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd('"'),
            ]
        )

    def test_string_interpolation_nested_double_quote(self) -> None:
        code = r'"foo #{ "bar #{ baz } qux" } goo"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.String("foo "),
                atom.Concat(),
                atom.StringStart('"'),
                atom.String("bar "),
                atom.Concat(),
                atom.Id("baz"),
                atom.Concat(),
                atom.String(" qux"),
                atom.StringEnd('"'),
                atom.Concat(),
                atom.String(" goo"),
                atom.StringEnd('"'),
            ]
        )

    def test_string_interpolation_before_concat_double_quote(self) -> None:
        code = r'"foo #{ "bar" }"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.String("foo "),
                atom.Concat(),
                atom.String("bar"),
                atom.StringEnd('"'),
            ]
        )

    def test_string_interpolation_after_concat_double_quote(self) -> None:
        code = r'"#{ "bar" } baz"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.String("bar"),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd('"'),
            ]
        )

    def test_string_interpolation_double_concat_single_quote(self) -> None:
        code = r"'foo #{ 'bar' } baz'"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart("'"),
                atom.String("foo "),
                atom.Concat(),
                atom.String("bar"),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd("'"),
            ]
        )

    def test_string_interpolation_before_concat_single_quote(self) -> None:
        code = r"'foo #{ 'bar' }'"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart("'"),
                atom.String("foo "),
                atom.Concat(),
                atom.String("bar"),
                atom.StringEnd("'"),
            ]
        )

    def test_string_interpolation_after_concat_single_quote(self) -> None:
        code = r"'#{ 'bar' } baz'"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart("'"),
                atom.String("bar"),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd("'"),
            ]
        )

    def test_string_interpolation_double_quote_block(self) -> None:
        code = r'"""foo #{ """bar""" } baz"""'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"', 3),
                atom.String("foo "),
                atom.Concat(),
                atom.String("bar"),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd('"', 3),
            ]
        )

    def test_string_interpolation_single_quote_block(self) -> None:
        code = r"'''foo #{ '''bar''' } baz'''"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart("'", 3),
                atom.String("foo "),
                atom.Concat(),
                atom.String("bar"),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd("'", 3),
            ]
        )

    def test_string_fake_end_of_interpolation_double_quote(self) -> None:
        code = r'"foo #{ {} } baz"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.String("foo "),
                atom.Concat(),
                atom.LeftBrace(),
                atom.RightBrace(),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd('"'),
            ]
        )

    def test_string_fake_end_of_interpolation_2_double_quote(self) -> None:
        code = r'"foo #{ "}" } baz"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.String("foo "),
                atom.Concat(),
                atom.String("}"),
                atom.Concat(),
                atom.String(" baz"),
                atom.StringEnd('"'),
            ]
        )

    def test_string_fake_interpolation_double_quote(self) -> None:
        code = r'"foo \#{ bar" baz'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r'foo \#{ bar'),
                atom.Id("baz"),
            ]
        )

    def test_string_fake_interpolation_2_double_quote(self) -> None:
        code = r'"foo #\{ bar" baz'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r'foo #\{ bar'),
                atom.Id("baz"),
            ]
        )

    def test_string_fake_interpolation_single_quote(self) -> None:
        code = r"'foo \#{ bar' baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r"foo \#{ bar"),
                atom.Id("baz"),
            ]
        )

    def test_string_fake_interpolation_2_single_quote(self) -> None:
        code = r"'foo #\{ bar' baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r"foo #\{ bar"),
                atom.Id("baz"),
            ]
        )

    def test_string_interpolation_does_not_interfere_with_outside_call(self) -> None:
        code = 'baz "#{ qux.goo }"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("baz"),
                atom.WhitespaceCallStart(),
                atom.StringStart('"'),
                atom.Id("qux"),
                atom.Dot(),
                atom.Property("goo"),
                atom.StringEnd('"'),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_string_interpolation_does_not_interfere_between_each_other(self) -> None:
        code = '"#{ qux goo }#{ foo bar }"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.StringStart('"'),
                atom.Id("qux"),
                atom.WhitespaceCallStart(),
                atom.Id("goo"),
                atom.WhitespaceCallEnd(),
                atom.Concat(),
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
                atom.StringEnd('"'),
            ]
        )

    def test_string_fake_escape_double_quote(self) -> None:
        code = r'"foo\\"bar'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r"foo\\"),
                atom.Id("bar"),
            ]
        )

    def test_string_escape_single_quote(self) -> None:
        code = r"'foo\'bar'"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r"foo\'bar"),
            ]
        )

    def test_string_escape_double_quote_block(self) -> None:
        code = r'"""foo\"""bar"""'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r'foo\"""bar')
            ]
        )

    def test_string_fake_escape_single_quote(self) -> None:
        code = r"'foo\\'bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r"foo\\"),
                atom.Id("bar"),
            ]
        )

    def test_string_unicode_escape_double_quote(self) -> None:
        code = r'"\u00ab"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.String(r"\u00ab"),
            ]
        )

    def test_simplest(self) -> None:
        code = dedent("""
            define [
            ], (
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_optional_temrminator(self) -> None:
        code = dedent("""
            define [
            ],
            (
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_single(self) -> None:
        code = dedent("""
            define [
                "lodash"
            ], (
                $_
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("lodash"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.Id("$_"),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_single_one_line(self) -> None:
        code = """define ["lodash"], ($_) ->"""

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.String("lodash"),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Id("$_"),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_single_line_comma_separated(self) -> None:
        code = """define ["lodash", "webgis/Core/ValidationUtils"], ($_, ValidationUtils) ->"""

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.String("lodash"),
                atom.Comma(),
                atom.String("webgis/Core/ValidationUtils"),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Id("$_"),
                atom.Comma(),
                atom.Id("ValidationUtils"),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_single_quote(self) -> None:
        code = dedent("""
            define [
                'lodash'
            ], (
                $_
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("lodash"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.Id("$_"),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_double(self) -> None:
        code = dedent("""
            define [
                "foo"
                "bar"
            ], (
                foo
                bar
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("foo"),
                atom.Terminator(),
                atom.String("bar"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.Id("foo"),
                atom.Terminator(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_double_pairs(self) -> None:
        code = dedent("""
            define [
                "foo", "bar"
                "baz", "qux"
            ], (
                foo, bar
                baz, qux
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("foo"),
                atom.Comma(),
                atom.String("bar"),
                atom.Terminator(),
                atom.String("baz"),
                atom.Comma(),
                atom.String("qux"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.Id("foo"),
                atom.Comma(),
                atom.Id("bar"),
                atom.Terminator(),
                atom.Id("baz"),
                atom.Comma(),
                atom.Id("qux"),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_at_property(self) -> None:
        code = "@foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.Property("foo"),
            ]
        )

    def test_at_with_dot_property(self) -> None:
        code = "@.foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.Dot(),
                atom.Property("foo"),
            ]
        )

    def test_at_expression(self) -> None:
        code = "foo @bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.At(),
                atom.Property("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_this_expression(self) -> None:
        code = "foo this.bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.This(),
                atom.Dot(),
                atom.Property("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_unpack_object_single_property(self) -> None:
        code = dedent("""
            define [
                "foo"
            ], (
                { foo }
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("foo"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.RightBrace(),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_unpack_two_objects_with_single_property(self) -> None:
        code = dedent("""
            define [
                "foo"
                "bar"
            ], (
                { foo }
                { bar }
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("foo"),
                atom.Terminator(),
                atom.String("bar"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.RightBrace(),
                atom.Terminator(),
                atom.LeftBrace(),
                atom.Id("bar"),
                atom.RightBrace(),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_unpack_object_with_two_comma_separated_properties(self) -> None:
        code = dedent("""
            define [
                "foo"
            ], (
                { foo, bar }
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("foo"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.Comma(),
                atom.Id("bar"),
                atom.RightBrace(),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_unpack_object_with_two_newline_separated_properties(self) -> None:
        code = dedent("""
            define [
                "foo"
            ], (
                {
                    foo
                    bar
                }
            ) ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("define"),
                atom.WhitespaceCallStart(),
                atom.LeftBracket(),
                atom.Indent(),
                atom.String("foo"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Comma(),
                atom.ParamStart(),
                atom.Indent(),
                atom.LeftBrace(),
                atom.Indent(),
                atom.Id("foo"),
                atom.Terminator(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.RightBrace(),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_nested_param_start(self) -> None:
        code = "(foo = () ->) ->"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.ParamStart(),
                atom.Id("foo"),
                atom.Assign(),
                atom.ParamStart(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.ParamEnd(),
                atom.ThinArrow(),
            ]
        )

    def test_nan(self) -> None:
        code = "foo NaN"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Nan(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_nan_is_not_callable(self) -> None:
        code = "NaN foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Nan(),
                atom.Id("foo"),
            ]
        )

    def test_infinity(self) -> None:
        code = "foo Infinity"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Infinity(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_infinity_is_not_callable(self) -> None:
        code = "Infinity foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Infinity(),
                atom.Id("foo"),
            ]
        )

    def test_try_operator(self) -> None:
        code = "try Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Try(),
                atom.Id("Foo"),
            ]
        )

    def test_catch_operator(self) -> None:
        code = "catch Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Catch(),
                atom.Id("Foo"),
            ]
        )

    def test_finally_operator(self) -> None:
        code = "finally Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Finally(),
                atom.Id("Foo"),
            ]
        )

    def test_break_operator(self) -> None:
        code = "break Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Break(),
                atom.Id("Foo"),
            ]
        )

    def test_continue_operator(self) -> None:
        code = "continue Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Continue(),
                atom.Id("Foo"),
            ]
        )

    def test_throw_operator(self) -> None:
        code = "throw Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Throw(),
                atom.Id("Foo"),
            ]
        )

    def test_type_of_operator(self) -> None:
        code = "typeof Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.TypeOf(),
                atom.Id("Foo"),
            ]
        )

    def test_type_of_is_an_expression(self) -> None:
        code = "foo typeof bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.TypeOf(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd()
            ]
        )

    def test_delete_operator(self) -> None:
        code = "delete Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Delete(),
                atom.Id("Foo"),
            ]
        )

    def test_new_operator(self) -> None:
        code = "new Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.New(),
                atom.Id("Foo"),
            ]
        )

    def test_for_keyword(self) -> None:
        code = "bar for baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.For(),
                atom.Id("baz"),
            ]
        )

    def test_while_keyword(self) -> None:
        code = "bar while baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.While(),
                atom.Id("baz"),
            ]
        )

    def test_loop_keyword(self) -> None:
        code = "bar loop baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Loop(),
                atom.Id("baz"),
            ]
        )

    def test_until_keyword(self) -> None:
        code = "bar until baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Until(),
                atom.Id("baz"),
            ]
        )

    def test_async_operator(self) -> None:
        code = "async Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Async(),
                atom.Id("Foo"),
            ]
        )

    def test_await_operator(self) -> None:
        code = "await Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Await(),
                atom.Id("Foo"),
            ]
        )

    def test_yield_operator(self) -> None:
        code = "yield Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Yield(),
                atom.Id("Foo"),
            ]
        )

    def test_when_keyword(self) -> None:
        code = "when foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.When(),
                atom.Id("foo"),
            ]
        )

    def test_switch_keyword(self) -> None:
        code = "switch foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Switch(),
                atom.Id("foo"),
            ]
        )

    def test_switch_is_an_expression(self) -> None:
        code = "foo switch"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Switch(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_extends(self) -> None:
        code = "class Foo extends Bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Class(),
                atom.Id("Foo"),
                atom.Extends(),
                atom.Id("Bar"),
            ]
        )

    def test_class_delcaration(self) -> None:
        code = "class Foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Class(),
                atom.Id("Foo"),
            ]
        )

    def test_class_delcaration_with_constructor(self) -> None:
        code = dedent("""
            class Foo
                constructor: () ->
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Class(),
                atom.Id("Foo"),
                atom.Indent(),
                atom.Id("constructor"),
                atom.Colon(),
                atom.ParamStart(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.Outdent(),
            ]
        )

    def test_array_with_call_inside(self) -> None:
        code = "[foo bar]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBracket(),
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
                atom.RightBracket(),
            ]
        )

    def test_object_empty(self) -> None:
        code = "{}"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.RightBrace(),
            ]
        )

    def test_object_with_short_property(self) -> None:
        code = "{ foo }"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.RightBrace(),
            ]
        )

    def test_object_with_regular_property(self) -> None:
        code = "{ foo: foo }"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.Colon(),
                atom.Id("foo"),
                atom.RightBrace(),
            ]
        )

    def test_object_with_string_key(self) -> None:
        code = '{ "foo": foo }'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.String("foo"),
                atom.Colon(),
                atom.Id("foo"),
                atom.RightBrace(),
            ]
        )

    def test_object_double_outdent(self) -> None:
        code = dedent("""
            {
                foo:
                    bar: baz
            }
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.Indent(),
                atom.Id("foo"),
                atom.Colon(),
                atom.Indent(),
                atom.Id("bar"),
                atom.Colon(),
                atom.Id("baz"),
                atom.Outdent(),
                atom.Outdent(),
                atom.RightBrace(),
            ]
        )

    def test_object_with_call_inside(self) -> None:
        code = "{ foo: bar baz }"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.Colon(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.RightBrace(),
            ]
        )

    def test_invocation_mixed_parenthesed_and_whitespaced(self) -> None:
        code = "foo bar() baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.ParenCallStart(),
                atom.ParenCallEnd(),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_id_starting_with_underscore(self) -> None:
        code = "_foo"

        self.assertSequenceEqual(scan(code), [atom.Id("_foo")])

    def test_id_followed_by_a_comment(self) -> None:
        code = "foo  # comment"

        self.assertSequenceEqual(scan(code), [atom.Id("foo")])

    def test_id_which_is_at_alone(self) -> None:
        code = "@"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At()
            ]
        )

    def test_mixed_invocation(self) -> None:
        code = dedent("""
            foo ->
                bar baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.ThinArrow(),
                atom.Indent(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.Outdent(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_binary_expression_add(self) -> None:
        code = "bar + baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Add(),
                atom.Id("baz"),
            ]
        )

    def test_unary_expression_add(self) -> None:
        code = "bar +baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Add(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_unary_postfix_expression_inc(self) -> None:
        code = "bar++"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Inc(),
            ]
        )

    def test_unary_expression_sub(self) -> None:
        code = "bar -baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Sub(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd()
            ]
        )

    def test_unary_expression_dec(self) -> None:
        code = "bar--"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Dec(),
            ]
        )

    def test_binary_expression_mul(self) -> None:
        code = "bar * baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Mul(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_exp(self) -> None:
        code = "bar ** baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Exp(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_float_div(self) -> None:
        code = "bar / baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.FloatDiv(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_float_div_twice(self) -> None:
        code = "foo / bar / baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.FloatDiv(),
                atom.Id("bar"),
                atom.FloatDiv(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_integral_div(self) -> None:
        code = "bar // baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.IntegerDiv(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_rem(self) -> None:
        code = "bar % baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Rem(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_bitwise_and(self) -> None:
        code = "bar & baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.BitAnd(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_bitwise_and_no_space(self) -> None:
        code = "bar&baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.BitAnd(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_js_and(self) -> None:
        code = "&&"

        expected_message = "operator '&&' is not allowed use 'and` instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_binary_expression_bitwise_or(self) -> None:
        code = "bar | baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.BitOr(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_bitwise_or_no_space(self) -> None:
        code = "bar|baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.BitOr(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_xor(self) -> None:
        code = "bar ^ baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.BitXor(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_xor_no_space(self) -> None:
        code = "bar^baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.BitXor(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_js_or(self) -> None:
        code = "||"

        expected_message = "operator '||' is not allowed use 'or` instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_binary_expression_mod(self) -> None:
        code = "bar %% baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Mod(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_or(self) -> None:
        code = "bar or baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Or(),
                atom.Id("baz"),
            ]
        )

    def test_binary_expression_and(self) -> None:
        code = "bar and baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.And(),
                atom.Id("baz"),
            ]
        )

    def test_unary_expression_not(self) -> None:
        code = "not foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Not(),
                atom.Id("foo"),
            ]
        )

    def test_unary_expression_js_not(self) -> None:
        code = "!"

        expected_message = "operator '!' is not allowed use 'not` instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_unary_expression_bitwise_not(self) -> None:
        code = "foo ~ bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.BitNot(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_unary_expression_bitwise_not_no_space(self) -> None:
        code = "foo ~bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.BitNot(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_binary_left_shift(self) -> None:
        code = "bar << baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.LeftShift(),
                atom.Id("baz"),
            ]
        )

    def test_binary_left_shift_no_space(self) -> None:
        code = "bar<<baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.LeftShift(),
                atom.Id("baz"),
            ]
        )

    def test_binary_sign_right_shift(self) -> None:
        code = "bar >> baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.SignRightShift(),
                atom.Id("baz"),
            ]
        )

    def test_binary_sign_right_shift_no_space(self) -> None:
        code = "bar>>baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.SignRightShift(),
                atom.Id("baz"),
            ]
        )

    def test_binary_zero_fill_right_shift(self) -> None:
        code = "bar >>> baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.ZeroFillRightShift(),
                atom.Id("baz"),
            ]
        )

    def test_binary_zero_fill_right_shift_no_space(self) -> None:
        code = "bar>>>baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.ZeroFillRightShift(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_equal_to(self) -> None:
        code = "=="

        expected_message = "operator '==' is not allowed use 'is` instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_compare_expression_not_equal_to(self) -> None:
        code = "!="

        expected_message = "operator '!=' is not allowed use 'isnt` instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_compare_expression_less_than(self) -> None:
        code = "bar < baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.LessThan(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_less_than_no_space(self) -> None:
        code = "bar<baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.LessThan(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_greater_than(self) -> None:
        code = "bar > baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.GreaterThan(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_greater_than_no_space(self) -> None:
        code = "bar>baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.GreaterThan(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_greater_than_or_equal_to(self) -> None:
        code = "bar >= baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.GreaterThanOrEqualTo(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_greater_than_or_equal_to_no_space(self) -> None:
        code = "bar>=baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.GreaterThanOrEqualTo(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_less_than_or_equal_to(self) -> None:
        code = "bar <= baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.LessThanOrEqualTo(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_less_than_or_equal_to_no_space(self) -> None:
        code = "bar<=baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.LessThanOrEqualTo(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_is(self) -> None:
        code = "bar is baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Is(),
                atom.Id("baz"),
            ]
        )

    def test_relation_expression_in(self) -> None:
        code = "bar in baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.In(),
                atom.Id("baz"),
            ]
        )

    def test_relation_expression_not_in(self) -> None:
        code = "bar not in baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Not(),
                atom.In(),
                atom.Id("baz"),
            ]
        )

    def test_relation_expression_instance_of(self) -> None:
        code = "bar instanceof baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.InstanceOf(),
                atom.Id("baz"),
            ]
        )

    def test_relation_expression_not_instance_of(self) -> None:
        code = "bar not instanceof baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Not(),
                atom.InstanceOf(),
                atom.Id("baz"),
            ]
        )

    def test_relation_expression_of(self) -> None:
        code = "bar of baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Of(),
                atom.Id("baz"),
            ]
        )

    def test_relation_expression_not_of(self) -> None:
        code = "bar not of baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Not(),
                atom.Of(),
                atom.Id("baz"),
            ]
        )

    def test_compare_expression_is_not(self) -> None:
        code = "bar isnt baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.IsNot(),
                atom.Id("baz"),
            ]
        )

    def test_unary_exist(self) -> None:
        code = "bar?"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.Exist(),
            ]
        )

    def test_exist_dot_property(self) -> None:
        code = "foo?.bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.DotExist(),
                atom.Property("bar"),
            ]
        )

    def test_exist_add_implicit(self) -> None:
        code = "foo?+bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Exist(),
                atom.WhitespaceCallStart(),
                atom.Add(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_exist_sub_implicit(self) -> None:
        code = "foo?-bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Exist(),
                atom.WhitespaceCallStart(),
                atom.Sub(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_prototype(self) -> None:
        code = "foo::"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Prototype(),
            ]
        )

    def test_at_prototype(self) -> None:
        code = "@::"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.Prototype(),
            ]
        )

    def test_prototype_id(self) -> None:
        code = "foo::bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Prototype(),
                atom.Property("bar"),
            ]
        )

    def test_at_prototype_id(self) -> None:
        code = "@::foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.Prototype(),
                atom.Property("foo"),
            ]
        )

    def test_exist_prototype_property(self) -> None:
        code = "foo?::bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.PrototypeExist(),
                atom.Property("bar"),
            ]
        )

    def test_exist_binary_operator(self) -> None:
        code = "foo? + bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Exist(),
                atom.Add(),
                atom.Id("bar"),
            ]
        )

    def test_exist_and_colon_are_separate(self) -> None:
        code = "foo?:bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Exist(),
                atom.Colon(),
                atom.Id("bar"),
            ]
        )

    def test_binary_exist(self) -> None:
        code = "bar ? foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("bar"),
                atom.BinExist(),
                atom.Id("foo"),
            ]
        )

    def test_binary_splat(self) -> None:
        code = "foo ... bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Splat(),
                atom.Id("bar"),
            ]
        )

    def test_binary_splat_no_space(self) -> None:
        code = "foo...bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Splat(),
                atom.Id("bar"),
            ]
        )

    def test_binary_span(self) -> None:
        code = "foo .. bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Span(),
                atom.Id("bar"),
            ]
        )

    def test_binary_span_no_space(self) -> None:
        code = "foo..bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Span(),
                atom.Id("bar"),
            ]
        )

    def test_binary_in_range_no_space(self) -> None:
        code = "foo...bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Splat(),
                atom.Id("bar"),
            ]
        )

    def test_call_start_new_line_dot(self) -> None:
        code = dedent("""
            foo bar
            .baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
                atom.Dot(),
                atom.Property("baz"),
            ]
        )

    def test_call_start_same_line_dot(self) -> None:
        code = dedent("""
            foo bar .baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.Dot(),
                atom.Property("baz"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_call_start_function_two_levels(self) -> None:
        code = dedent("""
            foo ->
                bar ->
                    baz qux
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.ThinArrow(),
                atom.Indent(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.ThinArrow(),
                atom.Indent(),
                atom.Id("baz"),
                atom.WhitespaceCallStart(),
                atom.Id("qux"),
                atom.WhitespaceCallEnd(),
                atom.Outdent(),
                atom.WhitespaceCallEnd(),
                atom.Outdent(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_call_start_function_new_line_dot(self) -> None:
        code = dedent("""
            foo ->
                bar baz
            .qux
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.ThinArrow(),
                atom.Indent(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.Outdent(),
                atom.WhitespaceCallEnd(),
                atom.Dot(),
                atom.Property("qux"),
            ]
        )

    def test_call_start_multiline_object(self) -> None:
        code = dedent("""
            foo {
                bar: 0
            }
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.LeftBrace(),
                atom.Indent(),
                atom.Id("bar"),
                atom.Colon(),
                atom.Number("0"),
                atom.Outdent(),
                atom.RightBrace(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_call_multiline_with_function_parameter(self) -> None:
        code = dedent("""
            foo(
                (bar) ->
            )
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Indent(),
                atom.ParamStart(),
                atom.Id("bar"),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.Outdent(),
                atom.ParenCallEnd(),
            ]
        )

    def test_call_multiline_with_parentesed_invocation(self) -> None:
        code = dedent("""
            foo(
                (bar())
            )
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Indent(),
                atom.LeftParen(),
                atom.Id("bar"),
                atom.ParenCallStart(),
                atom.ParenCallEnd(),
                atom.RightParen(),
                atom.Outdent(),
                atom.ParenCallEnd(),
            ]
        )

    def test_call_with_regex(self) -> None:
        code = "foo /bar/"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Regex("bar", delimiter="/"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_call_multiline_with_non_parentesed_invocation(self) -> None:
        code = dedent("""
            foo(
                (bar baz)
            )
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Indent(),
                atom.LeftParen(),
                atom.Id("bar"),
                atom.WhitespaceCallStart(),
                atom.Id("baz"),
                atom.WhitespaceCallEnd(),
                atom.RightParen(),
                atom.Outdent(),
                atom.ParenCallEnd(),
            ]
        )

    def test_call_multiline_with_string_paren_as_parameter(self) -> None:
        code = dedent("""
            foo(
                "("
            )
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Indent(),
                atom.String("("),
                atom.Outdent(),
                atom.ParenCallEnd(),
            ]
        )

    def test_call_multiline_with_at_properties(self) -> None:
        code = dedent("""
            foo(
                @bar
                @baz
            )
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.Indent(),
                atom.At(),
                atom.Property("bar"),
                atom.Terminator(),
                atom.At(),
                atom.Property("baz"),
                atom.Outdent(),
                atom.ParenCallEnd(),
            ]
        )

    def test_thick_arrow(self) -> None:
        code = "foo => bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.ThickArrow(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_thick_arrow_no_space(self) -> None:
        code = "()=>"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.ParamStart(),
                atom.ParamEnd(),
                atom.ThickArrow(),
            ]
        )

    def test_return(self) -> None:
        code = "return foo"

        self.assertSequenceEqual(scan(code), [atom.Return(), atom.Id("foo")])

    def test_null(self) -> None:
        code = "null bar"

        self.assertSequenceEqual(scan(code), [atom.Null(), atom.Id("bar")])

    def test_undefined(self) -> None:
        code = "undefined bar"

        self.assertSequenceEqual(scan(code), [atom.Void(), atom.Id("bar")])

_property_accessors = {
    ".": atom.Dot,
    "::": atom.Prototype,
    "?.": atom.DotExist,
    "?::": atom.PrototypeExist,
}

def _for_each_property_accessor(func):
    def wrapper(self):
        for accessor, atom_class in _property_accessors.items():
            with self.subTest(accessor=accessor):
                func(self, accessor, atom_class)

    return wrapper

class ScannerPropertyTest(unittest.TestCase):
    @_for_each_property_accessor
    def test_property_invocation(self, accessor, atom_class) -> None:
        code = f'foo{accessor}bar "baz"'

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom_class(),
                atom.Property("bar"),
                atom.WhitespaceCallStart(),
                atom.String("baz"),
                atom.WhitespaceCallEnd(),
            ]
        )

    @_for_each_property_accessor
    def test_property_multiline(self, accessor, atom_class) -> None:
        code = dedent(f"""
            foo{accessor}bar
            {accessor}baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom_class(),
                atom.Property("bar"),
                atom_class(),
                atom.Property("baz"),
            ]
        )

    @_for_each_property_accessor
    def test_property_after_function(self, accessor, atom_class) -> None:
        code = dedent(f"""
            foo ->
                bar
            {accessor}baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.ThinArrow(),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.WhitespaceCallEnd(),
                atom_class(),
                atom.Property("baz"),
            ]
        )

class ScannerIfTest(unittest.TestCase):
    def test_if_block(self) -> None:
        code = dedent("""
            if foo is bar
                baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("foo"),
                atom.Is(),
                atom.Id("bar"),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
            ]
        )

    def test_if_then_expression(self) -> None:
        code = "foo if bar then baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.If(),
                atom.Id("bar"),
                atom.InlineIndent(),
                atom.Id("baz"),
                atom.InlineOutdent(),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_if_then_line(self) -> None:
        code = "if foo then bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
            ]
        )

    def test_if_then_line_enclosed_paren(self) -> None:
        code = "if a then (if b then c else d) else e"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.LeftParen(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.Id("c"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.RightParen(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
            ]
        )

    def test_if_then_line_enclosed_bracket(self) -> None:
        code = "if a then [if b then c else d] else e"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.LeftBracket(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.Id("c"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.RightBracket(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
            ]
        )

    def test_if_then_line_enclosed_brace(self) -> None:
        code = "if a then { foo: if b then c else d } else e"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.LeftBrace(),
                atom.Id("foo"),
                atom.Colon(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.Id("c"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.RightBrace(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
            ]
        )

    def test_if_then_line_enclosed_param(self) -> None:
        code = "if a then (if b then c else d) -> else e"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.ParamStart(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.Id("c"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
            ]
        )

    def test_if_then_line_enclosed_call(self) -> None:
        code = "if a then foo(if b then c else d) else e"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.Id("foo"),
                atom.ParenCallStart(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.Id("c"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.ParenCallEnd(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
            ]
        )

    def test_if_then_nested_line(self) -> None:
        code = "if foo then if bar then baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.If(),
                atom.Id("bar"),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
                atom.Outdent(),
            ]
        )

    def test_if_then_else_line(self) -> None:
        code = "if foo then bar else baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
            ]
        )

    def test_if_then_else_messed_line_block(self) -> None:
        code = dedent("""
            if foo then bar
            else baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
            ]
        )

    def test_if_then_else_nested_line(self) -> None:
        code = "if foo then bar else if baz then qux else goo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.If(),
                atom.Id("baz"),
                atom.Indent(),
                atom.Id("qux"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("goo"),
                atom.Outdent(),
                atom.Outdent(),
            ]
        )

    def test_if_then_else_nested_deep_line(self) -> None:
        code = "if a then if b then if c then d else e else f"

        self.assertListEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.If(),
                atom.Id("c"),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("f"),
                atom.Outdent(),
                atom.Outdent(),
            ]
        )

    def test_if_then_else_nested_deep_block(self) -> None:
        code = dedent("""
            if a
                if b
                    if c
                        d
                    else
                        e
                else
                    f
        """)

        self.assertListEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.If(),
                atom.Id("c"),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("f"),
                atom.Outdent(),
                atom.Outdent(),
            ]
        )

    def test_if_then_else_nested_deep_mixed(self) -> None:
        code = dedent("""
            if a
                if b
                    if c then d else e
                else
                    f
        """)

        self.assertListEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("a"),
                atom.Indent(),
                atom.If(),
                atom.Id("b"),
                atom.Indent(),
                atom.If(),
                atom.Id("c"),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("f"),
                atom.Outdent(),
                atom.Outdent(),
            ]
        )

    def test_unless_then_else_nested_deep_line(self) -> None:
        code = "unless a then unless b then unless c then d else e else f"

        self.assertListEqual(
            scan(code),
            [
                atom.Unless(),
                atom.Id("a"),
                atom.Indent(),
                atom.Unless(),
                atom.Id("b"),
                atom.Indent(),
                atom.Unless(),
                atom.Id("c"),
                atom.Indent(),
                atom.Id("d"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("e"),
                atom.Outdent(),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("f"),
                atom.Outdent(),
                atom.Outdent(),
            ]
        )

    def test_unless_block(self) -> None:
        code = dedent("""
            unless foo is bar
                baz
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Unless(),
                atom.Id("foo"),
                atom.Is(),
                atom.Id("bar"),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
            ]
        )

    def test_post_if_return_with_empty_condition(self) -> None:
        code = "return if"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
            ]
        )

    def test_post_if_return_with_simple_condition(self) -> None:
        code = "return if foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
                atom.Id("foo"),
            ]
        )

    @_for_each_property_accessor
    def test_post_if_with_line_continuator(self, accessor, atom_class) -> None:
        code = dedent(f"""
            return if foo
            {accessor}bar
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
                atom.Id("foo"),
                atom_class(),
                atom.Property("bar"),
            ]
        )

    def test_post_if_is_delimited_by_terminator(self) -> None:
        code = dedent(f"""
            return if foo
            bar
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
                atom.Id("foo"),
                atom.Terminator(),
                atom.Id("bar"),
            ]
        )

    def test_post_if_like_call_with_simple_condition(self) -> None:
        code = "foo if bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.PostIf(),
                atom.Id("bar"),
            ]
        )

    def test_post_if_return_with_call_condition(self) -> None:
        code = "return if foo bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_post_if_cannot_be_recognised_if_it_starts_line(self) -> None:
        code = "if foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.If(),
                atom.Id("foo"),
            ]
        )

    def test_post_if_closes_all_open_calls(self) -> None:
        code = "foo bar if baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
                atom.PostIf(),
                atom.Id("baz"),
            ]
        )

    def test_post_unless_closes_all_open_calls(self) -> None:
        code = "foo bar unless baz"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
                atom.PostUnless(),
                atom.Id("baz"),
            ]
        )

    def test_post_unless_return_with_call_condition(self) -> None:
        code = "return unless foo bar"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostUnless(),
                atom.Id("foo"),
                atom.WhitespaceCallStart(),
                atom.Id("bar"),
                atom.WhitespaceCallEnd(),
            ]
        )

    def test_post_if_return_with_paren_if_expression_condition(self) -> None:
        code = "return if (if foo then bar else baz)"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
                atom.LeftParen(),
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
                atom.RightParen(),
            ]
        )

    def test_post_if_return_with_backet_if_expression_condition(self) -> None:
        code = "return if [if foo then bar else baz]"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
                atom.LeftBracket(),
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
                atom.RightBracket(),
            ]
        )

    def test_post_if_return_with_brace_if_expression_condition(self) -> None:
        code = "return if { goo: if foo then bar else baz }"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Return(),
                atom.PostIf(),
                atom.LeftBrace(),
                atom.Id("goo"),
                atom.Colon(),
                atom.If(),
                atom.Id("foo"),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
                atom.Else(),
                atom.Indent(),
                atom.Id("baz"),
                atom.Outdent(),
                atom.RightBrace(),
            ]
        )

    def test_post_if_inside_function(self) -> None:
        code = dedent("""
            (foo, bar) ->
                return baz if qux
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.ParamStart(),
                atom.Id("foo"),
                atom.Comma(),
                atom.Id("bar"),
                atom.ParamEnd(),
                atom.ThinArrow(),
                atom.Indent(),
                atom.Return(),
                atom.Id("baz"),
                atom.PostIf(),
                atom.Id("qux"),
                atom.Outdent(),
            ]
        )

    def test_outdents_do_not_eat_outer_terminators(self) -> None:
        code = dedent("""
            ->
                foo
            ->
                bar
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.ThinArrow(),
                atom.Indent(),
                atom.Id("foo"),
                atom.Outdent(),
                atom.Terminator(),
                atom.ThinArrow(),
                atom.Indent(),
                atom.Id("bar"),
                atom.Outdent(),
            ]
        )

_keywords = (
    "Infinity",
    "NaN"
    "and",
    "async",
    "await",
    "break",
    "catch",
    "class",
    "continue",
    "delete",
    "extends",
    "false",
    "finally",
    "for",
    "if",
    "in",
    "instanceof",
    "is",
    "isnt",
    "loop",
    "new",
    "no",
    "null",
    "of",
    "off",
    "on",
    "or",
    "return",
    "super",
    "switch",
    "this",
    "throw",
    "true",
    "try",
    "typeof",
    "undefined",
    "unless",
    "until",
    "when",
    "when",
    "while",
    "yes",
    "yield",
)

def _for_each_keyword(func):
    def wrapper(self):
        for keyword in _keywords:
            with self.subTest(keyword=keyword):
                func(self, keyword)

    return wrapper

class ScannerKeywordAsPropertyTest(unittest.TestCase):
    @_for_each_keyword
    def test_as_prototype_property(self, keyword) -> None:
        code = f"Foo::{keyword}"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("Foo"),
                atom.Prototype(),
                atom.Property(keyword),
            ]
        )

    @_for_each_keyword
    def test_as_dot_property(self, keyword) -> None:
        code = f"foo.{keyword}"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Dot(),
                atom.Property(keyword),
            ]
        )

    @_for_each_keyword
    def test_as_at_property(self, keyword) -> None:
        code = f"@{keyword}"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.At(),
                atom.Property(keyword),
            ]
        )

    @_for_each_keyword
    def test_as_object_literal_property_with_braces(self, keyword) -> None:
        code = f"{{ {keyword}: foo }}"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.Id(keyword),
                atom.Colon(),
                atom.Id("foo"),
                atom.RightBrace(),
            ]
        )

    @_for_each_keyword
    def test_as_object_literal_property_without_braces(self, keyword) -> None:
        code = dedent(f"""
            {{
                bar:
                    {keyword}: foo
            }}
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.LeftBrace(),
                atom.Indent(),
                atom.Id("bar"),
                atom.Colon(),
                atom.Indent(),
                atom.Id(keyword),
                atom.Colon(),
                atom.Id("foo"),
                atom.Outdent(),
                atom.Outdent(),
                atom.RightBrace(),
            ]
        )

class ScannerBooleanTest(unittest.TestCase):
    def test_true(self) -> None:
        code = "foo = true"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.True_(),
            ]
        )

    def test_true_is_not_callable(self) -> None:
        code = "true foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.True_(),
                atom.Id("foo"),
            ]
        )

    def test_yes_alias_is_not_allowed(self) -> None:
        code = "yes"

        expected_message = "literal 'yes' is not allowed, use 'true' instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_on_alias_is_not_allowed(self) -> None:
        code = "on"

        expected_message = "literal 'on' is not allowed, use 'true' instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_false(self) -> None:
        code = "foo = false"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Id("foo"),
                atom.Assign(),
                atom.False_(),
            ]
        )

    def test_false_is_not_callable(self) -> None:
        code = "false foo"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.False_(),
                atom.Id("foo"),
            ]
        )

    def test_no_alias_is_not_allowed(self) -> None:
        code = "no"

        expected_message = "literal 'no' is not allowed, use 'false' instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

    def test_off_alias_is_not_allowed(self) -> None:
        code = "off"

        expected_message = "literal 'off' is not allowed, use 'false' instead"

        with self.assertRaisesRegex(LexicalException, expected_message):
            scan(code)

class ScannerNumberTest(unittest.TestCase):
    def test_zero(self) -> None:
        code = "0"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_binary_single_digit(self) -> None:
        code = "0b1"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_binary_plus(self) -> None:
        code = "+0b101"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Add(),
                atom.Number("0b101")
            ]
        )

    def test_binary_minus(self) -> None:
        code = "-0b101"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Sub(),
                atom.Number("0b101")
            ]
        )

    def test_binary_multi_digit(self) -> None:
        code = "0b10110"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_oct_single_digit(self) -> None:
        code = "0o7"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_oct_plus(self) -> None:
        code = "+0o123"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Add(),
                atom.Number("0o123")
            ]
        )

    def test_oct_minus(self) -> None:
        code = "-0o123"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Sub(),
                atom.Number("0o123")
            ]
        )

    def test_oct_muli_digit(self) -> None:
        code = "0o12345670"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_hex_single_digit_upper(self) -> None:
        code = "0xF"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_hex_plus(self) -> None:
        code = "+0xabc"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Add(),
                atom.Number("0xabc")
            ]
        )

    def test_hex_minux(self) -> None:
        code = "-0xabc"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Sub(),
                atom.Number("0xabc")
            ]
        )

    def test_hex_single_digit_lower(self) -> None:
        code = "0xf"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_hex_multi_digit(self) -> None:
        code = "0xaBcDeF0123456789"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_multi_digit(self) -> None:
        code = "1234567890"

        self.assertSequenceEqual(

            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_point_start(self) -> None:
        code = ".123"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_start_with_zero(self) -> None:
        code = "0.5"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_plus(self) -> None:
        code = "+.123"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Add(),
                atom.Number(".123")
            ]
        )

    def test_dec_minus(self) -> None:
        code = "-.123"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Sub(),
                atom.Number(".123")
            ]
        )

    def test_dec_e_lower(self) -> None:
        code = "123e10"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_e_upper(self) -> None:
        code = "123E10"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_e_plus(self) -> None:
        code = "123e+11"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_e_minus(self) -> None:
        code = "123e-11"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Number(code)
            ]
        )

    def test_dec_all_parts(self) -> None:
        code = "-123.456e-11"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Sub(),
                atom.Number("123.456e-11")
            ]
        )

    def test_e_must_be_followed_by_a_number(self) -> None:
        code = "123e "

        with self.assertRaisesRegex(LexicalException, "^expected a digit but found ' '"):
            scan(code)

class ScannerRegexTest(unittest.TestCase):
    def test_regex_simple(self) -> None:
        code = "/foo/"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Regex("foo", delimiter="/")
            ]
        )

class ScannerUnbalancedTest(unittest.TestCase):
    def test_indent_is_not_multiplication_of_4(self) -> None:
        code = dedent("""
            foo
                  bar
        """)

        with self.assertRaisesRegex(LexicalException, r"^indentation is not multiple of 4"):
            scan(code)

    def test_unbalanced_indent_in_paren(self) -> None:
        code = dedent("""
            (foo
                bar)
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected 'outdent' but found '\)'"):
            scan(code)

    def test_unbalanced_indent_in_brace(self) -> None:
        code = dedent("""
            {foo
                bar}
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected 'outdent' but found '}'"):
            scan(code)

    def test_unbalanced_indent_in_bracket(self) -> None:
        code = dedent("""
            [foo
                bar]
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected 'outdent' but found '\]'"):
            scan(code)

    def test_unbalanced_indent_in_param(self) -> None:
        code = dedent("""
            (
                ) ->
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected 'outdent' but found '\)'"):
            scan(code)

    def test_unbalanced_indent_in_call(self) -> None:
        code = dedent("""
            foo(
                )
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected 'outdent' but found '\)'"):
            scan(code)

    def test_unbalanced_paren_in_indent(self) -> None:
        code = dedent("""
            foo
                (bar
            baz)
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected '\)' but found 'outdent'"):
            scan(code)

    def test_unbalanced_paren_in_brace(self) -> None:
        code = dedent("""
            {(foo bar})
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected '\)' but found '}'"):
            scan(code)

    def test_unbalanced_paren_in_bracket(self) -> None:
        code = dedent("""
            [(foo bar])
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected '\)' but found '\]'"):
            scan(code)

    def test_unbalanced_call_in_indent(self) -> None:
        code = dedent("""
            foo
                bar(
            baz)
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected '\)' but found 'outdent'"):
            scan(code)

    def test_unbalanced_call_in_brace(self) -> None:
        code = dedent("""
            {foo(bar})
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected '\)' but found '}'"):
            scan(code)

    def test_unbalanced_call_in_bracket(self) -> None:
        code = dedent("""
            [foo(bar])
        """)

        with self.assertRaisesRegex(LexicalException, r"^expected '\)' but found '\]'"):
            scan(code)

class BlockRegexTest(unittest.TestCase):
    def test_simple(self):
        code = dedent("""
            ///foo///
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Regex("foo", delimiter="///"),
            ]
        )

    def test_empty(self):
        code = "//////"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Regex("", delimiter="///"),
            ]
        )

    def test_missing_one_closing_slash(self):
        code = dedent("""
            ///foo//
        """)

        with self.assertRaisesRegex(LexicalException, r'^unexpected end of input, expected "///"'):
            scan(code)

    def test_missing_two_closing_slashes(self):
        code = dedent("""
            ///foo/
        """)

        with self.assertRaisesRegex(LexicalException, r'^unexpected end of input, expected "///"'):
            scan(code)

    def test_fake_indepolation_escaped_hash(self):
        code = dedent(r"""
            /// \#{ foo } ///
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Regex(r" \#{ foo } ", delimiter="///"),
            ]
        )

    def test_fake_indepolation_escaped_opening_brace(self):
        code = dedent(r"""
            /// #\{ foo } ///
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.Regex(r" #\{ foo } ", delimiter="///"),
            ]
        )

    def test_unclosed_indepolation(self):
        code = dedent("""
            /// #{ foo ///
        """)

        with self.assertRaisesRegex(LexicalException, r'^unexpected end of input, expected "}"'):
            scan(code)

    def test_unclosed_indepolation_escaped_brace(self):
        code = dedent("""
            /// #{ foo \} ///
        """)

        with self.assertRaisesRegex(LexicalException, r'^unexpected end of input, expected "}"'):
            scan(code)

    def test_indepolation(self):
        code = dedent("""
            /// #{ foo } ///
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.RegexStart(),
                atom.Regex(" ", delimiter=""),
                atom.Concat(),
                atom.Id("foo"),
                atom.Concat(),
                atom.Regex(" ", delimiter=""),
                atom.RegexEnd(),
            ]
        )

    def test_indepolation_few(self):
        code = dedent("""
            /// #{ foo } #{ bar } ///
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.RegexStart(),
                atom.Regex(" ", delimiter=""),
                atom.Concat(),
                atom.Id("foo"),
                atom.Concat(),
                atom.Regex(" ", delimiter=""),
                atom.Concat(),
                atom.Id("bar"),
                atom.Concat(),
                atom.Regex(" ", delimiter=""),
                atom.RegexEnd(),
            ]
        )

    def test_interpolation_empty(self):
        code = "///#{}///"

        self.assertSequenceEqual(
            scan(code),
            [
                atom.RegexStart(),
                atom.RegexEnd(),
            ]
        )

    def test_indepolation_nested_regex(self):
        code = dedent("""
            /// #{ ///foo/// bar } ///
        """)

        self.assertSequenceEqual(
            scan(code),
            [
                atom.RegexStart(),
                atom.Regex(" ", delimiter=""),
                atom.Concat(),
                atom.Regex("foo", delimiter="///"),
                atom.Id("bar"),
                atom.Concat(),
                atom.Regex(" ", delimiter=""),
                atom.RegexEnd(),
            ]
        )

if __name__ == "__main__":
    unittest.main()
