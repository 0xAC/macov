from macov.reader import scan
from os import path
from timeit import default_timer as timer
import unittest

class ScanTimeTest(unittest.TestCase):
    def test_scan(self) -> None:
        with open(path.join(path.dirname(path.abspath(__file__)), "big_object.coffee")) as file:
            code = file.read()

        start = timer()
        scan(code)
        end = timer()

        self.assertLess(end - start, 1)

if __name__ == "__main__":
    unittest.main()
