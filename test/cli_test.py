from macov.__main__ import cli
from macov.core import atom
from macov.core.util import LexicalException, Position, Range, SyntaxException
from click.testing import CliRunner
import unittest
from unittest import mock

code = "define [], () ->"
file_name = "foo.coffee"

class CliTest(unittest.TestCase):
    @mock.patch("macov.__main__.scanner.scan", return_value=[])
    def test_lint_success_file(self, scan_mock):
        runner = CliRunner()

        with runner.isolated_filesystem():
            with open(file_name, "w") as f:
                f.write(code)

            result = runner.invoke(cli, ["lint", file_name])

        self.assertIsNone(result.exception)
        self.assertEqual(result.exit_code, 0)
        scan_mock.assert_called_once_with(code)

    @mock.patch("macov.__main__.scanner.scan", side_effect=LexicalException("my message", Position(row=10, column=15)))
    def test_lint_lexical_error(self, scan_mock):
        runner = CliRunner()

        with runner.isolated_filesystem():
            with open(file_name, "w") as f:
                f.write(code)

            result = runner.invoke(cli, ["lint", file_name])

        self.assertEqual(result.output, f'error: my message in file "{file_name}" on line 10, col 15\n')
        self.assertEqual(result.exit_code, 0)
        scan_mock.assert_called_once_with(code)

    @mock.patch("macov.__main__.scanner.scan", return_value="atoms")
    @mock.patch("macov.__main__.parser.parse", side_effect=SyntaxException("my message", atom.String("baz", range=Range(Position(row=10, column=15), Position(row=10, column=20)))))
    def test_parse_syntax_error(self, parse_mock, scan_mock):
        runner = CliRunner()

        with runner.isolated_filesystem():
            with open(file_name, "w") as f:
                f.write(code)

            result = runner.invoke(cli, ["parse", file_name])

        self.assertEqual(result.output, f'error: my message in file "{file_name}" on line 10, col 15\n')
        self.assertEqual(result.exit_code, 0)
        scan_mock.assert_called_once_with(code)
        parse_mock.assert_called_once_with("atoms")

    @mock.patch("macov.__main__.scanner.scan", return_value="atoms")
    @mock.patch("macov.__main__.parser.parse", return_value="tree")
    def test_parse_success_file(self, parse_mock, scan_mock):
        runner = CliRunner()

        with runner.isolated_filesystem():
            with open(file_name, "w") as f:
                f.write(code)

            result = runner.invoke(cli, ["parse", file_name])

        self.assertIsNone(result.exception)
        self.assertEqual(result.exit_code, 0)
        scan_mock.assert_called_once_with(code)
        parse_mock.assert_called_once_with("atoms")

if __name__ == "__main__":
    unittest.main()
